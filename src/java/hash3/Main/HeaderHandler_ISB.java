/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author Haris
 */
public class HeaderHandler_ISB implements SOAPHandler<SOAPMessageContext> {

	String uName = "hash.cube";
	String pWord = "ufone@333";

	@Override
	public boolean handleMessage(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		SOAPMessage message = smc.getMessage();
		if (outboundProperty.booleanValue()) {
			try {
				SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
//				SOAPHeader header = envelope.addHeader();
				if (envelope.getHeader() != null) {
					envelope.getHeader().detachNode();
				}
				SOAPHeader header = envelope.addHeader();

				SOAPElement security = header.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
				security.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

				SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
				usernameToken.addAttribute(new QName("wsu:Id"), "UsernameToken-1");

				SOAPElement username = usernameToken.addChildElement("Username", "wsse");
				username.addTextNode(uName);

				SOAPElement password = usernameToken.addChildElement("Password", "wsse");
				password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				password.addTextNode(pWord);

				//Print out the outbound SOAP message to System.out
//		message.writeTo(System.out);
//		System.out.println("");
//		System.out.println("____________________________________________________");


			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try {
				//This handler does nothing with the response from the Web Service so
				//we just print out the SOAP message.
				//SOAPMessage message = smc.getMessage();
//		message.writeTo(System.out);
//		System.out.println("");
//		System.out.println("____________________________________________________");
			} catch (Exception ex) {
				Logger.getLogger(HeaderHandler_ISB.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		//return outboundProperty;
		return true;

	}

	@Override
	public Set getHeaders() {
		// The code below is added on order to invoke Spring secured WS.
		// Otherwise,
		// http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd
		// won't be recognised 
		QName securityHeader = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");

		HashSet headers = new HashSet();
		headers.add(securityHeader);

		return headers;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		//throw new UnsupportedOperationException("Not supported yet.");
		return true;
	}

	@Override
	public void close(MessageContext context) {
		//throw new UnsupportedOperationException("Not supported yet.");
	}
}