/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Haris
 */
public class SOPLinksClass {

	Connection conn = null;
	String dbURL = "172.16.23.102";
	String dbUrl_live = "jdbc:jtds:sqlserver://" + dbURL + "/csportal";
	String userId_live = "hash.cube@ufonegsm.com";
	String pass_live = "ptml@112233";
	
	public SOPLinksClass() {
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

		} catch (Exception ex) {
			Logger.getLogger(SOPLinksClass.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public String getAllSOPs(){
		String toReturn = "";
		
		Statement stat = null;
		ResultSet rs = null;
		
		JSONArray jsonArray = new JSONArray();
		
		String sql = "SELECT * FROM [csportal].[dbo].[View_tblSop] WHERE Department = 'call center' order by SOP_NAME";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("sopID", rs.getString("ID"));
				jsonObj.put("sopNumber", rs.getString("SOP_NUMBER"));
				jsonObj.put("sopName", rs.getString("SOP_NAME"));
				jsonObj.put("sopDpt", rs.getString("Department"));
				jsonObj.put("sopCreateDate", rs.getString("created_date"));
				jsonObj.put("sopCreatedBy", rs.getString("created_by"));
				jsonObj.put("sopURL", "http://cops/csportal/net/infobrowser/SOPS/");
				jsonArray.add(jsonObj.clone());
			}
			rs.close();
			stat.close();
			conn.close();
		} catch (Exception ex) {
			Logger.getLogger(SOPLinksClass.class.getName()).log(Level.SEVERE, null, ex);
		}
		conn = null;
		stat = null;
		rs = null;

		toReturn = jsonArray.toJSONString();
		return toReturn;
	}
	
	public void closeDBConnections() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception ex) {
		}
		conn = null;
	}
}
