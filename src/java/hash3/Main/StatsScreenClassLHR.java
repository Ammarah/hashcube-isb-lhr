/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Haris
 */
class StatsScreenClassLHR {

	Connection conn = null;
	Statement stat = null;
	ResultSet rs = null;
	JSONObject jsonObj = new JSONObject();
	JSONParser jsonParser = new JSONParser();

	public StatsScreenClassLHR(String url, String user, String pass) {
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn = java.sql.DriverManager.getConnection(url, user, pass);
		} catch (SQLException ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String getAgentStatsScreenDataPresent(String agentID) {
		String toReturn = "";
		JSONObject jsonPresentCurrent = new JSONObject();
		JSONObject jsonPresentMax = new JSONObject();
		CallableStatement proc = null;
		try {
			try {
				proc = conn.prepareCall("{ call stats_screen_present(?) }");
			} catch (SQLException ex) {
				Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
			}
			try {
				proc.setString("@agent_id", agentID);
			} catch (SQLException ex) {
				proc.setNull("@agent_id", java.sql.Types.NULL);
			}
			rs = proc.executeQuery();
			while (rs.next()) {
				jsonPresentCurrent.put("agentlogin", rs.getString("agentlogin"));
				jsonPresentCurrent.put("loggedintime", rs.getString("loggedintime"));
				jsonPresentCurrent.put("notreadytime", rs.getString("notreadytime"));
				jsonPresentCurrent.put("breaktime", rs.getString("breaktime"));
				jsonPresentCurrent.put("acwtime", rs.getString("acwtime"));
				jsonPresentCurrent.put("holdtime", rs.getString("holdtime"));
				jsonPresentCurrent.put("avgtalktime", rs.getString("avgtalktime"));
				jsonPresentCurrent.put("callsoffered", rs.getString("callsoffered"));
				jsonPresentCurrent.put("callsanswered", rs.getString("callsanswered"));
				jsonPresentCurrent.put("callsreq", rs.getString("callsreq"));
				jsonPresentCurrent.put("idleduration", rs.getString("idleduration"));
				//jsonPresentCurrent.put("readyduration", rs.getString("readyduration"));
				jsonPresentCurrent.put("prodduration", rs.getString("prodduration"));
				jsonPresentCurrent.put("shortcalls", rs.getString("shortcalls"));
			}
			rs.close();
			proc.close();
			rs = null;
			proc = null;
		} catch (Exception ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			try {
				proc = conn.prepareCall("{ call stats_screen_present_max(?) }");
			} catch (SQLException ex) {
				Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
			}
			try {
				proc.setString("@agent_id", agentID);
			} catch (SQLException ex) {
				proc.setNull("@agent_id", java.sql.Types.NULL);
			}
			rs = proc.executeQuery();
			while (rs.next()) {
				jsonPresentMax.put("agentlogin", agentID);
				jsonPresentMax.put("loggedintime", rs.getString("maxloggedintime"));
				jsonPresentMax.put("notreadytime", rs.getString("maxnotreadytime"));
				jsonPresentMax.put("breaktime", rs.getString("maxbreaktime"));
				jsonPresentMax.put("acwtime", rs.getString("maxacwtime"));
				jsonPresentMax.put("holdtime", rs.getString("maxholdtime"));
				jsonPresentMax.put("avgtalktime", rs.getString("maxavgtalktime"));
				jsonPresentMax.put("callsoffered", rs.getString("maxcallsoffered"));
				jsonPresentMax.put("callsanswered", rs.getString("maxcallsans"));
				jsonPresentMax.put("callsreq", rs.getString("maxcallsreq"));
				jsonPresentMax.put("idleduration", rs.getString("maxidleduration"));
				//jsonPresentMax.put("readyduration", rs.getString("maxreadyduration"));
				jsonPresentMax.put("prodduration", rs.getString("maxprodduration"));
				jsonPresentMax.put("shortcalls", rs.getString("maxshortcalls"));
			}
			rs.close();
			proc.close();
			rs = null;
			proc = null;
		} catch (Exception ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
		jsonObj.clear();
		jsonObj.put("presentCurrent", jsonPresentCurrent);
		jsonObj.put("presentMax", jsonPresentMax);
		toReturn = jsonObj.toJSONString();
		jsonPresentCurrent = null;
		jsonPresentMax = null;
		return toReturn;
	}

	public String getAgentStatsScreenDataPast(String agentID) {
		String toReturn = "";
		JSONObject jsonPresentCurrent = new JSONObject();
		JSONObject jsonPresentMax = new JSONObject();
		CallableStatement proc = null;
		try {
			try {
				proc = conn.prepareCall("{ call stats_screen_past(?) }");
			} catch (SQLException ex) {
				Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
			}
			try {
				proc.setString("@agent_id", agentID);
			} catch (SQLException ex) {
				proc.setNull("@agent_id", java.sql.Types.NULL);
			}
			rs = proc.executeQuery();
			while (rs.next()) {
				jsonPresentCurrent.put("agentlogin", rs.getString("agentlogin"));
				jsonPresentCurrent.put("loggedintime", rs.getString("loggedintime"));
				jsonPresentCurrent.put("notreadytime", rs.getString("notreadytime"));
				jsonPresentCurrent.put("breaktime", rs.getString("breaktime"));
				jsonPresentCurrent.put("acwtime", rs.getString("acwtime"));
				jsonPresentCurrent.put("holdtime", rs.getString("holdtime"));
				jsonPresentCurrent.put("avgtalktime", rs.getString("avgtalktime"));
				jsonPresentCurrent.put("callsoffered", rs.getString("callsoffered"));
				jsonPresentCurrent.put("callsanswered", rs.getString("callsanswered"));
				jsonPresentCurrent.put("callsreq", rs.getString("callsreq"));
				jsonPresentCurrent.put("idleduration", rs.getString("idleduration"));
				//jsonPresentCurrent.put("readyduration", rs.getString("readyduration"));
				jsonPresentCurrent.put("prodduration", rs.getString("prodduration"));
				jsonPresentCurrent.put("shortcalls", rs.getString("shortcalls"));
			}
			rs.close();
			proc.close();
			rs = null;
			proc = null;
		} catch (Exception ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			try {
				proc = conn.prepareCall("{ call stats_screen_past_max(?) }");
			} catch (SQLException ex) {
				Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
			}
			try {
				proc.setString("@agent_id", agentID);
			} catch (SQLException ex) {
				proc.setNull("@agent_id", java.sql.Types.NULL);
			}
			rs = proc.executeQuery();
			while (rs.next()) {
				jsonPresentMax.put("agentlogin", agentID);
				jsonPresentMax.put("loggedintime", rs.getString("maxloggedintime"));
				jsonPresentMax.put("notreadytime", rs.getString("maxnotreadytime"));
				jsonPresentMax.put("breaktime", rs.getString("maxbreaktime"));
				jsonPresentMax.put("acwtime", rs.getString("maxacwtime"));
				jsonPresentMax.put("holdtime", rs.getString("maxholdtime"));
				jsonPresentMax.put("avgtalktime", rs.getString("maxavgtalktime"));
				jsonPresentMax.put("callsoffered", rs.getString("maxcallsoffered"));
				jsonPresentMax.put("callsanswered", rs.getString("maxcallsans"));
				jsonPresentMax.put("callsreq", rs.getString("maxcallsreq"));
				jsonPresentMax.put("idleduration", rs.getString("maxidleduration"));
				//jsonPresentMax.put("readyduration", rs.getString("maxreadyduration"));
				jsonPresentMax.put("prodduration", rs.getString("maxprodduration"));
				jsonPresentMax.put("shortcalls", rs.getString("maxshortcalls"));
			}
			rs.close();
			proc.close();
			rs = null;
			proc = null;
		} catch (Exception ex) {
			Logger.getLogger(StatsScreenClassLHR.class.getName()).log(Level.SEVERE, null, ex);
		}
		jsonObj.clear();
		jsonObj.put("pastCurrent", jsonPresentCurrent);
		jsonObj.put("pastMax", jsonPresentMax);
		toReturn = jsonObj.toJSONString();
		jsonPresentCurrent = null;
		jsonPresentMax = null;
		return toReturn;
	}
	
	public void closeDBConnections() {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
		}
		try {
			if (stat != null) {
				stat.close();
			}
		} catch (Exception ex) {
		}
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception ex) {
		}
		conn = null;
		stat = null;
		rs = null;
	}
}
