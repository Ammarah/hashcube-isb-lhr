/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var timerId = null;
var LocalTime_timerId = null;

function populateStatsScreen() {

	var AnimationSpeed = 700;
	//statsScreenObj.destroy();

	var LoggedInTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.loggedInTimeCurrent_seconds,
			statsScreenObj.loggedInTime_sla_seconds,
			statsScreenObj.statsLongScaleWidth
			);
	var LoggedInTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.loggedInTimeMax_seconds,
			statsScreenObj.loggedInTime_sla_seconds,
			statsScreenObj.statsLongScaleWidth
			) - 18;
	var IdleTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.idleTimeCurrent_seconds,
			statsScreenObj.idleTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var IdleTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.idleTimeMax_seconds,
			statsScreenObj.idleTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var BreakTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.breakTimeCurrent_seconds,
			statsScreenObj.breakTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var BreakTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.breakTimeMax_seconds,
			statsScreenObj.breakTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var NotReadyTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.notReadyTimeCurrent_seconds,
			statsScreenObj.notReadyTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var NotReadyTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.notReadyTimeMax_seconds,
			statsScreenObj.notReadyTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;

	var ProductiveDurationCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.productiveDurationCurrent_seconds,
			statsScreenObj.productiveDuration_sla_seconds,
			statsScreenObj.statsLongScaleWidth
			);
	var ProductiveDurationMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.productiveDurationMax_seconds,
			statsScreenObj.productiveDuration_sla_seconds,
			statsScreenObj.statsLongScaleWidth
			) - 18;
	var ACWTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.acwTimeCurrent_seconds,
			statsScreenObj.acwTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var ACWTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.acwTimeMax_seconds,
			statsScreenObj.acwTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var HoldTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.holdTimeCurrent_seconds,
			statsScreenObj.holdTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var HoldTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.holdTimeMax_seconds,
			statsScreenObj.holdTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var AvgTalkTimeCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.avgTalkTimeCurrent_seconds,
			statsScreenObj.avgTalkTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			);
	var AvgTalkTimeMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.avgTalkTimeMax_seconds,
			statsScreenObj.avgTalkTime_sla_seconds,
			statsScreenObj.statsShortScaleWidth
			) - 18;

	var CallsOfferedCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsOfferedCurrent,
			statsScreenObj.callsOffered_sla,
			statsScreenObj.statsLongScaleWidth
			);
	var CallsOfferedMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsOfferedMax,
			statsScreenObj.callsOffered_sla,
			statsScreenObj.statsLongScaleWidth
			) - 18;
	var CallsAnsweredCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsAnsweredCurrent,
			statsScreenObj.callsAnswered_sla,
			statsScreenObj.statsShortScaleWidth
			);
	var CallsAnsweredMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsAnsweredMax,
			statsScreenObj.callsAnswered_sla,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var CallsREQCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsREQCurrent,
			statsScreenObj.callsREQ_sla,
			statsScreenObj.statsShortScaleWidth
			);
	var CallsREQMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.callsREQMax,
			statsScreenObj.callsREQ_sla,
			statsScreenObj.statsShortScaleWidth
			) - 18;
	var ShortCallsCurrent = statsScreenObj.getValuesInPixel(
			statsScreenObj.shortCallsCurrent,
			statsScreenObj.shortCalls_sla,
			statsScreenObj.statsShortScaleWidth
			);
	var ShortCallsMax = statsScreenObj.getValuesInPixel(
			statsScreenObj.shortCallsMax,
			statsScreenObj.shortCalls_sla,
			statsScreenObj.statsShortScaleWidth
			) - 18;

	$("#loggedInTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.loggedInTime_sla_seconds, statsScreenObj.statsLongSLABuffer)));
	$("#loggedInTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.loggedInTime_sla_seconds));

	$("#idleTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.idleTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#idleTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.idleTime_sla_seconds));

	$("#breakTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.breakTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#breakTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.breakTime_sla_seconds));

	$("#notReadyTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.notReadyTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#notReadyTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.notReadyTime_sla_seconds));

	$("#productiveDurationLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.productiveDuration_sla_seconds, statsScreenObj.statsLongSLABuffer)));
	$("#productiveDurationSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.productiveDuration_sla_seconds));

	$("#acwTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.acwTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#acwTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.acwTime_sla_seconds));

	$("#holdTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.holdTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#holdTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.holdTime_sla_seconds));

	$("#avgTalkTimeLimitSpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.getSLABufferLimit(statsScreenObj.avgTalkTime_sla_seconds, statsScreenObj.statsShortSLABuffer)));
	$("#avgTalkTimeSLASpan").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.avgTalkTime_sla_seconds));

	$("#callsOfferedLimitSpan").html(statsScreenObj.getSLABufferLimit(statsScreenObj.callsOffered_sla, statsScreenObj.statsLongSLABuffer));
	$("#callsOfferedSLASpan").html(statsScreenObj.callsOffered_sla);

	$("#callsAnsweredLimitSpan").html(statsScreenObj.getSLABufferLimit(statsScreenObj.callsAnswered_sla, statsScreenObj.statsShortSLABuffer));
	$("#callsAnsweredSLASpan").html(statsScreenObj.callsAnswered_sla);

	$("#callsREQLimitSpan").html(statsScreenObj.getSLABufferLimit(statsScreenObj.callsREQ_sla, statsScreenObj.statsShortSLABuffer));
	$("#callsREQSLASpan").html(statsScreenObj.callsREQ_sla);

	$("#shortCallsLimitSpan").html(statsScreenObj.getSLABufferLimit(statsScreenObj.shortCalls_sla, statsScreenObj.statsShortSLABuffer));
	$("#shortCallsSLASpan").html(statsScreenObj.shortCalls_sla);


	//Panel 1 - Track 1
	$(".progress-track-1-1").animate({
		width: LoggedInTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-1-1").animate({
		left: LoggedInTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-1-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-1-1").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.loggedInTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-1-1").animate({
		left: LoggedInTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-1-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-1-1").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.loggedInTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 1 - Track 2
	$(".progress-track-1-2").animate({
		width: IdleTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-1-2").animate({
		left: IdleTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-1-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-1-2").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.idleTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-1-2").animate({
		left: IdleTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-1-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-1-2").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.idleTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 1 - Track 3
	$(".progress-track-1-3").animate({
		width: BreakTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-1-3").animate({
		left: BreakTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-1-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-1-3").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.breakTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-1-3").animate({
		left: BreakTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-1-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-1-3").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.breakTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 1 - Track 4
	$(".progress-track-1-4").animate({
		width: NotReadyTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-1-4").animate({
		left: NotReadyTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-1-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-1-4").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.notReadyTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-1-4").animate({
		left: NotReadyTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-1-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-1-4").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.notReadyTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 2 - Track 1
	$(".progress-track-2-1").animate({
		width: ProductiveDurationCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-2-1").animate({
		left: ProductiveDurationCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-2-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-2-1").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.productiveDurationCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-2-1").animate({
		left: ProductiveDurationMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-2-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-2-1").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.productiveDurationMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 2 - Track 2
	$(".progress-track-2-2").animate({
		width: ACWTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-2-2").animate({
		left: ACWTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-2-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-2-2").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.acwTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-2-2").animate({
		left: ACWTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-2-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-2-2").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.acwTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 2 - Track 3
	$(".progress-track-2-3").animate({
		width: HoldTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-2-3").animate({
		left: HoldTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-2-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-2-3").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.holdTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-2-3").animate({
		left: HoldTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-2-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-2-3").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.holdTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 2 - Track 4
	$(".progress-track-2-4").animate({
		width: AvgTalkTimeCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-2-4").animate({
		left: AvgTalkTimeCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-2-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-2-4").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.avgTalkTimeCurrent_seconds));
		},
		complete: function() {

		}
	});
	$(".min-threshold-2-4").animate({
		left: AvgTalkTimeMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-2-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-2-4").html(statsScreenObj.convertSecondsToHHMM(statsScreenObj.avgTalkTimeMax_seconds));
		},
		complete: function() {

		}
	});

	//Panel 3 - Track 1
	$(".progress-track-3-1").animate({
		width: CallsOfferedCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-3-1").animate({
		left: CallsOfferedCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-3-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-3-1").html(statsScreenObj.callsOfferedCurrent);
		},
		complete: function() {

		}
	});
	$(".min-threshold-3-1").animate({
		left: CallsOfferedMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-3-1").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-3-1").html(statsScreenObj.callsOfferedMax);
		},
		complete: function() {

		}
	});

	//Panel 3 - Track 2
	$(".progress-track-3-2").animate({
		width: CallsAnsweredCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-3-2").animate({
		left: CallsAnsweredCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-3-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-3-2").html(statsScreenObj.callsAnsweredCurrent);
		},
		complete: function() {

		}
	});
	$(".min-threshold-3-2").animate({
		left: CallsAnsweredMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-3-2").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-3-2").html(statsScreenObj.callsAnsweredMax);
		},
		complete: function() {

		}
	});

	//Panel 3 - Track 3
	$(".progress-track-3-3").animate({
		width: CallsREQCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-3-3").animate({
		left: CallsREQCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-3-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-3-3").html(statsScreenObj.callsREQCurrent);
		},
		complete: function() {

		}
	});
	$(".min-threshold-3-3").animate({
		left: CallsREQMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-3-3").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-3-3").html(statsScreenObj.callsREQMax);
		},
		complete: function() {

		}
	});

	//Panel 3 - Track 4
	$(".progress-track-3-4").animate({
		width: ShortCallsCurrent
	}, AnimationSpeed, 'easeInOutExpo');
	$(".current-status-3-4").animate({
		left: ShortCallsCurrent
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".current-status-3-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#current-status-3-4").html(statsScreenObj.shortCallsCurrent);
		},
		complete: function() {

		}
	});
	$(".min-threshold-3-4").animate({
		left: ShortCallsMax
	}, {
		easing: 'easeInOutExpo',
		duration: AnimationSpeed,
		step: function() {
			var LeftValue = $(".min-threshold-3-4").css('left');
			LeftValue = LeftValue.replace("px", "");
			LeftValue = Number(LeftValue);
			LeftValue = Math.round(LeftValue);

			$("#min-threshold-3-4").html(statsScreenObj.shortCallsMax);
		},
		complete: function() {

		}
	});
	//clearInterval(timerId);
}

function getTime() {
	var d = new Date();
	var hours = d.getHours();
	var mins = d.getMinutes();

	var AnimationSpeed = 700;

	var total_mins = (hours * 60) + mins;
	var total_width = (total_mins * 0.76) - 4; //0.76 is a pixel
	total_width = Math.ceil(total_width);

	var current_width = $(".time-progress-track").css('width');
	current_width = current_width.replace("px", "");
	current_width = Number(current_width);
	current_width = Math.ceil(current_width);

	if (total_width != current_width) {
		$(".time-progress-track").animate({
			width: total_width
		}, AnimationSpeed, 'easeInOutExpo');
	}
}


/*
 //Statistics Page Settings
 //Indicator positions
 var CurrentStatus_top_01 = 347;
 var MinThreshold_top_01 = 700;
 var CurrentStatus_top_02 = 50;
 var MinThreshold_top_02 = 100;
 var CurrentStatus_top_03 = 25;
 var MinThreshold_top_03 = 100;
 var CurrentStatus_top_04 = 42;
 var MinThreshold_top_04 = 120;
 
 var CurrentStatus_middle_01 = 450;
 var MinThreshold_middle_01 = 650;
 var CurrentStatus_middle_02 = 40;
 var MinThreshold_middle_02 = 70;
 var CurrentStatus_middle_03 = 15;
 var MinThreshold_middle_03 = 120;
 var CurrentStatus_middle_04 = 62;
 var MinThreshold_middle_04 = 100;
 
 var CurrentStatus_bottom_01 = 250;
 var MinThreshold_bottom_01 = 450;
 var CurrentStatus_bottom_02 = 35;
 var MinThreshold_bottom_02 = 60;
 var CurrentStatus_bottom_03 = 40;
 var MinThreshold_bottom_03 = 80;
 var CurrentStatus_bottom_04 = 116;
 var MinThreshold_bottom_04 = 60;
 */