
//Global Settings
var AnimationSpeed = 700;      //Time in miliseconds
var WaitBeforeAnimation = 1;   //Time in seconds

//Statistics Page Settings
//Indicator positions
var CurrentStatus_top_01 = 347;
var MinThreshold_top_01 = 700;
var CurrentStatus_top_02 = 50;
var MinThreshold_top_02 = 100;
var CurrentStatus_top_03 = 25;
var MinThreshold_top_03 = 100;
var CurrentStatus_top_04 = 42;
var MinThreshold_top_04 = 120;

var CurrentStatus_middle_01 = 450;
var MinThreshold_middle_01 = 650;
var CurrentStatus_middle_02 = 40;
var MinThreshold_middle_02 = 70;
var CurrentStatus_middle_03 = 15;
var MinThreshold_middle_03 = 120;
var CurrentStatus_middle_04 = 62;
var MinThreshold_middle_04 = 100;

var CurrentStatus_bottom_01 = 250;
var MinThreshold_bottom_01 = 450;
var CurrentStatus_bottom_02 = 35;
var MinThreshold_bottom_02 = 60;
var CurrentStatus_bottom_03 = 40;
var MinThreshold_bottom_03 = 80;
var CurrentStatus_bottom_04 = 116;
var MinThreshold_bottom_04 = 60;
