/*
 * Main HASHCUBE JavaScript Library v1.0
 * 
 * O3Interfaces (Pvt) Ltd. 2012
 * Client: Apollo Telecom
 * Date: July 2012
 */

//Global Variables
var timerId;
var LocalTime_timerId;

require(["settings"], function() {
	$(function() {
		function init() {
			//Panel 1 - Track 1
			$(".progress-track-1-1").animate({
				width: CurrentStatus_top_01
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-1-1").animate({
				left: CurrentStatus_top_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-1-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-1-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-1-1").animate({
				left: MinThreshold_top_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-1-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-1-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 1 - Track 2
			$(".progress-track-1-2").animate({
				width: CurrentStatus_top_02
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-1-2").animate({
				left: CurrentStatus_top_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-1-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-1-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-1-2").animate({
				left: MinThreshold_top_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-1-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-1-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 1 - Track 3
			$(".progress-track-1-3").animate({
				width: CurrentStatus_top_03
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-1-3").animate({
				left: CurrentStatus_top_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-1-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-1-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-1-3").animate({
				left: MinThreshold_top_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-1-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-1-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 1 - Track 4
			$(".progress-track-1-4").animate({
				width: CurrentStatus_top_04
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-1-4").animate({
				left: CurrentStatus_top_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-1-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-1-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-1-4").animate({
				left: MinThreshold_top_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-1-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-1-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 2 - Track 1
			$(".progress-track-2-1").animate({
				width: CurrentStatus_middle_01
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-2-1").animate({
				left: CurrentStatus_middle_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-2-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-2-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-2-1").animate({
				left: MinThreshold_middle_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-2-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-2-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 2 - Track 2
			$(".progress-track-2-2").animate({
				width: CurrentStatus_middle_02
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-2-2").animate({
				left: CurrentStatus_middle_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-2-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-2-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-2-2").animate({
				left: MinThreshold_middle_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-2-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-2-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 2 - Track 3
			$(".progress-track-2-3").animate({
				width: CurrentStatus_middle_03
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-2-3").animate({
				left: CurrentStatus_middle_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-2-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-2-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-2-3").animate({
				left: MinThreshold_middle_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-2-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-2-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 2 - Track 4
			$(".progress-track-2-4").animate({
				width: CurrentStatus_middle_04
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-2-4").animate({
				left: CurrentStatus_middle_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-2-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-2-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-2-4").animate({
				left: MinThreshold_middle_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-2-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-2-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 3 - Track 1
			$(".progress-track-3-1").animate({
				width: CurrentStatus_bottom_01
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-3-1").animate({
				left: CurrentStatus_bottom_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-3-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-3-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-3-1").animate({
				left: MinThreshold_bottom_01
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-3-1").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-3-1").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 3 - Track 2
			$(".progress-track-3-2").animate({
				width: CurrentStatus_bottom_02
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-3-2").animate({
				left: CurrentStatus_bottom_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-3-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-3-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-3-2").animate({
				left: MinThreshold_bottom_02
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-3-2").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-3-2").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 3 - Track 3
			$(".progress-track-3-3").animate({
				width: CurrentStatus_bottom_03
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-3-3").animate({
				left: CurrentStatus_bottom_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-3-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-3-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-3-3").animate({
				left: MinThreshold_bottom_03
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-3-3").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-3-3").html(LeftValue);
				},
				complete: function () {
                
				}
			});
            
			//Panel 3 - Track 4
			$(".progress-track-3-4").animate({
				width: CurrentStatus_bottom_04
			}, AnimationSpeed, 'easeInOutCirc');
			$(".current-status-3-4").animate({
				left: CurrentStatus_bottom_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".current-status-3-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#current-status-3-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			$(".min-threshold-3-4").animate({
				left: MinThreshold_bottom_04
			}, {
				easing: 'easeInOutCirc',
				duration: AnimationSpeed,
				step: function () {
					var LeftValue = $(".min-threshold-3-4").css('left');
					LeftValue = LeftValue.replace("px", "");
					LeftValue = Number(LeftValue);
					LeftValue = Math.round(LeftValue);
                
					$("#min-threshold-3-4").html(LeftValue);
				},
				complete: function () {
                
				}
			});
			clearInterval(timerId);
		}
		timerId = setInterval(init, WaitBeforeAnimation*1000);
        
		function getTime() {
			var d = new Date();
			var hours = d.getHours();
			var mins = d.getMinutes();
            
			var total_mins = (hours*60)+mins;
			var total_width = (total_mins*0.76)-4; //0.76 is a pixel
			total_width = Math.ceil(total_width);
            
			var current_width = $(".time-progress-track").css('width');
			current_width = current_width.replace("px", "");
			current_width = Number(current_width);
			current_width = Math.ceil(current_width);
            
			if (total_width != current_width) {
				$(".time-progress-track").animate({
					width: total_width
				}, AnimationSpeed, 'easeInOutCirc');
			}
		}
		LocalTime_timerId = setInterval(getTime, 1000);
	});
});



