var totalWindows = 3;
var winWidth = $(window).width();

var intervalVariable = null;
var intervalCallVariable = null;
var intervalHoldVariable = null;
var intervalBreakVariable = null;

//$("#anchrBtn_brkDiv").fancybox({
//	openEffect: 'none',
//	closeEffect: 'none',
//	closeBtn: false,
//	modal: true,
//	beforeClose: function () {
//		//alert($('#spanBreakTimer').countdown('getTimes'));
//		$('#txtBrkPwd').val('');
//		clientObj.Ready();
//	}
//});

//$("#anchrBtn_internalCallDiv").fancybox({
//	openEffect: 'none',
//	closeEffect: 'none',
//	closeBtn: false,
//	modal: true,
//	beforeClose: function () {
//		//alert($('#spanBreakTimer').countdown('getTimes'));
//	}
//});

$('#btnChangePassword').live('click', function () {
    //$('#anchrBtn_chngpwd').click();
});

//$("#anchrBtn_chngpwd").fancybox({
//	openEffect: 'none',
//	closeEffect: 'none',
//	closeBtn: false,
//	beforeClose: function () {
//		$("#chngpwd_username").val('');
//		$("#chngpwd_oldpwd").val('');
//		$("#chngpwd_newpwd").val('');
//	}
//});

$("#btnChangePwd_Confirm").live('click', function () {
    caresChangePassword($.trim($("#chngpwd_username").val()), $("#chngpwd_oldpwd").val(), $("#chngpwd_newpwd").val());
});

$("#btnChangePwd_Cancel").live('click', function () {
    $("#chngpwd_username").val('');
    $("#chngpwd_oldpwd").val('');
    $("#chngpwd_newpwd").val('');
    try {
        $.fancybox.close();
    } catch (ex) {
    }
});

function formatArray(attachedData) {
    var attData = Array();
    for (var i = 0; i < attachedData.length; i++) {
        attData[i] = $.trim(attachedData[i].substring(3));
        if (attachedData[i] == '') {
            attData[i] = 'N/A';
        }
    }
    return attData;
}

function close_break_timer_lightbox() {
    $("button").css('z-index', '1500');
    $('button.expand-button').css('z-index', '0');
    $("#lightbox-bg-blackout").css('display', 'none');
    $("#lightbox-bg-blackout").css('z-index', '0');
    $("#lightbox-bg-blackout").css('opacity', '0');
    /*
     $("#lightbox-bg-blackout").animate({
     opacity: 0
     }, 10, 'easeOutCirc');
     */
    /*
     $(".break-timer-lightbox").animate({
     opacity: 0
     }, 10, 'easeOutCirc', function() {
     $(".break-timer-lightbox").css('display', 'none');
     });
     */
    $(".break-timer-lightbox").css('opacity', '0');
    $(".break-timer-lightbox").css('display', 'none');
}

function hide_break_timer_password() {
    $(".break-timer-counter-wrapper").animate({
        marginTop: "61px"
    }, 10, 'easeInOutCirc');
    $("#spanBreakTimer").animate({
        fontSize: "350%"
    }, 10, 'easeInOutCirc');
    $(".break-timer-password-field-wrapper").animate({
        opacity: 0
    }, 10, 'easeInOutCirc', function () {
        $(".break-timer-password-field-wrapper").css('display', 'none');
    });
    clearInterval(hide_break_timerId);
}

starts_with = function (toMatch, origStr) {
    return origStr.slice(0, toMatch.length) == toMatch;
};

function getFormatedDate(d) {
    var a_p = "";

    var curr_hour = d.getHours();
    if (curr_hour < 12)
    {
        a_p = "AM";
    }
    else
    {
        a_p = "PM";
    }
    if (curr_hour == 0)
    {
        curr_hour = 12;
    }
    if (curr_hour > 12)
    {
        curr_hour = curr_hour - 12;
    }

    var curr_min = d.getMinutes();

    curr_min = curr_min + "";

    if (curr_min.length == 1)
    {
        curr_min = "0" + curr_min;
    }

    var curr_sec = d.getSeconds();

    curr_sec = curr_sec + "";

    if (curr_sec.length == 1)
    {
        curr_sec = "0" + curr_sec;
    }

    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();

    var toReturn = curr_month + "/" + curr_date + "/" + curr_year + " " + curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p;

    return toReturn;
}

function toggleButton(obj) {
    if ($(obj).parent().attr('class') == 'selected-telebar-btn-wrapper') {
        $(obj).parent().attr('class', 'telebar-btn-wrapper');
    }
    else {
        $(obj).parent().attr('class', 'selected-telebar-btn-wrapper');
    }
    return false;
}

function resetButtonSelection(obj) {
    $(obj).parent().attr('class', 'telebar-btn-wrapper');
}

function setButtonSelection(obj) {
    $(obj).parent().attr('class', 'selected-telebar-btn-wrapper');
}

var numpad_mutex = true;

document.onkeydown = function (evt) {
    if (evt.keyCode == 115) {
        switch (clientObj.getIpPhoneType()) {
            case "Nortel":
                return true;
                break;
            case "Cisco_LHR":
                if (clientObj.isCCTLoggedIn()) {
                    return false;
                }
                else {
                    return true;
                }
                break;
            case "Cisco_ISB":
                if (clientObj.isCCTLoggedIn()) {
                    return false;
                }
                else {
                    return true;
                }
                break;
            case "Cisco_KHI":
                if (clientObj.isCCTLoggedIn()) {
                    return false;
                }
                else {
                    return true;
                }
                break;
        }
    }
    else if (evt.keyCode >= 112 && evt.keyCode <= 123) {
        return false;
    }
    if (evt.keyCode >= 96 && evt.keyCode <= 105 && numpad_mutex) {
        numpad_mutex = false;
        if (!$("#search_acw").is(":focus") && !$("#search_ivr").is(":focus") && !$("#txtAgentDN").is(":focus") && !$("#txtUsername").is(":focus") && !$("#txtUserpass").is(":focus"))
        {
            if (evt.keyCode == 103) {
                closeLightBox();
                setTimeout(function () {
                    $("#package-details-expand-btn").mousedown();
                    numpad_mutex = true;
                }, 1000);
                //$(".package-details-lightbox").css('display', 'block');
            }
            else if (evt.keyCode == 100) {
                closeLightBox();
                setTimeout(function () {
                    $("#profit-sanc-expand-btn").mousedown();
                    numpad_mutex = true;
                }, 1000);
                //$(".profit-sanc-lightbox").css('display', 'block');
            }
            else if (evt.keyCode == 101) {
                closeLightBox();
                setTimeout(function () {
                    $("#customer-demo-expand-btn").mousedown();
                    numpad_mutex = true;
                }, 1000);
                //$(".customer-demo-lightbox").css('display', 'block');
            }
            else if (evt.keyCode == 102) {
                closeLightBox();
                setTimeout(function () {
                    $("#latest-activities-expand-btn").mousedown();
                    numpad_mutex = true;
                }, 1000);
                //$(".latest-activities-lightbox").css('display', 'block');
            }
        }
        return true;
    }
    else if (evt.keyCode == 27) {
        closeLightBox();
        return true;
    }
};

function closeLightBox() {
    if (acw_check) {
        $("#close-acw-lightbox").mousedown();
    }
    $("#close-ivr-lightbox").mousedown();
    $("#close-consent-lightbox").mousedown();
    $("#close-customer-demo-lightbox").mousedown();
    $("#close-profit-sanc-lightbox").mousedown();
    $("#close-package-details-lightbox").mousedown();
    $("#close-latest-activities-lightbox").mousedown();
}

function toggle_agent_state(status) {
    if (status == 1) {
        $("#agent-info-btn").attr('class', 'agent-info-ready');
    }
    else if (status == 0) {
        $("#agent-info-btn").attr('class', 'agent-info-not-ready');
    }
}

var splash_mutex = true;
function splash_screen_show_hash() {
    if (splash_mutex)
    {
        splash_mutex = false;
        $("button").css('z-index', '0');
        $("#lightbox-bg-blackout").css('display', 'block');
        $("#lightbox-bg-blackout").css('z-index', '1500');
        $("#lightbox-bg-blackout").css('opacity', '0.8');
        /*
         $("#lightbox-bg-blackout").animate({
         opacity: 0.8
         }, 10, 'easeOutCirc');
         */
        $(".splash-screen-lightbox").css('display', 'block');
        $(".splash-screen-lightbox").css('z-index', '2000');
        $(".splash-screen-lightbox").css('opacity', '1');
        /*
         $(".splash-screen-lightbox").animate({
         opacity: 1
         }, 10, 'easeOutCirc');
         */
        $(".splash-screen-lightbox").focus();
        splash_mutex = true;
    }
}

function splash_screen_hide_hash() {
    if (splash_mutex)
    {
        splash_mutex = false;
        $("button").css('z-index', '1500');
        $('button.expand-button').css('z-index', '0');
        $("#lightbox-bg-blackout").css('display', 'none');
        $("#lightbox-bg-blackout").css('z-index', '0');
        $("#lightbox-bg-blackout").css('opacity', '0');
        /*
         $("#lightbox-bg-blackout").animate({
         opacity: 0
         }, 10, 'easeOutCirc');
         */

        $(".splash-screen-lightbox").css('opacity', '0');
        $(".splash-screen-lightbox").css('display', 'none');
        $(".splash-screen-lightbox").css('z-index', '0');
        /*
         $(".splash-screen-lightbox").animate({
         opacity: 0
         }, 10, 'easeOutCirc', function() {
         $(".splash-screen-lightbox").css('display', 'none');
         $(".splash-screen-lightbox").css('z-index','0');
         });
         */
        $(".splash-screen-lightbox").blur();
        splash_mutex = true;
    }
}

function clearPortals() {
    $('.package-details-body-wrapper').find('.services-wrapper div.gprs').attr('class', 'inactive gprs');
    $('.package-details-body-wrapper').find('.services-wrapper div.conference').attr('class', 'inactive conference');
    $('.package-details-body-wrapper').find('.services-wrapper div.sms').attr('class', 'inactive sms');
    $('.package-details-body-wrapper').find('.services-wrapper div.cli').attr('class', 'inactive cli');
    $('.package-details-body-wrapper').find('.services-wrapper div.forward').attr('class', 'inactive forward');
    $('.package-details-body-wrapper').find('.services-wrapper div.mms').attr('class', 'inactive mms');
    $('.package-details-body-wrapper').find('.services-wrapper div.waiting').attr('class', 'inactive waiting');

    $('.package-details-body-wrapper').find('.services-wrapper div.restrict').attr('class', 'inactive restrict');
    $('.package-details-body-wrapper').find('.services-wrapper div.vmail').attr('class', 'inactive vmail');
    $('.package-details-body-wrapper').find('.services-wrapper div.wap').attr('class', 'inactive wap');
    $('.package-details-body-wrapper').find('.services-wrapper div.umail').attr('class', 'inactive umail');
    $('.package-details-body-wrapper').find('.services-wrapper div.fax').attr('class', 'inactive fax');
    $('.package-details-body-wrapper').find('.services-wrapper div.info').attr('class', 'inactive info');
    $('.package-details-body-wrapper').find('.services-wrapper div.crbt').attr('class', 'inactive crbt');
    $('.package-details-body-wrapper').find('.services-wrapper div.mcn').attr('class', 'inactive mcn');
    $('.package-details-body-wrapper').find('.services-wrapper div.sb').attr('class', 'inactive sb');
    $('.package-details-body-wrapper').find('.services-wrapper div.vb').attr('class', 'inactive vb');
    $('.package-details-body-wrapper').find('.services-wrapper div.bb').attr('class', 'inactive bb');
    $('.package-details-body-wrapper').find('.services-wrapper div.utrack').attr('class', 'inactive utrack');
    $('.package-details-body-wrapper').find('.services-wrapper div.dr').attr('class', 'inactive dr');

    //$('#spanCurrentBill').html('');
    $('#spanAverageBill').html('');
    $('#spanLastBill').html('');

    $('#spanAverageBillValue').html('');
    $('#divSancAverageBill').html('');
    $('#divSancAverageBill2').html('');

    $('.profit-progress-track-2').css('width', '0px');
    $('.profit-current-status-2').css('left', '0px');

    $('.profit-progress-track-3').css('width', '0px');
    $('.profit-current-status-3').css('left', '0px');

    $('.profit-progress-track-4').css('width', '0px');
    $('.profit-current-status-4').css('left', '0px');

    $('#spanCurrentBillValue').html('');
    $('#spanLastBillValue').html('');

    $('#divSancTotalPayable').html('');
    $('#divSancCustomerBalance').html('');

    $('#divSancTotalPayable2').html('');
    $('#divSancCustomerBalance2').html('');

    $('#divSancCustomerDeposit').html('');
    $('#divSancLastPaidAmmnt').html('');

    $('#divSancWHT').html('');
    $('#divSancGST').html('');

    $('#divSancLineRent').html('');
    $('#divSancLastPaymentdate').html('');

    $('#divCurrentHandset').html('');
    $('#divCurrentHandset_bg').html('');

    $('#divPreviousHandset').html('');
    $('#divPreviousHandset_bg').html('');

    $('#divWithUfone').html('');
    $('#divBirthday').html('');
    $('#divWithUfone_bg').html('');
    $('#divBirthday_bg').html('');

    $('#divCallStatus').html('');
    $('#div_cli_anis_bg').html('');
    $('#div_punchedin_subnumber_bg').html('');

    $('#divCustomerName').html('');
    $('#divCustomerNumber').html('');

    $('#divCustomerRatingStars').html('');

    $('#divPackageName').html('');

    $('#divCustomerName_bg').html('');
    $('#divCustomerNumber_bg').html('');
    $('#divCustomerRatingStars_bg').html('');

    $('#divCustomerUserName').html('');
    $('#divPackageName_bg').html('');
    $('#divAddress').html('');

    $('#divCustomerMotherName').html('');
    $('#divCustomerFatherName').html('');
    $('#divCustomerCNIC').html('');
    $('#divCustomerServiceType').html('');
    $('#divSpecialInst').html('');

    $('#divCustomerIVRlang').html('');
    $('#divCustomerPUK').html('');
    $('#divCustomerStatus').html('');

    $('#divAgentHistory').html('');
    $('#divAgentHistory_bg').html('');

    $('#DNISDescriptionDiv').html('');
    $('#CDNDescriptionDiv').html('');

    $('#DNISDescriptionSpan').html('');
    $('#CDNDescriptionSpan').html('');


    $('#splashName span').html('');
    $('#splashNumber span').html('');
    $('#splashPackageDetails span').html('');

    $('ul.pagination-buttons li button').attr('class', 'inactive');

    $("#close-ivr-lightbox").mousedown();
    $("#close-consent-lightbox").mousedown();
    $("#close-customer-demo-lightbox").mousedown();
    $("#close-profit-sanc-lightbox").mousedown();
    $("#close-package-details-lightbox").mousedown();
    $("#close-latest-activities-lightbox").mousedown();

    resetUnicaCards();

    $("select#resegCondition option").removeAttr("disabled");


    $(".complaint-management-lightbox .small-complaints-panel-wrapper").html("");
    $(".cms-body-wrapper").html("");
    $(".complaint-management-lightbox .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .work-code-name span").html("");
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .date-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .details-01-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .remarks-wrapper .remarks-details-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .action-remarks-wrapper .details span").html("");
    $(".complaint-management-lightbox .history-wrapper .last-action-remarks-wrapper .details span").html("");
    $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#5a8800");
    $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("");
    $(".complaint-management-lightbox .history-wrapper .customer-complaint-details-wrapper").html("");

    $(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html('');

    $(".map-controls-wrapper").css("display", "none");

    $("#statsTimeStampSpan").html("");
    $("#firstLoginTimeStampSpan").html("");
    
    $("#isSuperCardUser").text("N/A");
    $("#superCardOnMinutes").text("N/A");
}

function resetButtons() {
    $('#btnReady').parent().find('div.label span').html('ready');
    resetButtonSelection($('#btnReady'));
    $('#btnNot_Ready').parent().find('div.label span').html('ready');
    resetButtonSelection($('#btnNot_Ready'));

    $('#btnNot_Ready').css('background-position-x', '-110px');

    $('#btnNot_Ready').attr('title', 'Ready');
    $('#btnNot_Ready').removeAttr('disabled');
    $('#btnNot_Ready').attr('id', 'btnReady');

    $('#btnHold').parent().find('div.label span').html('hold');
    resetButtonSelection($('#btnHold'));
    $('#btnUnHold').parent().find('div.label span').html('hold');
    resetButtonSelection($('#btnUnHold'));

    $('#btnUnHold').css('background-position-x', '0px');

    $('#btnUnHold').attr('title', 'Hold');
    $('#btnUnHold').removeAttr('disabled');
    $('#btnUnHold').attr('id', 'btnHold');

    $('#btnAnswer_Call').parent().find('div.label span').html('answer');
    resetButtonSelection($('#btnAnswer_Call'));
    $('#btnHang_up').parent().find('div.label span').html('answer');
    resetButtonSelection($('#btnHang_up'));

    $('#btnHang_up').css('background-position-x', '-110px');

    $('#btnHang_up').attr('title', 'Answer');
    $('#btnHang_up').removeAttr('disabled');
    $('#btnHang_up').attr('id', 'btnAnswer_Call');

}

function populateVAS() {
    var attachedData = clientObj.getAttachedData();
    //getBundleInfo(clientObj.getCallerMsisdn());
    if ($.isArray(attachedData)) {
        var vasArray = attachedData[19].split(',');
        //alert(vasArray);
        $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'inactive');
        if ($.inArray('GPRS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.gprs').attr('class', 'active gprs');
        }
        if ($.inArray('CONF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.conference').attr('class', 'active conference');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('SMS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.sms').attr('class', 'active sms');
        }
        if ($.inArray('CLI', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.cli').attr('class', 'active cli');
        }
        if ($.inArray('FWD', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.forward').attr('class', 'active forward');
        }
        if ($.inArray('MMS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.mms').attr('class', 'active mms');
        }
        if ($.inArray('WAIT', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.waiting').attr('class', 'active waiting');
        }

        if ($.inArray('RST', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.restrict').attr('class', 'active restrict');
        }
        if ($.inArray('VM ENGLISH', vasArray) != -1 || $.inArray('VM URDU', vasArray) != -1 || $.inArray('VM', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.vmail').attr('class', 'active vmail');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('WAP', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.wap').attr('class', 'active wap');
        }
        if ($.inArray('Umail', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.umail').attr('class', 'active umail');
        }
        if ($.inArray('FAX', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.fax').attr('class', 'active fax');
        }
        if ($.inArray('INFO SERVICE', vasArray) != -1 || $.inArray('Info Service', vasArray) != -1 || $.inArray('Info Service 60', vasArray) != -1 || $.inArray('Info Service 90', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.info').attr('class', 'active info');
        }
        if ($.inArray('CRBT', vasArray) != -1 || $.inArray('CRBT_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.crbt').attr('class', 'active crbt');
        }
        if ($.inArray('MCN', vasArray) != -1 || $.inArray('MCN_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.mcn').attr('class', 'active mcn');
        }
        if ($.inArray('SMS Bucket', vasArray) != -1 || $.inArray('BUCKET', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.sb').attr('class', 'active sb');
        }
        if ($.inArray('Voice Bucket', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.vb').attr('class', 'active vb');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('BB', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.bb').attr('class', 'active bb');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('UTrack', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.utrack').attr('class', 'active utrack');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('DR', vasArray) != -1 || $.inArray('DR_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.dr').attr('class', 'active dr');
        }

    }
}

function populateVASCares(attachedData) {
    if ($.isArray(attachedData) && attachedData.length > 1) {
        var vasArray = attachedData[13].split(',');
        //getBundleInfo(clientObj.getCallerMsisdn());
        $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'inactive');
        if ($.inArray('GPRS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.gprs').attr('class', 'active gprs');
        }
        if ($.inArray('CALL CONFERENCE', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.conference').attr('class', 'active conference');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('SMS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.sms').attr('class', 'active sms');
        }
        if ($.inArray('CLI', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.cli').attr('class', 'active cli');
        }
        if ($.inArray('CALL FORWARDING', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.forward').attr('class', 'active forward');
        }
        if ($.inArray('MMS', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.mms').attr('class', 'active mms');
        }
        if ($.inArray('CALL WAITING', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.waiting').attr('class', 'active waiting');
        }

        if ($.inArray('RST', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.restrict').attr('class', 'active restrict');
        }
        if ($.inArray('VM ENGLISH', vasArray) != -1 || $.inArray('VM URDU', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.vmail').attr('class', 'active vmail');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('WAP', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.wap').attr('class', 'active wap');
        }
        if ($.inArray('Umail', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.umail').attr('class', 'active umail');
        }
        if ($.inArray('FAX', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.fax').attr('class', 'active fax');
        }
        if ($.inArray('INFO SERVICE', vasArray) != -1 || $.inArray('Info Service', vasArray) != -1 || $.inArray('Info Service 60', vasArray) != -1 || $.inArray('Info Service 90', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.info').attr('class', 'active info');
        }
        if ($.inArray('CRBT', vasArray) != -1 || $.inArray('CRBT_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.crbt').attr('class', 'active crbt');
        }
        if ($.inArray('MCN', vasArray) != -1 || $.inArray('MCN_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.mcn').attr('class', 'active mcn');
        }
        if ($.inArray('SMS Bucket', vasArray) != -1 || $.inArray('BUCKET', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.sb').attr('class', 'active sb');
        }
        if ($.inArray('Voice Bucket', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.vb').attr('class', 'active vb');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('BB', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.bb').attr('class', 'active bb');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('UTrack', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.utrack').attr('class', 'active utrack');
            $('ul.pagination-buttons li button#vas_screen_active_btn').attr('class', 'active');
        }
        if ($.inArray('DR', vasArray) != -1 || $.inArray('DR_OFF', vasArray) != -1) {
            $('.package-details-body-wrapper').find('.services-wrapper div.dr').attr('class', 'active dr');
        }
    }
}

function populateProfitSanc() {
    switch (clientObj.getIpPhoneType()) {
        case "Nortel":
            var attachedData = clientObj.getAttachedData();
            if ($.isArray(attachedData)) {
                if (clientObj.getCustomertype() == "Post-paid") {
                    $('#spanCurrentBill').html('Current Bill');
                }
                else {
                    $('#spanCurrentBill').html('Current Balance');
                }

                $('#spanAverageBill').html('Avg Bill');
                $('#spanLastBill').html('Last Bill');


                //getAvgBills(clientObj.getCallerMsisdn());
                //getSumBills(clientObj.getCallerMsisdn());

                if (clientObj.getCustomertype() == "Post-paid") {
                    $('#spanCurrentBillValue').html('Rs. ' + attachedData[23]);
                }
                else {
                    $('#spanCurrentBillValue').html('Rs. ' + attachedData[11]);
                }

                $('#spanLastBillValue').html('Rs. ' + attachedData[30]);

                $('#divSancTotalPayable').html('<span class="regular-lucida-yellow">Total Payable: </span><span class="regular-body-white-bold">Rs. ' + attachedData[27] + '</span>');
                $('#divSancCustomerBalance').html('<span class="regular-lucida-yellow">Customer Balance: </span><span class="regular-body-white-bold">Rs. ' + attachedData[11] + '</span>');

                $('#divUnBilledAmnt').html('<span class="regular-lucida-yellow">Unbilled Amount: </span><span class="regular-body-white-bold">Rs. ' + attachedData[23] + '</span>');
                $('#divUsedLimit').html('<span class="regular-lucida-yellow">Used Limit: </span><span class="regular-body-white-bold">' + attachedData[28] + '%</span>');
                $('#divTotalLimit').html('<span class="regular-lucida-yellow">Total Limit: </span><span class="regular-body-white-bold">Rs. ' + attachedData[32] + '</span>');

                $('#divSancCustomerDeposit').html('<span class="regular-lucida-yellow">Customer Deposit: </span><span class="regular-body-white-bold">Rs. ' + attachedData[29] + '</span>');
                $('#divSancLastPaidAmmnt').html('<span class="regular-lucida-yellow">Last Paid Amount: </span><span class="regular-body-white-bold">Rs. ' + attachedData[30] + '</span>');

                $('#divSancWHT').html('<span class="regular-lucida-yellow">WHT: </span><span class="regular-body-white-bold">Rs. ' + attachedData[26] + '</span>');
                $('#divSancGST').html('<span class="regular-lucida-yellow">GST: </span><span class="regular-body-white-bold">Rs. ' + attachedData[25] + '</span>');

                $('#divSancLineRent').html('<span class="regular-lucida-yellow">Line Rent: </span><span class="regular-body-white-bold">Rs. ' + attachedData[24] + '</span>');
                if (clientObj.getCustomertype() == "Post-paid") {
                    $('#divSancLastPaymentdate').html('<span class="regular-lucida-yellow">Last Payment Date: </span><span class="regular-body-white-bold">' + attachedData[31] + '</span>');
                }
                else {
                    $('#divSancLastPaymentdate').html('<span class="regular-lucida-yellow">Expiry Date: </span><span class="regular-body-white-bold">' + attachedData[12] + '</span>');
                }
            }
            break;
        case "Cisco_LHR":
            var caresArray = clientObj.getCaresCustomerInfo();
            populateProfitSancCares(caresArray);
            break;
        case "Cisco_ISB":
            var caresArray = clientObj.getCaresCustomerInfo();
            populateProfitSancCares(caresArray);
            break;
        case "Cisco_KHI":
            var caresArray = clientObj.getCaresCustomerInfo();
            populateProfitSancCares(caresArray);
            break;
    }


}

function populateProfitSancCares(attachedData) {

    if ($.isArray(attachedData) && attachedData.length > 1)
    {

        if (clientObj.getCustomertype() == "Post-paid") {
            $('#spanCurrentBill').html('Current Bill');
        }
        else {
            $('#spanCurrentBill').html('Current Balance');
        }

        $('#spanAverageBill').html('Avg Bill');
        $('#spanLastBill').html('Last Bill');

        //getAvgBills(clientObj.getCallerMsisdn());
        //getSumBills(clientObj.getCallerMsisdn());

        if (clientObj.getCustomertype() == "Post-paid") {
            $('#spanCurrentBillValue').html('Rs. ' + attachedData[18]);
        }
        else {
            $('#spanCurrentBillValue').html('Rs. ' + attachedData[17]);
        }
        $('#spanLastBillValue').html('Rs. ' + attachedData[25]);

        $('#divSancTotalPayable').html('<span class="regular-lucida-yellow">Total Payable: </span><span class="regular-body-white-bold">Rs. ' + attachedData[22] + '</span>');
        $('#divSancCustomerBalance').html('<span class="regular-lucida-yellow">Customer Balance: </span><span class="regular-body-white-bold">Rs. ' + attachedData[17] + '</span>');

        $('#divUnBilledAmnt').html('<span class="regular-lucida-yellow">Unbilled Amount: </span><span class="regular-body-white-bold">Rs. ' + attachedData[18] + '</span>');
        $('#divUsedLimit').html('<span class="regular-lucida-yellow">Used Limit: </span><span class="regular-body-white-bold">' + attachedData[23] + '%</span>');
        $('#divTotalLimit').html('<span class="regular-lucida-yellow">Total Limit: </span><span class="regular-body-white-bold">Rs. ' + attachedData[27] + '</span>');

        $('#divSancCustomerDeposit').html('<span class="regular-lucida-yellow">Customer Deposit: </span><span class="regular-body-white-bold">Rs. ' + attachedData[24] + '</span>');
        $('#divSancLastPaidAmmnt').html('<span class="regular-lucida-yellow">Last Paid Amount: </span><span class="regular-body-white-bold">Rs. ' + attachedData[25] + '</span>');

        $('#divSancWHT').html('<span class="regular-lucida-yellow">WHT: </span><span class="regular-body-white-bold">Rs. ' + attachedData[21] + '</span>');
        $('#divSancGST').html('<span class="regular-lucida-yellow">GST: </span><span class="regular-body-white-bold">Rs. ' + attachedData[20] + '</span>');

        $('#divSancLineRent').html('<span class="regular-lucida-yellow">Line Rent: </span><span class="regular-body-white-bold">Rs. ' + attachedData[19] + '</span>');
        if (clientObj.getCustomertype() == "Post-paid") {
            $('#divSancLastPaymentdate').html('<span class="regular-lucida-yellow">Last Payment Date: </span><span class="regular-body-white-bold">' + attachedData[26] + '</span>');
        }
        else {
            $('#divSancLastPaymentdate').html('<span class="regular-lucida-yellow">Expiry Date: </span><span class="regular-body-white-bold">' + attachedData[15] + '</span>');
        }
    }
}

function populateCustomerDemo() {
    //getPreviousHandSet(clientObj.getCallerMsisdn());
    //getCustomerDates(clientObj.getCallerMsisdn());

    switch (clientObj.getIpPhoneType()) {
        case "Nortel":
            caresHandSetInfo(clientObj.getCallerMsisdn());
            var attachedData = clientObj.getAttachedData();
            if ($.isArray(attachedData)) {
                var customer_name = attachedData[5];
                if (customer_name.length > 19) {
                    customer_name = customer_name.substring(0, 15) + '...';
                }

                $('#divCustomerName').html('<span title="' + attachedData[5] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                //$('#divCustomerNumber').html('<span class="regular-lucida-italic">0'+attachedData[2]+'</span>');
                var tmp_msisdn = clientObj.getCallerMsisdn();

                if (starts_with('9292', tmp_msisdn) || starts_with('00', tmp_msisdn)) {
                    //$('#divCustomerNumber').html('<span class="regular-lucida-italic">'+tmp_msisdn+'</span>');
                    //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">'+tmp_msisdn+'</span>');
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attachedData[2] + '</span>');
                }
                else if (starts_with('92', tmp_msisdn)) {
                    if (tmp_msisdn.length == 12 && starts_with('923', tmp_msisdn)) {
                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
                        $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1] + '</span>');
                        //$('#divCustomerNumber').html('<span class="regular-lucida-italic">0'+tmp_msisdn.substring(2,tmp_msisdn.length)+'</span>');
                        //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0'+attachedData[2]+'</span>');

                    }
                    else {
                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        //$('#divCustomerNumber').html('<span class="regular-lucida-italic">0'+tmp_msisdn.substring(2,tmp_msisdn.length)+'</span>');
                        //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0'+attachedData[2]+'</span>');

                    }
                }
                else {
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
                }

                $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[5] + '</span>');
                //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0'+attachedData[2]+'</span>');

                var star_rating = attachedData[6];
                var starRatingInnerHtml = '';
                for (var i = 0; i < 5; i++) {
                    if (i < star_rating) {
                        starRatingInnerHtml += '<div class="active-star"></div>\n';
                    }
                    else {
                        starRatingInnerHtml += '<div class="inactive-star"></div>\n';
                    }
                }
                $('#divCustomerRatingStars').html(starRatingInnerHtml);

                $('#divPackageName').html('<span class="regular-body-lucida-light-grey">' + attachedData[7] + '</span>');

                $('#divCustomerRatingStars_bg').html(starRatingInnerHtml);

                $('#divCustomerUserName').html('<span class="regular-lucida-yellow">Customer Username: </span><span class="regular-body-white-bold">' + attachedData[13] + '</span>');
                $('#divPackageName_bg').html('<span class="regular-lucida-yellow">Customer Package: </span><span class="regular-body-white-bold">' + attachedData[7] + '</span>');
                $('#divAddress').html('<span class="regular-lucida-yellow">Customer Address: </span><span class="regular-body-white-bold">' + attachedData[35] + '' + attachedData[36] + '</span>');

                $('#divCustomerMotherName').html('<span class="regular-lucida-yellow">Customer Mother Name: </span><span class="regular-body-white-bold">' + attachedData[15] + '</span>');
                $('#divCustomerFatherName').html('<span class="regular-lucida-yellow">Customer Father Name: </span><span class="regular-body-white-bold">' + attachedData[16] + '</span>');
                $('#divCustomerCNIC').html('<span class="regular-lucida-yellow">Customer CNIC: </span><span class="regular-body-white-bold">' + attachedData[14] + '</span>');
                $('#divCustomerServiceType').html('<span class="regular-lucida-yellow">Service Type: </span><span class="regular-body-white-bold">' + attachedData[9] + '</span>');
                $('#divSpecialInst').html('<span class="regular-lucida-yellow">Special Instructions: </span><span class="regular-body-white-bold">' + attachedData[38] + '</span>');

                var ivr_lang = "";
                if (attachedData[17] == '1') {
                    ivr_lang = "English";
                }
                else if (attachedData[17] == '2') {
                    ivr_lang = "Urdu";
                }
                else if (attachedData[17] == '4') {
                    ivr_lang = "Pushto";
                }
                else if (attachedData[17] == '5') {
                    ivr_lang = "Sindhi";
                }
                else if (attachedData[17] == '7') {
                    ivr_lang = "Saraiki";
                }

                $('#divCustomerIVRlang').html('<span class="regular-lucida-yellow">Customer IVR Language: </span><span class="regular-body-white-bold">' + ivr_lang + '</span>');
                $('#divCustomerPUK').html('<span class="regular-lucida-yellow">Customer PUK Code: </span><span class="regular-body-white-bold">' + attachedData[18] + '</span>');
                $('#divCustomerStatus').html('<span class="regular-lucida-yellow">Customer Status: </span><span class="regular-body-white-bold">' + attachedData[22] + '</span>');
            }
            break;

        case "Cisco_LHR":
            var caresArray = clientObj.getCaresCustomerInfo();
            var attData = (clientObj.getAttachedData());
            populateCustomerDemoCares(caresArray);
            var tmp_msisdn = clientObj.getCallerMsisdn();
            if (!clientObj.getNDFCall())
            {
                var customer_name = attData[7];
                if (customer_name.length > 19) {
                    customer_name = customer_name.substring(0, 15) + '...';
                }
                $('#divCustomerName').html('<span title="' + attData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attData[7] + '</span>');
            }
            if (starts_with('9292', tmp_msisdn) || starts_with('00', tmp_msisdn)) {
                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
            }
            else {
                if (attData[2].length < 5) {
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                }
                else {
                    if ($.trim(attData[1]).length == 0) {
                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    }
                    else {

                        if (starts_with('0', attData[2]) && attData[2].length == 11) {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        } else {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        }

                    }
                }
            }
            break;

        case "Cisco_ISB":
            var caresArray = clientObj.getCaresCustomerInfo();
            var attData = (clientObj.getAttachedData());
            populateCustomerDemoCares(caresArray);
            var tmp_msisdn = clientObj.getCallerMsisdn();
            if (!clientObj.getNDFCall())
            {
                var customer_name = attData[7];
                if (customer_name.length > 19) {
                    customer_name = customer_name.substring(0, 15) + '...';
                }
                $('#divCustomerName').html('<span title="' + attData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attData[7] + '</span>');
            }
            if (starts_with('9292', tmp_msisdn) || starts_with('00', tmp_msisdn)) {
                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
            }
            else {
                if (attData[2].length < 5) {
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                }
                else {
                    if ($.trim(attData[1]).length == 0) {
                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    }
                    else {

                        if (starts_with('0', attData[2]) && attData[2].length == 11) {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        } else {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        }

                    }
                }
            }
            break;

        case "Cisco_KHI":
            var caresArray = clientObj.getCaresCustomerInfo();
            var attData = (clientObj.getAttachedData());
            populateCustomerDemoCares(caresArray);
            var tmp_msisdn = clientObj.getCallerMsisdn();
            if (!clientObj.getNDFCall())
            {
                var customer_name = attData[7];
                if (customer_name.length > 19) {
                    customer_name = customer_name.substring(0, 15) + '...';
                }
                $('#divCustomerName').html('<span title="' + attData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attData[7] + '</span>');
            }
            if (starts_with('9292', tmp_msisdn) || starts_with('00', tmp_msisdn)) {
                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
            }
            else {
                if (attData[2].length < 5) {
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                }
                else {
                    if ($.trim(attData[1]).length == 0) {
                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    }
                    else {
                        if (starts_with('0', attData[2]) && attData[2].length == 11) {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        } else {
                            if (_call_punchedin_subnumber === _call_cli_anis) {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[2] + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            } else {
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            }
                        }
                    }
                }
            }
            break;

    }
}

function populateCustomerDemoCares(attachedData) {
    caresHandSetInfo(clientObj.getCallerMsisdn());
    //getPreviousHandSet(clientObj.getCallerMsisdn());
    //getCustomerDates(clientObj.getCallerMsisdn());
    console.log("Customer demographic info: "+attachedData);
    if ($.isArray(attachedData) && attachedData.length > 1)
    {
        var customer_name = attachedData[1];
        if (customer_name.length > 19) {
            customer_name = customer_name.substring(0, 15) + '...';
        }

        $('#divCustomerName').html('<span title="' + attachedData[1] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');

        //$('#divCustomerNumber').html('<span class="regular-lucida-italic">0'+attachedData[0]+'</span>');
        var tmp_msisdn = clientObj.getCallerMsisdn();
        var attData = (clientObj.getAttachedData());

        if (starts_with('9292', tmp_msisdn) || starts_with('00', tmp_msisdn)) {
            //$('#divCustomerNumber').html('<span class="regular-lucida-italic">'+tmp_msisdn+'</span>');
            //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">'+tmp_msisdn+'</span>');
            $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
        }
        else if (starts_with('92', tmp_msisdn)) {
            switch (clientObj.getIpPhoneType()) {
                case "Nortel":
                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                    break;
                case "Cisco_LHR":
                    if ($.trim(attData[1]).length == 0) {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    else {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    break;
                case "Cisco_ISB":
                    if ($.trim(attData[1]).length == 0) {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    else {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    break;
                case "Cisco_KHI":
                    if ($.trim(attData[1]).length == 0) {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + tmp_msisdn.substring(2, tmp_msisdn.length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    else {
                        if (_call_punchedin_subnumber === _call_cli_anis) {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attData[1].substring(2, attData[1].length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0' + attData[1].substring(2, attData[1].length) + '</span>');
                        } else {
                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        }
                    }
                    break;
            }
        }
        else {
            $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + tmp_msisdn + '</span>');
            $('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">' + tmp_msisdn + '</span>');
        }

        $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[1] + '</span>');
        //$('#divCustomerNumber_bg').html('<span class="h2-lucida-bold-white">0'+attachedData[0]+'</span>');

        var star_rating = attachedData[7];
        var starRatingInnerHtml = '';
        for (var i = 0; i < 5; i++) {
            if (i < star_rating) {
                starRatingInnerHtml += '<div class="active-star"></div>\n';
            }
            else {
                starRatingInnerHtml += '<div class="inactive-star"></div>\n';
            }
        }
        $('#divCustomerRatingStars').html(starRatingInnerHtml);

        $('#divPackageName').html('<span class="regular-body-lucida-light-grey">' + attachedData[11] + '</span>');

        $('#divCustomerRatingStars_bg').html(starRatingInnerHtml);

        $('#divCustomerUserName').html('<span class="regular-lucida-yellow">Customer Username: </span><span class="regular-body-white-bold">' + attachedData[2] + '</span>');
        $('#divPackageName_bg').html('<span class="regular-lucida-yellow">Customer Package: </span><span class="regular-body-white-bold">' + attachedData[11] + '</span>');
        $('#divAddress').html('<span class="regular-lucida-yellow">Customer Address: </span><span class="regular-body-white-bold">' + attachedData[28] + '</span>');

        $('#divCustomerMotherName').html('<span class="regular-lucida-yellow">Customer Mother Name: </span><span class="regular-body-white-bold">' + attachedData[5] + '</span>');
        $('#divCustomerFatherName').html('<span class="regular-lucida-yellow">Customer Father Name: </span><span class="regular-body-white-bold">' + attachedData[6] + '</span>');
        $('#divCustomerCNIC').html('<span class="regular-lucida-yellow">Customer CNIC: </span><span class="regular-body-white-bold">' + attachedData[4] + '</span>');
        $('#divCustomerServiceType').html('<span class="regular-lucida-yellow">Service Type: </span><span class="regular-body-white-bold">' + attachedData[10] + '</span>');
        $('#divSpecialInst').html('<span class="regular-lucida-yellow">Special Instructions: </span><span class="regular-body-white-bold">' + 'N/A' + '</span>');

        var ivr_lang = "";
        if (attachedData[8] == '6') {//=='1' changed by qaseem for PIA Case
            ivr_lang = "English";
        }
        else if (attachedData[8] == '3') {//=='2' changed by qaseem for PIA Case
            ivr_lang = "Urdu";
        }
        else if (attachedData[8] == '4') {
            ivr_lang = "Pushto";
        }
        else if (attachedData[8] == '5') {
            ivr_lang = "Sindhi";
        }
        else if (attachedData[8] == '7') {
            ivr_lang = "Saraiki";
        }

        $('#divCustomerIVRlang').html('<span class="regular-lucida-yellow">Customer IVR Language: </span><span class="regular-body-white-bold">' + ivr_lang + '</span>');
        $('#divCustomerPUK').html('<span class="regular-lucida-yellow">Customer PUK Code: </span><span class="regular-body-white-bold">' + attachedData[9] + '</span>');
        $('#divCustomerStatus').html('<span class="regular-lucida-yellow">Customer Status: </span><span class="regular-body-white-bold">' + attachedData[16] + '</span>');

        $('#splashName span').html(attachedData[1]);
        $('#splashNumber span').html('0' + attachedData[0]);
        $('#splashPackageDetails span').html(attachedData[11]);
    }
}

function resetUnicaCards() {

    $('#unicaOfferLoadImage').css('display', 'none');
    $('.cards-wrapper').html("");

    last_card_clicked = 0;

}

function populateUnicaCards() {
    var unicaOfferName = "";
    var unicaOfferDesc = "";
    var unicaOfferScore = "";
    var unicaOfferTreatmentCode = "";
    var unicaOfferCode = "";
    var unicaOfferType = "";
    var unicaAPID = "";
    var unicaNoOfSMS = "";
    var unicaSMSValidity = "";
    var unicaColor = "";
    var unicaValidity = "";
    var unicaOfferCharges = "";
    var unicaResegmentFlag = "";
    var unicaFlowchartName = "";
    var unicaEventName = "";

    /*
     $("div.card-action-btns-wrapper").hide();
     $("div.card-heading-text span").html("----");
     */

    resetUnicaCards();

    var cardsToShow = clientObj.getCardsToShow();

    if ($.isArray(unicaOfferArray) && $.trim(unicaOfferArray[0]) != "||||||||||||,,|,,|" && unicaOfferArray.length > 0) {
        var i = null;
        //for(i = unicaOfferArrayIndex; i < unicaOfferArrayIndex + 4 && i < unicaOfferArray.length; i++){

        var cardsHtml = "";
        for (i = 0; i < unicaOfferArray.length; i++) {

            unicaOfferName = unicaOfferArray[i].split("|")[0];
            unicaOfferDesc = unicaOfferArray[i].split("|")[1];
            unicaOfferScore = unicaOfferArray[i].split("|")[2];
            unicaOfferTreatmentCode = unicaOfferArray[i].split("|")[3];
            unicaOfferCode = unicaOfferArray[i].split("|")[4];
            unicaOfferType = unicaOfferArray[i].split("|")[5];
            unicaAPID = unicaOfferArray[i].split("|")[6];
            unicaNoOfSMS = unicaOfferArray[i].split("|")[7];
            unicaSMSValidity = unicaOfferArray[i].split("|")[8];
            unicaColor = unicaOfferArray[i].split("|")[9];
            unicaValidity = unicaOfferArray[i].split("|")[10];
            unicaOfferCharges = unicaOfferArray[i].split("|")[11];
            unicaResegmentFlag = unicaOfferArray[i].split("|")[12];
            unicaFlowchartName = unicaOfferArray[i].split("|")[13];
            unicaEventName = unicaOfferArray[i].split("|")[14];

            //$("div.offers-card div.header").hide();
            if (i == 0) {
                cardsHtml += '<div class="offers-card-first" id="card-color-' + getUnicaCardColor(unicaColor) + '" contextmenu="' + parseInt(parseInt(i) + parseInt(1)) + '" cardposition="' + parseInt(parseInt(i) + parseInt(1)) + '" style="top: 0px;">';
            }
            else {
                if (i <= parseInt(cardsToShow) - 1) {
                    cardsHtml += '<div class="offers-card" id="card-color-' + getUnicaCardColor(unicaColor) + '" contextmenu="' + parseInt(parseInt(i) + parseInt(1)) + '" cardposition="' + parseInt(parseInt(i) + parseInt(1)) + '" style="top: 0px;">';
                }
                //				else if(cardsToShow == 3){
                //					cardsHtml += '<div class="offers-card" id="card-color-'+getUnicaCardColor(unicaColor)+'" contextmenu="'+parseInt(parseInt(i)+parseInt(1))+'">';
                //				}
                //				else if(cardsToShow == 4){
                //					cardsHtml += '<div class="offers-card" id="card-color-'+getUnicaCardColor(unicaColor)+'" contextmenu="'+parseInt(parseInt(i)+parseInt(1))+'">';
                //				}
                else {
                    cardsHtml += '<div class="offers-card" id="card-disabled" contextmenu="' + parseInt(parseInt(i) + parseInt(1)) + '" cardposition="' + parseInt(parseInt(i) + parseInt(1)) + '" style="top: 0px;">';
                }
            }
            cardsHtml += '<div class="header">';
            cardsHtml += '<div class="orange-card-icon"></div>';
            //			if(unicaOfferType == "1"){
            //				cardsHtml += '<div class="red-card-icon"></div>';
            //			}
            //			else if(unicaOfferType == "2"){
            //				cardsHtml += '<div class="blue-card-icon"></div>';
            //			}
            //			else if(unicaOfferType == "3"){
            //				cardsHtml += '<div class="orange-card-icon"></div>';
            //			}
            //			else{
            //				cardsHtml += '<div class="orange-card-icon"></div>';
            //			}
            var tmp_unicaOfferName = unicaOfferName;
            if (unicaOfferName.length > 26) {
                tmp_unicaOfferName = unicaOfferName.substring(0, 23) + '...';
            }
            cardsHtml += '<div class="card-heading-text" title="' + unicaOfferName.replace(/"/g, '&quot;') + '">';
            cardsHtml += '<span class="regular-body-lucida-uppercase-white">' + tmp_unicaOfferName + '</span>';
            cardsHtml += '</div>';
            cardsHtml += '<input class="inputUnicaCode" type="hidden" value="' +
                    unicaOfferTreatmentCode + "|" +
                    unicaOfferCode + "|" +
                    unicaOfferType + "|" +
                    unicaAPID + "|" +
                    unicaNoOfSMS + "|" +
                    unicaSMSValidity + "|" +
                    unicaColor + "|" +
                    unicaValidity + "|" +
                    unicaOfferCharges + "|" +
                    unicaResegmentFlag + "|" +
                    unicaFlowchartName + "|" +
                    clientObj.getCallSessionID() + '" />';
            cardsHtml += '<div class="card-action-btns-wrapper">';
            cardsHtml += '<div class="sep"></div>';
            cardsHtml += '<div class="buttons">';
            cardsHtml += '<button class="cards-accept-btn" title="accept" id="consolidated-accept"></button>';
            cardsHtml += '<button class="cards-pending-btn" title="interested" id="consolidated-pending"></button>';
            cardsHtml += '<button class="cards-reject-btn" title="reject" id="consolidated-reject"></button>';
            cardsHtml += '</div>';
            cardsHtml += '</div>';
            cardsHtml += '</div>';
            var tmp_unicaOfferDesc = unicaOfferDesc;
            if (unicaOfferDesc.length > 256) {
                tmp_unicaOfferDesc = unicaOfferDesc.substring(0, 253) + '...';
            }
            cardsHtml += '<div style="padding: 8px 0 0 0;overflow: hidden;">';
            cardsHtml += '<div style="overflow: hidden;padding: 0 12px;">';
            cardsHtml += '<span class="spanCardDesc regular-body-lucida-uppercase-white" style="margin: 10px;word-wrap: break-word;" title="' + unicaOfferDesc.replace(/"/g, '&quot;') + '">';
            cardsHtml += tmp_unicaOfferDesc;
            cardsHtml += '</span>';
            cardsHtml += '</div>';
            cardsHtml += '</div>';
            cardsHtml += '<div class="body">';
            cardsHtml += '</div>';
            cardsHtml += '</div>';

        }
        $('.cards-wrapper').html(cardsHtml);
        //unicaOfferArrayIndex = i;
        /*
         for (i = 1; i <= unicaOfferArray.length; i++) {
         var divObj = $('div.cards-wrapper div[contextmenu=\"' + i + '\"]');
         $(divObj).find('.orange-card-icon').first().hide();
         $(divObj).find('.card-heading-text').first().hide();
         $(divObj).find('.card-action-btns-wrapper').first().hide();			
         }
         */

        $('div.cards-wrapper').find('.red-card-icon').hide();
        $('div.cards-wrapper').find('.blue-card-icon').hide();
        $('div.cards-wrapper').find('.orange-card-icon').hide();
        $('div.cards-wrapper').find('.card-heading-text').hide();
        $('div.cards-wrapper').find('.card-action-btns-wrapper').hide();
        $('div.cards-wrapper').find('span.spanCardDesc').css("display", "none");

        for (i = 1; i <= cardsToShow; i++) {
            var divObj = $('div.cards-wrapper div[contextmenu=\"' + i + '\"]');
            $(divObj).find('.red-card-icon').first().show();
            $(divObj).find('.blue-card-icon').first().show();
            $(divObj).find('.orange-card-icon').first().show();
            $(divObj).find('.card-heading-text').first().show();
            $(divObj).find('.card-action-btns-wrapper').first().show();
            if (clientObj.getCardsToFetch() == 1 || i == 1) {
                $(divObj).find('span.spanCardDesc').css("display", "");
            }
        }
        $(".show_all_cards").click();
    }
}

function getUnicaCardColor(unicaColor) {
    var toReturn = "";
    if (unicaColor == "orange") {
        toReturn = 1;
    }
    else if (unicaColor == "red") {
        toReturn = 2;
    }
    else if (unicaColor == "light blue") {
        toReturn = 3;
    }
    else if (unicaColor == "deep blue") {
        toReturn = 4;
    }
    else if (unicaColor == "yellow") {
        toReturn = 5;
    }
    else if (unicaColor == "lime") {
        toReturn = 6;
    }
    else if (unicaColor == "light brown") {
        toReturn = 7;
    }
    else if (unicaColor == "deep brown") {
        toReturn = 8;
    }
    else {
        toReturn = 1;
    }
    return toReturn;
}
