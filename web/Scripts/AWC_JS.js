var ACW_XML = null;
var IVR_XML = null;
var freqCodesXML = null;
var ACW_idArray = new Array();

var acw_ivr_transfered = false;

function ivrSearchKeyPress(obj, evt) {
	var tmp_html = '';
	var tmp_html2 = '';
	var tmp_html3 = '';
	var toSearch = $('#search_ivr').val();
	var title_txt = '';
	//if($.trim(toSearch).length > 0)
	{
		if (ivr_click_state == 1) {
			for (var i = 0; i < IVR_tmpArray.length; i++) {
				if (IVR_tmpArray[i].toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
					if (IVR_tmpArray[i].length > 15) {
						title_txt = 'title="' + IVR_tmpArray[i] + '"';
					}
					else {
						title_txt = '';
					}
					tmp_html += '<option value="' + IVR_tmpArray[i] + '" ' + title_txt + ' >' + IVR_tmpArray[i] + '</option>\n';
				}
			}
			$('#IVRTransSubCat').html('');
			$('#IVRTransSubCat').html(tmp_html);
		}
		else if (ivr_click_state == 2) {
			if (IVR_tmpArray2.length > 0)
			{
				tmp_html2 += '<select id="IVRTransMenuValue" class="select-box-generic" style="width: 335px;" size="2" >\n';

				for (var i = 0; i < IVR_tmpArray2.length; i++) {
					if (IVR_tmpArray2[i].split(',')[1].toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
						if (IVR_tmpArray2[i].split(',')[1].length > 15) {
							title_txt = 'title="' + IVR_tmpArray2[i].split(',')[1] + '"';
						}
						else {
							title_txt = '';
						}
						tmp_html2 += '<option value="' + IVR_tmpArray2[i].split(',')[0] + '" ' + title_txt + '>' + IVR_tmpArray2[i].split(',')[1] + '</option>\n';
					}
				}
				tmp_html2 += "</select>";
				$('#IVRTransMenu').html('');
				$('#IVRTransMenu').html(tmp_html2);
			}
		}
		else if (ivr_click_state == 3) {
			if (toSearch.length > 0) {
				tmp_html3 += '<select id="IVRTransMenuValue" class="select-box-generic" style="width: 335px;" size="2" >\n';

				$(IVR_XML).find('ivrname').each(function() {
					if ($(this).find('menudesc').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
						if ($(this).find('menudesc').text().length > 15) {
							title_txt = 'title="' + $(this).find('menudesc').text() + '"';
						}
						else {
							title_txt = '';
						}
						tmp_html3 += '<option value="' + $(this).find('menucode').text() + '" ' + title_txt + '>' + $(this).find('menudesc').text() + '</option>\n';
					}
				});
				tmp_html3 += "</select>";
				$('#IVRTransMenu').html('');
				$('#IVRTransMenu').html(tmp_html3);
			}
			else {
				$('#IVRTransMenu').html('');
			}
		}
	}
}

function acwSearchKeyPress(obj, evt) {
	var tmp_html = '';
	var toSearch = $('#search_acw').val();
	if (awc_click_state == 2)
	{
//		var title_txt = '';
//		for (var i = 0; i < ACW_tmpArray.length; i++) {
//			if (ACW_tmpArray[i].toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
//				if (ACW_tmpArray[i].length > 15) {
//					title_txt = 'title="' + ACW_tmpArray[i] + '"';
//				}
//				else {
//					title_txt = '';
//				}
//				tmp_html += '<option value="' + ACW_tmpArray[i] + '" ' + title_txt + '>' + ACW_tmpArray[i] + '</option>\n';
//			}
//		}
//		$('#acwListCodeName').html('');
//		$('#acwListCodeName').html(tmp_html);

		if (toSearch.length > 0) {
			var title_txt = '';
			var color_txt = '';
			var ivrcode = '';
			var wctype = '';
			$(ACW_XML).find('wname').each(function() {
				$(this).find('wctype').each(function() {
					wctype = $(this).text();
				});
				if (starts_with('pre', clientObj.getProductType())) {
					if (wctype == 'prepaid') {
						if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
							if ($(this).find('name').text().length > 15) {
								title_txt = 'title="' + $(this).find('name').text() + '"';
							}
							else {
								title_txt = '';
							}
							if ($(this).find('sms').text() == '1') {
								color_txt = 'color: #FFD200';
							}
							else {
								color_txt = '';
							}
							ivrcode = $(this).find('ivrcode').text();
							if (ivrcode != 'null' && ivrcode != null) {
								if (color_txt == '') {
									color_txt = 'color: #FF0000';
								}
								else {
									color_txt = 'color: #00FF00';
								}
							}
							tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
						}
					}
				} else if (starts_with('post', clientObj.getProductType())) {
					if (wctype == 'postpaid') {
						if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
							if ($(this).find('name').text().length > 15) {
								title_txt = 'title="' + $(this).find('name').text() + '"';
							}
							else {
								title_txt = '';
							}
							if ($(this).find('sms').text() == '1') {
								color_txt = 'color: #FFD200';
							}
							else {
								color_txt = '';
							}
							ivrcode = $(this).find('ivrcode').text();
							if (ivrcode != 'null' && ivrcode != null) {
								if (color_txt == '') {
									color_txt = 'color: #FF0000';
								}
								else {
									color_txt = 'color: #00FF00';
								}
							}
							tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
						}
					}
				} else {
					if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
						if ($(this).find('name').text().length > 15) {
							title_txt = 'title="' + $(this).find('name').text() + '"';
						}
						else {
							title_txt = '';
						}
						if ($(this).find('sms').text() == '1') {
							color_txt = 'color: #FFD200';
						}
						else {
							color_txt = '';
						}
						ivrcode = $(this).find('ivrcode').text();
						if (ivrcode != 'null' && ivrcode != null) {
							if (color_txt == '') {
								color_txt = 'color: #FF0000';
							}
							else {
								color_txt = 'color: #00FF00';
							}
						}
						tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
					}
				}
			});
			$('#acwListCodeName').html('');
			$('#acwListCodeName').html(tmp_html);
		}
		else {
			$('#acwListCodeName').html('');
		}

	}
	else if (awc_click_state == 1) {
		if (toSearch.length > 0) {
			var title_txt = '';
			var color_txt = '';
			var ivrcode = '';
			var wctype = '';
			$(ACW_XML).find('wname').each(function() {
				$(this).find('wctype').each(function() {
					wctype = $(this).text();
				});
				if (starts_with('pre', clientObj.getProductType())) {
					if (wctype == 'prepaid') {
						if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
							if ($(this).find('name').text().length > 15) {
								title_txt = 'title="' + $(this).find('name').text() + '"';
							}
							else {
								title_txt = '';
							}
							if ($(this).find('sms').text() == '1') {
								color_txt = 'color: #FFD200';
							}
							else {
								color_txt = '';
							}
							ivrcode = $(this).find('ivrcode').text();
							if (ivrcode != 'null' && ivrcode != null) {
								if (color_txt == '') {
									color_txt = 'color: #FF0000';
								}
								else {
									color_txt = 'color: #00FF00';
								}
							}
							tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
						}
					}
				} else if (starts_with('post', clientObj.getProductType())) {
					if (wctype == 'postpaid') {
						if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
							if ($(this).find('name').text().length > 15) {
								title_txt = 'title="' + $(this).find('name').text() + '"';
							}
							else {
								title_txt = '';
							}
							if ($(this).find('sms').text() == '1') {
								color_txt = 'color: #FFD200';
							}
							else {
								color_txt = '';
							}
							ivrcode = $(this).find('ivrcode').text();
							if (ivrcode != 'null' && ivrcode != null) {
								if (color_txt == '') {
									color_txt = 'color: #FF0000';
								}
								else {
									color_txt = 'color: #00FF00';
								}
							}
							tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
						}
					}
				} else {
					if ($(this).find('name').text().toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
						if ($(this).find('name').text().length > 15) {
							title_txt = 'title="' + $(this).find('name').text() + '"';
						}
						else {
							title_txt = '';
						}
						if ($(this).find('sms').text() == '1') {
							color_txt = 'color: #FFD200';
						}
						else {
							color_txt = '';
						}
						ivrcode = $(this).find('ivrcode').text();
						if (ivrcode != 'null' && ivrcode != null) {
							if (color_txt == '') {
								color_txt = 'color: #FF0000';
							}
							else {
								color_txt = 'color: #00FF00';
							}
						}
						tmp_html += '<option value="' + $(this).find('name').text() + '" ' + title_txt + ' style="' + color_txt + '">' + $(this).find('name').text() + '</option>\n';
					}
				}
			});
			$('#acwListCodeName').html('');
			$('#acwListCodeName').html(tmp_html);
		}
		else {
			$('#acwListCodeName').html('');
		}
	}
}


function populate_acwListHeadName() {
	var acwListHeadName_txt = '';
	var title_txt = '';
	$(ACW_XML).find('wcheadname').each(function() {
		if ($(this).text().length > 15) {
			title_txt = 'title="' + $(this).text() + '"';
		}
		else {
			title_txt = '';
		}
		acwListHeadName_txt += '<option value="' + $(this).text() + '" ' + title_txt + '>' + $(this).text() + '</option>' + '\n';
	});
	$('#acwListHeadName').html(acwListHeadName_txt);
}

var ACW_tmpArray = new Array();

function populate_acwListCodeName() {
	awc_click_state = 2;
	var acwListCodeName_txt = '';
	var selectedHeadName = $('#acwListHeadName').val();
	var sms = '';
	var wctype = '';
	var ivrcode = '';
	var highlightedStyle = '';
	var title_txt = '';
	ACW_tmpArray = new Array();
	$(ACW_XML).find('wcheadname').each(function() {
		if ($(this).text() == selectedHeadName) {
			$(this).parent().find('wname name').each(function() {
				$(this).parent().find('wctype').each(function() {
					wctype = $(this).text();
				});
				$(this).parent().find('ivrcode').each(function() {
					ivrcode = $(this).text();
				});
				if (starts_with('pre', clientObj.getProductType())) {
					if (wctype == 'prepaid') {
						$(this).parent().find('sms').each(function() {
							sms = $(this).text();
						});
						if (sms == '1') {
							highlightedStyle = 'color: #FFD200';
						}
						else {
							highlightedStyle = '';
						}
						if ($(this).text().length > 15) {
							title_txt = 'title="' + $(this).text() + '"';
						}
						else {
							title_txt = '';
						}
						if (ivrcode != 'null' && ivrcode != null) {
							if (highlightedStyle == '') {
								highlightedStyle = 'color: #FF0000';
							}
							else {
								highlightedStyle = 'color: #00FF00';
							}
						}
						acwListCodeName_txt += '<option style="' + highlightedStyle + '" value="' + $(this).text() + '" ' + title_txt + '>' + $(this).text() + '</option>' + '\n';
						ACW_tmpArray.push($(this).text());
					}
				}
				else if (starts_with('post', clientObj.getProductType())) {
					if (wctype == 'postpaid') {
						$(this).parent().find('sms').each(function() {
							sms = $(this).text();
						});
						if (sms == '1') {
							highlightedStyle = 'color: #FFD200';
						}
						else {
							highlightedStyle = '';
						}
						if ($(this).text().length > 15) {
							title_txt = 'title="' + $(this).text() + '"';
						}
						else {
							title_txt = '';
						}
						if (ivrcode != 'null' && ivrcode != null) {
							if (highlightedStyle == '') {
								highlightedStyle = 'color: #FF0000';
							}
							else {
								highlightedStyle = 'color: #00FF00';
							}
						}
						acwListCodeName_txt += '<option style="' + highlightedStyle + '" value="' + $(this).text() + '" ' + title_txt + '>' + $(this).text() + '</option>' + '\n';
						ACW_tmpArray.push($(this).text());
					}
				}
				else {
					$(this).parent().find('sms').each(function() {
						sms = $(this).text();
					});
					if (sms == '1') {
						highlightedStyle = 'color: #FFD200';
					}
					else {
						highlightedStyle = '';
					}
					if ($(this).text().length > 15) {
						title_txt = 'title="' + $(this).text() + '"';
					}
					else {
						title_txt = '';
					}
					if (ivrcode != 'null' && ivrcode != null) {
						if (highlightedStyle == '') {
							highlightedStyle = 'color: #FF0000';
						}
						else {
							highlightedStyle = 'color: #00FF00';
						}
					}
					acwListCodeName_txt += '<option style="' + highlightedStyle + '" value="' + $(this).text() + '" ' + title_txt + '>' + $(this).text() + '</option>' + '\n';
					ACW_tmpArray.push($(this).text());
				}

			});
		}
	});
	//$('#search_acw').focus();
	$('#acwListCodeName').html(acwListCodeName_txt);
}

function populate_acwSelectedName() {
	var acwSelectedName_txt = '';
	var selectedHeadName = $('#acwListHeadName').val();
	var selectedCodeName = $('#acwListCodeName').val();
	var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
	$('#acwDesc').empty();
	var acwDescDiv = $('#acwDesc').html() + '\n';
	var id = '';
	var name = '';
	var sms = '';
	var ivrcode = '';
	var smsstring = '';
	var sop = '';
	var wctype = '';
	$(ACW_XML).find('wname name').each(function() {
		if ($(this).text() == selectedCodeName) {
			$(this).parent().find('id').each(function() {
				id = $(this).text();
			});
			$(this).parent().find('name').each(function() {
				name = $(this).text();
			});
			$(this).parent().find('sms').each(function() {
				sms = $(this).text();
			});
			$(this).parent().find('ivrcode').each(function() {
				ivrcode = $(this).text();
			});
			$(this).parent().find('smsstring').each(function() {
				smsstring = $(this).text();
			});
			$(this).parent().find('sop').each(function() {
				sop = $(this).text();
			});
			$(this).parent().find('wctype').each(function() {
				wctype = $(this).text();
			});
		}
	});
	if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype, ACW_idArray) == -1 && id != '') {
		acwSelectedNameDiv += '<div class="selected-option-wrapper dragable">';
		acwSelectedNameDiv += '<div class="label">';
		acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
		acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype + '" onchange="state_acwSelectedName(this)" >';
		acwSelectedNameDiv += '</div>';
		acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
		acwSelectedNameDiv += '<button class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
		acwSelectedNameDiv += '</div>';
		acwSelectedNameDiv += '</div>';

//		if (sop != 'null') {
//			acwDescDiv += '<option>' + sop + '</option>' + '\n';
//			acwDescDiv += '<option>' + '' + '</option>' + '\n';
//		}
//		if (smsstring != 'null') {
//			acwDescDiv += '<option>' + smsstring + '</option>' + '\n';
//		}

		ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype);
	}
	if (smsstring != 'null') {
		acwDescDiv += '' + "SMS Description:" + '<br/>' + '\n';
		acwDescDiv += '' + smsstring + '<br/><br/>' + '\n';
	}
	if (sop != 'null') {
		acwDescDiv += '' + 'SMS SOP:' + '<br/>' + '\n';
		acwDescDiv += '' + sop + '<br/>' + '\n';
	}
	$('#acwSelectedName').html(acwSelectedNameDiv);
	$('#acwDesc').html(acwDescDiv);
}

/*function populate_acwSelectedName() {
 var acwSelectedName_txt = '';
 var selectedHeadName = $('#acwListHeadName').val();
 var selectedCodeName = $('#acwListCodeName').val();
 var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
 var id = '';
 var name = '';
 var sms = '';
 $(ACW_XML).find('wname name').each(function() {
 if ($(this).text() == selectedCodeName) {
 $(this).parent().find('id').each(function() {
 id = $(this).text();
 });
 $(this).parent().find('name').each(function() {
 name = $(this).text();
 });
 $(this).parent().find('sms').each(function() {
 sms = $(this).text();
 });
 }
 });
 if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName, ACW_idArray) == -1 && id != '') {
 acwSelectedNameDiv += '<div class="selected-option-wrapper dragable">';
 acwSelectedNameDiv += '<div class="label">';
 acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
 acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + '" onchange="state_acwSelectedName(this)" >';
 acwSelectedNameDiv += '</div>';
 acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
 acwSelectedNameDiv += '<button class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
 acwSelectedNameDiv += '</div>';
 acwSelectedNameDiv += '</div>';
 
 ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName);
 }
 $('#acwSelectedName').html(acwSelectedNameDiv);
 }*/

function state_acwSelectedName(obj) {
	$('#acwDesc').empty();
	var dbclick = $(obj).parent().parent();
	if (dbclick.hasClass('bgdblClick')) {
		$('#submitAWC_btn').css('display', 'none');
	}
	ACW_idArray = $.grep(ACW_idArray, function(value) {
		return value != $(obj).parent().parent().find('input.acwSelectedNameChkBox_class').val();
	});
	$(obj).parent().parent().remove();
}

function acwKeyPress(obj, evt) {
	var id = $(obj).attr('id');
	if (id == 'acwListHeadName') {
		if (evt.keyCode == 32) {
			populate_acwListCodeName();
		}
		else if (evt.keyCode == 39) {
			$('#acwListCodeName').focus();
			$('#acwListCodeName option:first-child').attr("selected", "selected");
		}
		else if (evt.keyCode == 37) {
			$('#acwListHeadName').blur();
			$('#search_acw').focus();
			awc_click_state = 1;
		}
	}
	else if (id == 'acwListCodeName') {
		if (evt.keyCode == 32) {
			populate_acwSelectedName();
		}
		else if (evt.keyCode == 39) {
			populate_acwSelectedName();
		}
		else if (evt.keyCode == 37) {
			$('#acwListCodeName').blur();
			$('#acwListHeadName').focus();
		}
	}
}

function ivrKeyPress(obj, evt) {
	var id = $(obj).attr('id');
	if (id == 'IVRTransCat') {
		if (evt.keyCode == 32) {
			getIVRSubCategory();
		}
		else if (evt.keyCode == 39) {
			$('#IVRTransSubCat').focus();
			$('#IVRTransSubCat option:first-child').attr("selected", "selected");
		}
		else if (evt.keyCode == 37) {
			$('#IVRTransCat').blur();
			$('#search_ivr').focus();
			ivr_click_state = 3;
		}
	}
	else if (id == 'IVRTransSubCat') {
		if (evt.keyCode == 32) {
			getIVRMenuCode();
		}
		else if (evt.keyCode == 37) {
			$('#IVRTransSubCat').blur();
			$('#IVRTransCat').focus();
		}
		else if (evt.keyCode == 39) {
			$('#IVRTransMenuValue').focus();
			$('#IVRTransMenuValue option:first-child').attr("selected", "selected");
		}
	}
	else if (id == 'IVRTransMenuValue') {
		if (evt.keyCode == 32) {
			if ($('#IVRTransMenuValue').val() != null)
			{
				if ($.trim($('#IVRTransMenuValue').val()).length > 0) {
					clientObj.IVR_Transfer($('#IVRTransMenuValue').val());
					$('#IVRTransSubCat').html('');
					$('#IVRTransMenu').html('');
					$('#close-ivr-lightbox').mousedown();
				}
			}
		}
		else if (evt.keyCode == 37) {
			$('#IVRTransMenuValue').blur();
			$('#IVRTransSubCat').focus();
		}
	}
}

function freqCodeBtnPress(obj, id, sms, name, selectedHeadName, ivrcode, smsstring, sop, wctype) {
	var classname = $(obj).attr('class').split(' ');

	if (classname.length > 1) {
		//alert('deactive');
		$(obj).attr('class', 'checkbox-generic');
	}
	else {
		var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
		$('#acwDesc').empty();
		var acwDescDiv = $('#acwDesc').html() + '\n';
		$(obj).attr('class', 'checkbox-generic checked-checkbox');
		if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype, ACW_idArray) == -1 && id != '') {
//			acwSelectedNameDiv += '<div class="selected-option-wrapper">';
//			acwSelectedNameDiv += '<div class="label">';
//			acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
//			acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype + '" onchange="state_acwSelectedName(this)" >';
//			acwSelectedNameDiv += '</div>';
//			acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
//			acwSelectedNameDiv += '<button class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
//			acwSelectedNameDiv += '</div>';
//			acwSelectedNameDiv += '</div>';

			acwSelectedNameDiv += '<div class="selected-option-wrapper dragable">';
			acwSelectedNameDiv += '<div class="label">';
			acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
			acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype + '" onchange="state_acwSelectedName(this)" >';
			acwSelectedNameDiv += '</div>';
			acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
			acwSelectedNameDiv += '<button class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
			acwSelectedNameDiv += '</div>';
			acwSelectedNameDiv += '</div>';

			if (smsstring != 'null') {
				acwDescDiv += '' + "SMS Description:" + '<br/>' + '\n';
				acwDescDiv += '' + smsstring + '<br/><br/>' + '\n';
			}
			if (sop != 'null') {
				acwDescDiv += '' + 'SMS SOP:' + '<br/>' + '\n';
				acwDescDiv += '' + sop + '<br/>' + '\n';
			}
			ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype);
		}
		$('#acwSelectedName').html(acwSelectedNameDiv);
		$('#acwDesc').html(acwDescDiv);

	}
}

function afterCallPopup() {
	WebLogs("AWC_JS::afterCallPopup::afterCallPopup function started", 0);
	WebLogs("AWC_JS::afterCallPopup::acw_check prev value = " + acw_check, 0);
	acw_check = false;
	WebLogs("AWC_JS::afterCallPopup::acw_check now value = " + acw_check, 0);

	//$('#acwListHeadName').html('');
	//getAWCXML();

	$('#close-acw-lightbox').css('display', 'none');
	$('#minimize-acw-lightbox').css('display', 'none');

	$('#submitAWC_btn').css('display', '');

	$('.checkbox-wrapper div.checkbox-container a').attr('class', 'checkbox-generic');
	/*	
	 $("button").css('z-index', '0');
	 $("#lightbox-bg-blackout").css('display','block');
	 $("#lightbox-bg-blackout").css('z-index','1500');
	 $("#lightbox-bg-blackout").css('opacity','0.8');
	 */
	/*
	 $("#lightbox-bg-blackout").animate({
	 opacity: 0.8
	 }, 500, 'easeOutCirc');
	 */

	WebLogs("AWC_JS::afterCallPopup::showing acw popup", 0);

	setTimeout(function() {
		$("button.expand-button,.pagination-buttons button").css('z-index', '500');
	}, 1000);
	$(".acw-lightbox").css('z-index', '900');
	$(".acw-lightbox").css('display', 'block');
	$(".acw-lightbox").css('opacity', '1');

	/*
	 $(".acw-lightbox").animate({
	 opacity: 1
	 }, 500, 'easeOutCirc');
	 */

	WebLogs("AWC_JS::afterCallPopup::supervisor value = " + clientObj.isSupervisor(), 0);

	setTimeout(function() {
		$('#btnReady').attr("disabled", true);
		$('#btnReady').css('opacity', 0.1);
		console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
	}, 500);



	if (clientObj.isSupervisor()) {
		setTimeout(function() {

//			var isIvrSelected = false;
//			var ivrCode = null;
//			$(".acw-lightbox .dragging").find(".selected-option-wrapper").each(function() {
//				if ($(this).hasClass("bgdblClick")) {
//					isIvrSelected = true;
//					var acw_value = $(this).find("input.acwSelectedNameChkBox_class").first().val();
//					ivrCode = acw_value.split(",")[4];
//				}
//			});

			setTimeout(function() {
				$('#btnReady').attr("disabled", false);
				$('#btnReady').css('opacity', 1);
				console.log("#btnReady state now (supervisor) : " + $('#btnReady').attr("disabled"));
			}, 500);

			if (ACW_idArray.length > 0) {
				WebLogs("AWC_JS::afterCallPopup::Work codes selected", 0);
				WebLogs("AWC_JS::afterCallPopup::calling caresWorkCode function", 0);
				caresWorkCode(ACW_idArray);
				/*
				 if(clientObj.isAgentOnCall())
				 {
				 WebLogs("AWC_JS::afterCallPopup::agent is on call(closing acw popup)",0);
				 //mainScreenTrans();
				 $('#close-acw-lightbox').mousedown();
				 }
				 else		
				 */
				{
					WebLogs("AWC_JS::afterCallPopup::agent is NOT on call(closing acw popup)", 0);
					$('#close-acw-lightbox').mousedown();
					switch (clientObj.getIpPhoneType()) {
						case "Nortel":
							if (!clientObj.getNRforNextCall()) {
								WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
								$('#btnReady').css('background-position-x', '-275px');

								$('#btnReady').parent().find('div.label span').html('not ready');
								$('#btnReady').attr('id', 'btnNot_Ready');
								WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);
								clientObj.Ready();
							}
							else {
								WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

								clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
							}
							break;
						case "Cisco_LHR":

							$('#btnReady').css('background-position-x', '-275px');

							$('#btnReady').parent().find('div.label span').html('not ready');
							$('#btnReady').attr('id', 'btnNot_Ready');
							if (!clientObj.getNRforNextCall()) {
								WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

								clientObj.Ready();
							}
							else {
								WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

								clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
							}
							break;
						case "Cisco_ISB":

							$('#btnReady').css('background-position-x', '-275px');

							$('#btnReady').parent().find('div.label span').html('not ready');
							$('#btnReady').attr('id', 'btnNot_Ready');
							if (!clientObj.getNRforNextCall()) {
								WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

								clientObj.Ready();
							}
							else {
								WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

								clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
							}
							break;
						case "Cisco_KHI":

							$('#btnReady').css('background-position-x', '-275px');

							$('#btnReady').parent().find('div.label span').html('not ready');
							$('#btnReady').attr('id', 'btnNot_Ready');
							if (!clientObj.getNRforNextCall()) {
								WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

								clientObj.Ready();
							}
							else {
								WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
								WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

								clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
							}
							break;
					}
				}
			}
			else {
				WebLogs("AWC_JS::afterCallPopup::Work codes NOT selected", 0);

				switch (clientObj.getIpPhoneType()) {
					case "Nortel":
						if (!clientObj.getNRforNextCall()) {
							WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);

							$('#btnReady').css('background-position-x', '-275px');

							$('#btnReady').parent().find('div.label span').html('not ready');
							$('#btnReady').attr('id', 'btnNot_Ready');
							WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

							clientObj.Ready();
						}
						else {
							WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

							clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
						}
						break;
					case "Cisco_LHR":

						$('#btnReady').css('background-position-x', '-275px');

						$('#btnReady').parent().find('div.label span').html('not ready');
						$('#btnReady').attr('id', 'btnNot_Ready');
						if (!clientObj.getNRforNextCall()) {
							WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

							clientObj.Ready();
						}
						else {
							WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

							clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
						}
						break;
					case "Cisco_ISB":

						$('#btnReady').css('background-position-x', '-275px');

						$('#btnReady').parent().find('div.label span').html('not ready');
						$('#btnReady').attr('id', 'btnNot_Ready');
						if (!clientObj.getNRforNextCall()) {
							WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

							clientObj.Ready();
						}
						else {
							WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

							clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
						}
						break;
					case "Cisco_KHI":

						$('#btnReady').css('background-position-x', '-275px');

						$('#btnReady').parent().find('div.label span').html('not ready');
						$('#btnReady').attr('id', 'btnNot_Ready');
						if (!clientObj.getNRforNextCall()) {
							WebLogs("AWC_JS::afterCallPopup::agent is ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to ready agent", 0);

							clientObj.Ready();
						}
						else {
							WebLogs("AWC_JS::afterCallPopup::agent is NOT ready for next call", 0);
							WebLogs("AWC_JS::afterCallPopup::going to NOT ready agent", 0);

							clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
						}
						break;
				}
			}
			$('#acwSelectedName').html('');
			$('#acwSelectedName1').html('');
			$('#acwListCodeName').html('');
			$('#acwDesc').html("");
			ACW_idArray = new Array();
			WebLogs("AWC_JS::afterCallPopup::closing acw popup", 0);
			$('#close-acw-lightbox').mousedown();
		}, clientObj.getSupVsrAcwDuration() * 1000);
	}
}

$('#submitAWC_btn').live('click', function() {

	var isIvrSelected = false;
	var ivrCode = null;
	$(".acw-lightbox .dragging").find(".selected-option-wrapper").each(function() {
		if ($(this).hasClass("bgdblClick")) {
			isIvrSelected = true;
			var acw_value = $(this).find("input.acwSelectedNameChkBox_class").first().val();
			ivrCode = acw_value.split(",")[4];
		}
	});

	WebLogs("AWC_JS::submitAWC_btn.click()::submitAWC_btn button clicked", 0);
	if (clientObj.isAgentOnCall() && !isIvrSelected)
	{
		WebLogs("AWC_JS::submitAWC_btn.click()::agent is on call(closing acw popup)", 0);

		$('#close-acw-lightbox').mousedown();
	}
	else if (ACW_idArray.length > 0) {
		if (isIvrSelected && ivrCode != null && ivrCode != 'null') {
			clientObj.IVR_Transfer(ivrCode);
			acw_ivr_transfered = true;
			$('#close-acw-lightbox').mousedown();
		}
		else {
			WebLogs("AWC_JS::submitAWC_btn.click()::workcodes selected", 0);
			WebLogs("AWC_JS::submitAWC_btn.click()::calling caresWorkCode function", 0);

			setTimeout(function() {
				$('#btnReady').attr("disabled", false);
				$('#btnReady').css('opacity', 1);
				console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
			}, 500);

			caresWorkCode(ACW_idArray);

			WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT on call(closing acw popup)", 0);
			$('#close-acw-lightbox').mousedown();
			switch (clientObj.getIpPhoneType()) {
				case "Nortel":
					if (!clientObj.getNRforNextCall()) {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
						$('#btnReady').css('background-position-x', '-275px');

						$('#btnReady').parent().find('div.label span').html('not ready');
						$('#btnReady').attr('id', 'btnNot_Ready');
						WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);
						clientObj.Ready();
					}
					else {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);
						clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
					}
					break;
				case "Cisco_LHR":

					$('#btnReady').css('background-position-x', '-275px');

					$('#btnReady').parent().find('div.label span').html('not ready');
					$('#btnReady').attr('id', 'btnNot_Ready');
					if (!clientObj.getNRforNextCall()) {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

						clientObj.Ready();
					}
					else {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

						clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
					}
					break;
				case "Cisco_ISB":

					$('#btnReady').css('background-position-x', '-275px');

					$('#btnReady').parent().find('div.label span').html('not ready');
					$('#btnReady').attr('id', 'btnNot_Ready');
					if (!clientObj.getNRforNextCall()) {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

						clientObj.Ready();
					}
					else {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

						clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
					}
					break;
				case "Cisco_KHI":

					$('#btnReady').css('background-position-x', '-275px');

					$('#btnReady').parent().find('div.label span').html('not ready');
					$('#btnReady').attr('id', 'btnNot_Ready');
					if (!clientObj.getNRforNextCall()) {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

						clientObj.Ready();
					}
					else {
						WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
						WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

						clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
					}
					break;
			}
		}
	}
	$('#acwSelectedName').html('');
	$('#acwSelectedName1').html('');
	$('#acwListCodeName').html('');
	$('#acwDesc').html("");
	ACW_idArray = new Array();
});