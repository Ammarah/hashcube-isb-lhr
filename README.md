# Hash Cube Contact Center

HashCube Contact Center helps telecommunications companies contact centers to communicate with their customers via call. This project is specifically designed for Ufone contact center.

## Getting Started

Clone repository `git clone https://Ammarah@bitbucket.org/Ammarah/hashcube-isb-lhr.git`

### Prerequisites

1. Netbeans (8.2)
2. Tomcat (8.5 or above)
3. MSSQL (12)
4. Cisco IP Communicator
5. Hash Exe


### Installing

1. Open project in netbeans
2. Build
3. Deploy
4. Run Cisco IP Communicator.
   1. Set  Preference > Network > (Device Name, TFTP servers IPs)
5. Run hash.exe (.Net project)

## Deployment

Copy the `.war` file from dist/ and paste it in tomcat `webapp`

## Versioning

Use the following format to version the `.war` file `app##1.0.0.war`.

## License

Apollo Telecom Pvt Ltd

