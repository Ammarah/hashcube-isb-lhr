
package cares.WS1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cares.WS1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cares.WS1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPostPaidBillInfoResponse }
     * 
     */
    public GetPostPaidBillInfoResponse createGetPostPaidBillInfoResponse() {
        return new GetPostPaidBillInfoResponse();
    }

    /**
     * Create an instance of {@link QueryCustomer }
     * 
     */
    public QueryCustomer createQueryCustomer() {
        return new QueryCustomer();
    }

    /**
     * Create an instance of {@link ChangeTarrif }
     * 
     */
    public ChangeTarrif createChangeTarrif() {
        return new ChangeTarrif();
    }

    /**
     * Create an instance of {@link QueryBundle }
     * 
     */
    public QueryBundle createQueryBundle() {
        return new QueryBundle();
    }

    /**
     * Create an instance of {@link SubUpdateIvr }
     * 
     */
    public SubUpdateIvr createSubUpdateIvr() {
        return new SubUpdateIvr();
    }

    /**
     * Create an instance of {@link SubCollectiveCall }
     * 
     */
    public SubCollectiveCall createSubCollectiveCall() {
        return new SubCollectiveCall();
    }

    /**
     * Create an instance of {@link SubscribeServiceResponse }
     * 
     */
    public SubscribeServiceResponse createSubscribeServiceResponse() {
        return new SubscribeServiceResponse();
    }

    /**
     * Create an instance of {@link SubTpinChange }
     * 
     */
    public SubTpinChange createSubTpinChange() {
        return new SubTpinChange();
    }

    /**
     * Create an instance of {@link UnSubscribeServiceResponse }
     * 
     */
    public UnSubscribeServiceResponse createUnSubscribeServiceResponse() {
        return new UnSubscribeServiceResponse();
    }

    /**
     * Create an instance of {@link ChangeTarrifResponse }
     * 
     */
    public ChangeTarrifResponse createChangeTarrifResponse() {
        return new ChangeTarrifResponse();
    }

    /**
     * Create an instance of {@link SubTpinChangeResponse }
     * 
     */
    public SubTpinChangeResponse createSubTpinChangeResponse() {
        return new SubTpinChangeResponse();
    }

    /**
     * Create an instance of {@link CallQuotaResponse }
     * 
     */
    public CallQuotaResponse createCallQuotaResponse() {
        return new CallQuotaResponse();
    }

    /**
     * Create an instance of {@link SubCollectiveCallResponse }
     * 
     */
    public SubCollectiveCallResponse createSubCollectiveCallResponse() {
        return new SubCollectiveCallResponse();
    }

    /**
     * Create an instance of {@link Get789Info }
     * 
     */
    public Get789Info createGet789Info() {
        return new Get789Info();
    }

    /**
     * Create an instance of {@link QueryBundleResponse }
     * 
     */
    public QueryBundleResponse createQueryBundleResponse() {
        return new QueryBundleResponse();
    }

    /**
     * Create an instance of {@link UpdateBillAddress }
     * 
     */
    public UpdateBillAddress createUpdateBillAddress() {
        return new UpdateBillAddress();
    }

    /**
     * Create an instance of {@link CallQuota }
     * 
     */
    public CallQuota createCallQuota() {
        return new CallQuota();
    }

    /**
     * Create an instance of {@link QueryCustomerResponse }
     * 
     */
    public QueryCustomerResponse createQueryCustomerResponse() {
        return new QueryCustomerResponse();
    }

    /**
     * Create an instance of {@link SubscribeService }
     * 
     */
    public SubscribeService createSubscribeService() {
        return new SubscribeService();
    }

    /**
     * Create an instance of {@link UnSubscribeService }
     * 
     */
    public UnSubscribeService createUnSubscribeService() {
        return new UnSubscribeService();
    }

    /**
     * Create an instance of {@link SubUpdateIvrResponse }
     * 
     */
    public SubUpdateIvrResponse createSubUpdateIvrResponse() {
        return new SubUpdateIvrResponse();
    }

    /**
     * Create an instance of {@link Get789InfoResponse }
     * 
     */
    public Get789InfoResponse createGet789InfoResponse() {
        return new Get789InfoResponse();
    }

    /**
     * Create an instance of {@link UpdateBillAddressResponse }
     * 
     */
    public UpdateBillAddressResponse createUpdateBillAddressResponse() {
        return new UpdateBillAddressResponse();
    }

    /**
     * Create an instance of {@link GetPostPaidBillInfo }
     * 
     */
    public GetPostPaidBillInfo createGetPostPaidBillInfo() {
        return new GetPostPaidBillInfo();
    }

}
