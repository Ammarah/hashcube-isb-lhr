
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SecurityDeposit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentDue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinimumPayment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastInvoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastBillPayment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentInfoType", propOrder = {
    "securityDeposit",
    "currentDue",
    "creditLimit",
    "minimumPayment",
    "lastInvoice",
    "lastBillPayment"
})
public class PaymentInfoType {

    @XmlElement(name = "SecurityDeposit")
    protected String securityDeposit;
    @XmlElement(name = "CurrentDue")
    protected String currentDue;
    @XmlElement(name = "CreditLimit")
    protected String creditLimit;
    @XmlElement(name = "MinimumPayment")
    protected String minimumPayment;
    @XmlElement(name = "LastInvoice")
    protected String lastInvoice;
    @XmlElement(name = "LastBillPayment")
    protected String lastBillPayment;

    /**
     * Gets the value of the securityDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityDeposit() {
        return securityDeposit;
    }

    /**
     * Sets the value of the securityDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityDeposit(String value) {
        this.securityDeposit = value;
    }

    /**
     * Gets the value of the currentDue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentDue() {
        return currentDue;
    }

    /**
     * Sets the value of the currentDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentDue(String value) {
        this.currentDue = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditLimit(String value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the minimumPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumPayment() {
        return minimumPayment;
    }

    /**
     * Sets the value of the minimumPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumPayment(String value) {
        this.minimumPayment = value;
    }

    /**
     * Gets the value of the lastInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastInvoice() {
        return lastInvoice;
    }

    /**
     * Sets the value of the lastInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastInvoice(String value) {
        this.lastInvoice = value;
    }

    /**
     * Gets the value of the lastBillPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastBillPayment() {
        return lastBillPayment;
    }

    /**
     * Sets the value of the lastBillPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastBillPayment(String value) {
        this.lastBillPayment = value;
    }

}
