
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VPNType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VPNType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsVPN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentIsVPN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroupID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotlineDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ForcedOnNet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VPNType", propOrder = {
    "isVPN",
    "currentIsVPN",
    "groupID",
    "shortCode",
    "hotlineDN",
    "forcedOnNet"
})
public class VPNType {

    @XmlElement(name = "IsVPN")
    protected String isVPN;
    @XmlElement(name = "CurrentIsVPN")
    protected String currentIsVPN;
    @XmlElement(name = "GroupID")
    protected String groupID;
    @XmlElement(name = "ShortCode")
    protected String shortCode;
    @XmlElement(name = "HotlineDN")
    protected String hotlineDN;
    @XmlElement(name = "ForcedOnNet")
    protected String forcedOnNet;

    /**
     * Gets the value of the isVPN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsVPN() {
        return isVPN;
    }

    /**
     * Sets the value of the isVPN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsVPN(String value) {
        this.isVPN = value;
    }

    /**
     * Gets the value of the currentIsVPN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentIsVPN() {
        return currentIsVPN;
    }

    /**
     * Sets the value of the currentIsVPN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentIsVPN(String value) {
        this.currentIsVPN = value;
    }

    /**
     * Gets the value of the groupID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Sets the value of the groupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupID(String value) {
        this.groupID = value;
    }

    /**
     * Gets the value of the shortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortCode() {
        return shortCode;
    }

    /**
     * Sets the value of the shortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortCode(String value) {
        this.shortCode = value;
    }

    /**
     * Gets the value of the hotlineDN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotlineDN() {
        return hotlineDN;
    }

    /**
     * Sets the value of the hotlineDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotlineDN(String value) {
        this.hotlineDN = value;
    }

    /**
     * Gets the value of the forcedOnNet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForcedOnNet() {
        return forcedOnNet;
    }

    /**
     * Sets the value of the forcedOnNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForcedOnNet(String value) {
        this.forcedOnNet = value;
    }

}
