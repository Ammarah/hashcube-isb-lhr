
package cares.WCMS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LastComplaintListBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LastComplaintListBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LastComplaintBOList" type="{http://Ufone-WCMS-Integration}LastComplaintBO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LastComplaintListBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "lastComplaintBOList"
})
public class LastComplaintListBO {

    @XmlElement(name = "LastComplaintBOList")
    protected List<LastComplaintBO> lastComplaintBOList;

    /**
     * Gets the value of the lastComplaintBOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lastComplaintBOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLastComplaintBOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LastComplaintBO }
     * 
     * 
     */
    public List<LastComplaintBO> getLastComplaintBOList() {
        if (lastComplaintBOList == null) {
            lastComplaintBOList = new ArrayList<LastComplaintBO>();
        }
        return this.lastComplaintBOList;
    }

}
