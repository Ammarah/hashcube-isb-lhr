
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="complaintId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "complaintId",
    "remarks",
    "agentId",
    "callingSystem",
    "callingIP"
})
@XmlRootElement(name = "DeclineTicket")
public class DeclineTicket {

    @XmlElement(required = true, nillable = true)
    protected String complaintId;
    @XmlElement(name = "Remarks", required = true, nillable = true)
    protected String remarks;
    @XmlElement(required = true, nillable = true)
    protected String agentId;
    @XmlElement(name = "CallingSystem", required = true, nillable = true)
    protected String callingSystem;
    @XmlElement(name = "CallingIP", required = true, nillable = true)
    protected String callingIP;

    /**
     * Gets the value of the complaintId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintId() {
        return complaintId;
    }

    /**
     * Sets the value of the complaintId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintId(String value) {
        this.complaintId = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the agentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Sets the value of the agentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentId(String value) {
        this.agentId = value;
    }

    /**
     * Gets the value of the callingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingSystem() {
        return callingSystem;
    }

    /**
     * Sets the value of the callingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingSystem(String value) {
        this.callingSystem = value;
    }

    /**
     * Gets the value of the callingIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingIP() {
        return callingIP;
    }

    /**
     * Sets the value of the callingIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingIP(String value) {
        this.callingIP = value;
    }

}
