
package cares.WCMS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for TaskBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="complaintid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workcodeid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wcname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complaintlogstartdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userinformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agentremarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wcstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actionremarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prevactionremarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wcqualitystatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complaintlogenddate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loggedstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loggedby" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logagentid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loglocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clinumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iswithintat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isfileattached" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fileData" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
 *         &lt;element name="fileExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="filePath" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsWithInTatString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "complaintid",
    "workcodeid",
    "wcname",
    "complaintlogstartdate",
    "userinformation",
    "agentremarks",
    "wcstatus",
    "actionremarks",
    "prevactionremarks",
    "wcqualitystatus",
    "complaintlogenddate",
    "loggedstatus",
    "loggedby",
    "logagentid",
    "loglocation",
    "clinumber",
    "iswithintat",
    "isfileattached",
    "fileData",
    "fileExt",
    "filePath",
    "isWithInTatString"
})
public class TaskBO {

    @XmlElementRef(name = "complaintid", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complaintid;
    @XmlElementRef(name = "workcodeid", type = JAXBElement.class, required = false)
    protected JAXBElement<String> workcodeid;
    @XmlElementRef(name = "wcname", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wcname;
    @XmlElementRef(name = "complaintlogstartdate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complaintlogstartdate;
    @XmlElementRef(name = "userinformation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userinformation;
    @XmlElementRef(name = "agentremarks", type = JAXBElement.class, required = false)
    protected JAXBElement<String> agentremarks;
    @XmlElementRef(name = "wcstatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wcstatus;
    @XmlElementRef(name = "actionremarks", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionremarks;
    @XmlElementRef(name = "prevactionremarks", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevactionremarks;
    @XmlElementRef(name = "wcqualitystatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wcqualitystatus;
    @XmlElementRef(name = "complaintlogenddate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complaintlogenddate;
    @XmlElementRef(name = "loggedstatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loggedstatus;
    @XmlElementRef(name = "loggedby", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loggedby;
    @XmlElementRef(name = "logagentid", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logagentid;
    @XmlElementRef(name = "loglocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loglocation;
    @XmlElementRef(name = "clinumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clinumber;
    @XmlElementRef(name = "iswithintat", type = JAXBElement.class, required = false)
    protected JAXBElement<String> iswithintat;
    protected Integer isfileattached;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(HexBinaryAdapter.class)
    @XmlSchemaType(name = "hexBinary")
    protected byte[] fileData;
    protected String fileExt;
    protected String filePath;
    @XmlElement(name = "IsWithInTatString")
    protected String isWithInTatString;

    /**
     * Gets the value of the complaintid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplaintid() {
        return complaintid;
    }

    /**
     * Sets the value of the complaintid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplaintid(JAXBElement<String> value) {
        this.complaintid = value;
    }

    /**
     * Gets the value of the workcodeid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWorkcodeid() {
        return workcodeid;
    }

    /**
     * Sets the value of the workcodeid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWorkcodeid(JAXBElement<String> value) {
        this.workcodeid = value;
    }

    /**
     * Gets the value of the wcname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWcname() {
        return wcname;
    }

    /**
     * Sets the value of the wcname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWcname(JAXBElement<String> value) {
        this.wcname = value;
    }

    /**
     * Gets the value of the complaintlogstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplaintlogstartdate() {
        return complaintlogstartdate;
    }

    /**
     * Sets the value of the complaintlogstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplaintlogstartdate(JAXBElement<String> value) {
        this.complaintlogstartdate = value;
    }

    /**
     * Gets the value of the userinformation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserinformation() {
        return userinformation;
    }

    /**
     * Sets the value of the userinformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserinformation(JAXBElement<String> value) {
        this.userinformation = value;
    }

    /**
     * Gets the value of the agentremarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAgentremarks() {
        return agentremarks;
    }

    /**
     * Sets the value of the agentremarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAgentremarks(JAXBElement<String> value) {
        this.agentremarks = value;
    }

    /**
     * Gets the value of the wcstatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWcstatus() {
        return wcstatus;
    }

    /**
     * Sets the value of the wcstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWcstatus(JAXBElement<String> value) {
        this.wcstatus = value;
    }

    /**
     * Gets the value of the actionremarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionremarks() {
        return actionremarks;
    }

    /**
     * Sets the value of the actionremarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionremarks(JAXBElement<String> value) {
        this.actionremarks = value;
    }

    /**
     * Gets the value of the prevactionremarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevactionremarks() {
        return prevactionremarks;
    }

    /**
     * Sets the value of the prevactionremarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevactionremarks(JAXBElement<String> value) {
        this.prevactionremarks = value;
    }

    /**
     * Gets the value of the wcqualitystatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWcqualitystatus() {
        return wcqualitystatus;
    }

    /**
     * Sets the value of the wcqualitystatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWcqualitystatus(JAXBElement<String> value) {
        this.wcqualitystatus = value;
    }

    /**
     * Gets the value of the complaintlogenddate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplaintlogenddate() {
        return complaintlogenddate;
    }

    /**
     * Sets the value of the complaintlogenddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplaintlogenddate(JAXBElement<String> value) {
        this.complaintlogenddate = value;
    }

    /**
     * Gets the value of the loggedstatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoggedstatus() {
        return loggedstatus;
    }

    /**
     * Sets the value of the loggedstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoggedstatus(JAXBElement<String> value) {
        this.loggedstatus = value;
    }

    /**
     * Gets the value of the loggedby property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoggedby() {
        return loggedby;
    }

    /**
     * Sets the value of the loggedby property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoggedby(JAXBElement<String> value) {
        this.loggedby = value;
    }

    /**
     * Gets the value of the logagentid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogagentid() {
        return logagentid;
    }

    /**
     * Sets the value of the logagentid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogagentid(JAXBElement<String> value) {
        this.logagentid = value;
    }

    /**
     * Gets the value of the loglocation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoglocation() {
        return loglocation;
    }

    /**
     * Sets the value of the loglocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoglocation(JAXBElement<String> value) {
        this.loglocation = value;
    }

    /**
     * Gets the value of the clinumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClinumber() {
        return clinumber;
    }

    /**
     * Sets the value of the clinumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClinumber(JAXBElement<String> value) {
        this.clinumber = value;
    }

    /**
     * Gets the value of the iswithintat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIswithintat() {
        return iswithintat;
    }

    /**
     * Sets the value of the iswithintat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIswithintat(JAXBElement<String> value) {
        this.iswithintat = value;
    }

    /**
     * Gets the value of the isfileattached property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsfileattached() {
        return isfileattached;
    }

    /**
     * Sets the value of the isfileattached property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsfileattached(Integer value) {
        this.isfileattached = value;
    }

    /**
     * Gets the value of the fileData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public byte[] getFileData() {
        return fileData;
    }

    /**
     * Sets the value of the fileData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileData(byte[] value) {
        this.fileData = value;
    }

    /**
     * Gets the value of the fileExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileExt() {
        return fileExt;
    }

    /**
     * Sets the value of the fileExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileExt(String value) {
        this.fileExt = value;
    }

    /**
     * Gets the value of the filePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the value of the filePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilePath(String value) {
        this.filePath = value;
    }

    /**
     * Gets the value of the isWithInTatString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWithInTatString() {
        return isWithInTatString;
    }

    /**
     * Sets the value of the isWithInTatString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWithInTatString(String value) {
        this.isWithInTatString = value;
    }

}
