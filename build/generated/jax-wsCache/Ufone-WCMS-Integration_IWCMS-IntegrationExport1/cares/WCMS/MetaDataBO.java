
package cares.WCMS;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetaDataBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetaDataBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workcodeu32title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="script" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="controlu32name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fieldu32name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fieldu32tooltip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ismandatory" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="sequencenumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="controlu32fieldu32type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autou32filledu32control" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="lovu32idu32foru32comboboxu32control" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="lovu32nameu32foru32treeviewu32control" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clusteru47dependency" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="batchu32logging" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="worku32codeu32type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDataBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "workcodeu32Title",
    "script",
    "controlu32Name",
    "fieldu32Name",
    "fieldu32Tooltip",
    "ismandatory",
    "sequencenumber",
    "controlu32Fieldu32Type",
    "autou32Filledu32Control",
    "lovu32Idu32Foru32Comboboxu32Control",
    "lovu32Nameu32Foru32Treeviewu32Control",
    "clusteru47Dependency",
    "batchu32Logging",
    "worku32Codeu32Type"
})
public class MetaDataBO {

    @XmlElementRef(name = "workcodeu32title", type = JAXBElement.class, required = false)
    protected JAXBElement<String> workcodeu32Title;
    @XmlElementRef(name = "script", type = JAXBElement.class, required = false)
    protected JAXBElement<String> script;
    @XmlElementRef(name = "controlu32name", type = JAXBElement.class, required = false)
    protected JAXBElement<String> controlu32Name;
    @XmlElementRef(name = "fieldu32name", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fieldu32Name;
    @XmlElementRef(name = "fieldu32tooltip", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fieldu32Tooltip;
    @XmlElementRef(name = "ismandatory", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> ismandatory;
    @XmlElementRef(name = "sequencenumber", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sequencenumber;
    @XmlElement(name = "controlu32fieldu32type")
    protected String controlu32Fieldu32Type;
    @XmlElement(name = "autou32filledu32control")
    protected BigDecimal autou32Filledu32Control;
    @XmlElement(name = "lovu32idu32foru32comboboxu32control")
    protected BigDecimal lovu32Idu32Foru32Comboboxu32Control;
    @XmlElement(name = "lovu32nameu32foru32treeviewu32control")
    protected String lovu32Nameu32Foru32Treeviewu32Control;
    @XmlElement(name = "clusteru47dependency")
    protected BigDecimal clusteru47Dependency;
    @XmlElement(name = "batchu32logging")
    protected BigDecimal batchu32Logging;
    @XmlElement(name = "worku32codeu32type")
    protected String worku32Codeu32Type;

    /**
     * Gets the value of the workcodeu32Title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWorkcodeu32Title() {
        return workcodeu32Title;
    }

    /**
     * Sets the value of the workcodeu32Title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWorkcodeu32Title(JAXBElement<String> value) {
        this.workcodeu32Title = value;
    }

    /**
     * Gets the value of the script property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScript() {
        return script;
    }

    /**
     * Sets the value of the script property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScript(JAXBElement<String> value) {
        this.script = value;
    }

    /**
     * Gets the value of the controlu32Name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getControlu32Name() {
        return controlu32Name;
    }

    /**
     * Sets the value of the controlu32Name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setControlu32Name(JAXBElement<String> value) {
        this.controlu32Name = value;
    }

    /**
     * Gets the value of the fieldu32Name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFieldu32Name() {
        return fieldu32Name;
    }

    /**
     * Sets the value of the fieldu32Name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFieldu32Name(JAXBElement<String> value) {
        this.fieldu32Name = value;
    }

    /**
     * Gets the value of the fieldu32Tooltip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFieldu32Tooltip() {
        return fieldu32Tooltip;
    }

    /**
     * Sets the value of the fieldu32Tooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFieldu32Tooltip(JAXBElement<String> value) {
        this.fieldu32Tooltip = value;
    }

    /**
     * Gets the value of the ismandatory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getIsmandatory() {
        return ismandatory;
    }

    /**
     * Sets the value of the ismandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setIsmandatory(JAXBElement<BigDecimal> value) {
        this.ismandatory = value;
    }

    /**
     * Gets the value of the sequencenumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSequencenumber() {
        return sequencenumber;
    }

    /**
     * Sets the value of the sequencenumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSequencenumber(JAXBElement<BigDecimal> value) {
        this.sequencenumber = value;
    }

    /**
     * Gets the value of the controlu32Fieldu32Type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlu32Fieldu32Type() {
        return controlu32Fieldu32Type;
    }

    /**
     * Sets the value of the controlu32Fieldu32Type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlu32Fieldu32Type(String value) {
        this.controlu32Fieldu32Type = value;
    }

    /**
     * Gets the value of the autou32Filledu32Control property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAutou32Filledu32Control() {
        return autou32Filledu32Control;
    }

    /**
     * Sets the value of the autou32Filledu32Control property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAutou32Filledu32Control(BigDecimal value) {
        this.autou32Filledu32Control = value;
    }

    /**
     * Gets the value of the lovu32Idu32Foru32Comboboxu32Control property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLovu32Idu32Foru32Comboboxu32Control() {
        return lovu32Idu32Foru32Comboboxu32Control;
    }

    /**
     * Sets the value of the lovu32Idu32Foru32Comboboxu32Control property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLovu32Idu32Foru32Comboboxu32Control(BigDecimal value) {
        this.lovu32Idu32Foru32Comboboxu32Control = value;
    }

    /**
     * Gets the value of the lovu32Nameu32Foru32Treeviewu32Control property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLovu32Nameu32Foru32Treeviewu32Control() {
        return lovu32Nameu32Foru32Treeviewu32Control;
    }

    /**
     * Sets the value of the lovu32Nameu32Foru32Treeviewu32Control property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLovu32Nameu32Foru32Treeviewu32Control(String value) {
        this.lovu32Nameu32Foru32Treeviewu32Control = value;
    }

    /**
     * Gets the value of the clusteru47Dependency property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClusteru47Dependency() {
        return clusteru47Dependency;
    }

    /**
     * Sets the value of the clusteru47Dependency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClusteru47Dependency(BigDecimal value) {
        this.clusteru47Dependency = value;
    }

    /**
     * Gets the value of the batchu32Logging property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBatchu32Logging() {
        return batchu32Logging;
    }

    /**
     * Sets the value of the batchu32Logging property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBatchu32Logging(BigDecimal value) {
        this.batchu32Logging = value;
    }

    /**
     * Gets the value of the worku32Codeu32Type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorku32Codeu32Type() {
        return worku32Codeu32Type;
    }

    /**
     * Sets the value of the worku32Codeu32Type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorku32Codeu32Type(String value) {
        this.worku32Codeu32Type = value;
    }

}
