
package cares.location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIMSILocationResultMsgType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIMSILocationResultMsgType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIMSILocationResult" type="{http://www.ufone.org/GetIMSILocation/}GetIMSILocationResultType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIMSILocationResultMsgType", propOrder = {
    "getIMSILocationResult"
})
public class GetIMSILocationResultMsgType {

    @XmlElement(name = "GetIMSILocationResult", required = true)
    protected GetIMSILocationResultType getIMSILocationResult;

    /**
     * Gets the value of the getIMSILocationResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetIMSILocationResultType }
     *     
     */
    public GetIMSILocationResultType getGetIMSILocationResult() {
        return getIMSILocationResult;
    }

    /**
     * Sets the value of the getIMSILocationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIMSILocationResultType }
     *     
     */
    public void setGetIMSILocationResult(GetIMSILocationResultType value) {
        this.getIMSILocationResult = value;
    }

}
