
package cares.WS5;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cares.WS5 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cares.WS5
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LogInfoWc }
     * 
     */
    public LogInfoWc createLogInfoWc() {
        return new LogInfoWc();
    }

    /**
     * Create an instance of {@link SendWrkCodeSmsResponse }
     * 
     */
    public SendWrkCodeSmsResponse createSendWrkCodeSmsResponse() {
        return new SendWrkCodeSmsResponse();
    }

    /**
     * Create an instance of {@link UserLogOut }
     * 
     */
    public UserLogOut createUserLogOut() {
        return new UserLogOut();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link PostCallAdjustmentResponse }
     * 
     */
    public PostCallAdjustmentResponse createPostCallAdjustmentResponse() {
        return new PostCallAdjustmentResponse();
    }

    /**
     * Create an instance of {@link PostCallAdjustment }
     * 
     */
    public PostCallAdjustment createPostCallAdjustment() {
        return new PostCallAdjustment();
    }

    /**
     * Create an instance of {@link UserLogOnResponse }
     * 
     */
    public UserLogOnResponse createUserLogOnResponse() {
        return new UserLogOnResponse();
    }

    /**
     * Create an instance of {@link UserLogOutResponse }
     * 
     */
    public UserLogOutResponse createUserLogOutResponse() {
        return new UserLogOutResponse();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link SendWrkCodeSms }
     * 
     */
    public SendWrkCodeSms createSendWrkCodeSms() {
        return new SendWrkCodeSms();
    }

    /**
     * Create an instance of {@link LogInfoWcResponse }
     * 
     */
    public LogInfoWcResponse createLogInfoWcResponse() {
        return new LogInfoWcResponse();
    }

    /**
     * Create an instance of {@link UserLogOn }
     * 
     */
    public UserLogOn createUserLogOn() {
        return new UserLogOn();
    }

    /**
     * Create an instance of {@link SyncWorkCodeListResponse }
     * 
     */
    public SyncWorkCodeListResponse createSyncWorkCodeListResponse() {
        return new SyncWorkCodeListResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link SyncWorkCodeList }
     * 
     */
    public SyncWorkCodeList createSyncWorkCodeList() {
        return new SyncWorkCodeList();
    }

}
