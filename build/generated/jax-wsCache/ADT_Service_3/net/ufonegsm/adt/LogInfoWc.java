
package net.ufonegsm.adt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operusername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operpassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_group" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logged_by_user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="user_loc_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="user_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rsl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result_Desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "operusername",
    "operpassword",
    "msisdn",
    "wcName",
    "wcGroup",
    "dateTime",
    "loggedByUser",
    "userLocId",
    "userId",
    "rsl",
    "resultDesc"
})
@XmlRootElement(name = "Log_Info_wc")
public class LogInfoWc {

    protected String operusername;
    protected String operpassword;
    protected String msisdn;
    @XmlElement(name = "wc_name")
    protected String wcName;
    @XmlElement(name = "wc_group")
    protected String wcGroup;
    protected String dateTime;
    @XmlElement(name = "logged_by_user")
    protected String loggedByUser;
    @XmlElement(name = "user_loc_id")
    protected int userLocId;
    @XmlElement(name = "user_id")
    protected int userId;
    protected String rsl;
    @XmlElement(name = "Result_Desc")
    protected String resultDesc;

    /**
     * Gets the value of the operusername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperusername() {
        return operusername;
    }

    /**
     * Sets the value of the operusername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperusername(String value) {
        this.operusername = value;
    }

    /**
     * Gets the value of the operpassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperpassword() {
        return operpassword;
    }

    /**
     * Sets the value of the operpassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperpassword(String value) {
        this.operpassword = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the wcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcName() {
        return wcName;
    }

    /**
     * Sets the value of the wcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcName(String value) {
        this.wcName = value;
    }

    /**
     * Gets the value of the wcGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcGroup() {
        return wcGroup;
    }

    /**
     * Sets the value of the wcGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcGroup(String value) {
        this.wcGroup = value;
    }

    /**
     * Gets the value of the dateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Sets the value of the dateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

    /**
     * Gets the value of the loggedByUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoggedByUser() {
        return loggedByUser;
    }

    /**
     * Sets the value of the loggedByUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoggedByUser(String value) {
        this.loggedByUser = value;
    }

    /**
     * Gets the value of the userLocId property.
     * 
     */
    public int getUserLocId() {
        return userLocId;
    }

    /**
     * Sets the value of the userLocId property.
     * 
     */
    public void setUserLocId(int value) {
        this.userLocId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(int value) {
        this.userId = value;
    }

    /**
     * Gets the value of the rsl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsl() {
        return rsl;
    }

    /**
     * Sets the value of the rsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsl(String value) {
        this.rsl = value;
    }

    /**
     * Gets the value of the resultDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

}
