
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TicketType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TicketType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkCodeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkCodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TATStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QualityStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoggedOn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoggedStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogLocationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogLocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActionRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastActionRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotWorkCodeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotComplaintAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedOn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComplaintId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeclineAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeclineBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CLI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttachmentAvailable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attachments" type="{http://www.ufone.com/CMS/}AttachmentListType" minOccurs="0"/>
 *         &lt;element name="CtrlConfig" type="{http://www.ufone.com/CMS/}CtrlConfigItemType" minOccurs="0"/>
 *         &lt;element name="IsRoutingEnabled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SMSAssociated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInformationSMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentDomainId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketType", propOrder = {
    "id",
    "status",
    "workCodeId",
    "workCodeName",
    "tatStatus",
    "qualityStatus",
    "msisdn",
    "loggedOn",
    "loggedStatus",
    "logLocationId",
    "logLocationName",
    "actionRemarks",
    "lastActionRemarks",
    "hotWorkCodeId",
    "hotComplaintAllowed",
    "closedOn",
    "complaintId",
    "declineAllowed",
    "declineBy",
    "cli",
    "attachmentAvailable",
    "attachments",
    "ctrlConfig",
    "isRoutingEnabled",
    "smsAssociated",
    "sendInformationSMS",
    "agentDomainId"
})
public class TicketType {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "WorkCodeId")
    protected String workCodeId;
    @XmlElement(name = "WorkCodeName")
    protected String workCodeName;
    @XmlElement(name = "TATStatus")
    protected String tatStatus;
    @XmlElement(name = "QualityStatus")
    protected String qualityStatus;
    @XmlElement(name = "MSISDN")
    protected String msisdn;
    @XmlElement(name = "LoggedOn")
    protected String loggedOn;
    @XmlElement(name = "LoggedStatus")
    protected String loggedStatus;
    @XmlElement(name = "LogLocationId")
    protected String logLocationId;
    @XmlElement(name = "LogLocationName")
    protected String logLocationName;
    @XmlElement(name = "ActionRemarks")
    protected String actionRemarks;
    @XmlElement(name = "LastActionRemarks")
    protected String lastActionRemarks;
    @XmlElement(name = "HotWorkCodeId")
    protected String hotWorkCodeId;
    @XmlElement(name = "HotComplaintAllowed")
    protected String hotComplaintAllowed;
    @XmlElement(name = "ClosedOn")
    protected String closedOn;
    @XmlElement(name = "ComplaintId")
    protected String complaintId;
    @XmlElement(name = "DeclineAllowed")
    protected String declineAllowed;
    @XmlElement(name = "DeclineBy")
    protected String declineBy;
    @XmlElement(name = "CLI")
    protected String cli;
    @XmlElement(name = "AttachmentAvailable")
    protected String attachmentAvailable;
    @XmlElement(name = "Attachments")
    protected AttachmentListType attachments;
    @XmlElement(name = "CtrlConfig")
    protected CtrlConfigItemType ctrlConfig;
    @XmlElement(name = "IsRoutingEnabled")
    protected String isRoutingEnabled;
    @XmlElement(name = "SMSAssociated")
    protected String smsAssociated;
    @XmlElement(name = "SendInformationSMS")
    protected String sendInformationSMS;
    @XmlElement(name = "AgentDomainId")
    protected String agentDomainId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the workCodeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkCodeId() {
        return workCodeId;
    }

    /**
     * Sets the value of the workCodeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkCodeId(String value) {
        this.workCodeId = value;
    }

    /**
     * Gets the value of the workCodeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkCodeName() {
        return workCodeName;
    }

    /**
     * Sets the value of the workCodeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkCodeName(String value) {
        this.workCodeName = value;
    }

    /**
     * Gets the value of the tatStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTATStatus() {
        return tatStatus;
    }

    /**
     * Sets the value of the tatStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTATStatus(String value) {
        this.tatStatus = value;
    }

    /**
     * Gets the value of the qualityStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualityStatus() {
        return qualityStatus;
    }

    /**
     * Sets the value of the qualityStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualityStatus(String value) {
        this.qualityStatus = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the loggedOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoggedOn() {
        return loggedOn;
    }

    /**
     * Sets the value of the loggedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoggedOn(String value) {
        this.loggedOn = value;
    }

    /**
     * Gets the value of the loggedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoggedStatus() {
        return loggedStatus;
    }

    /**
     * Sets the value of the loggedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoggedStatus(String value) {
        this.loggedStatus = value;
    }

    /**
     * Gets the value of the logLocationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogLocationId() {
        return logLocationId;
    }

    /**
     * Sets the value of the logLocationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogLocationId(String value) {
        this.logLocationId = value;
    }

    /**
     * Gets the value of the logLocationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogLocationName() {
        return logLocationName;
    }

    /**
     * Sets the value of the logLocationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogLocationName(String value) {
        this.logLocationName = value;
    }

    /**
     * Gets the value of the actionRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionRemarks() {
        return actionRemarks;
    }

    /**
     * Sets the value of the actionRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionRemarks(String value) {
        this.actionRemarks = value;
    }

    /**
     * Gets the value of the lastActionRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastActionRemarks() {
        return lastActionRemarks;
    }

    /**
     * Sets the value of the lastActionRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastActionRemarks(String value) {
        this.lastActionRemarks = value;
    }

    /**
     * Gets the value of the hotWorkCodeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotWorkCodeId() {
        return hotWorkCodeId;
    }

    /**
     * Sets the value of the hotWorkCodeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotWorkCodeId(String value) {
        this.hotWorkCodeId = value;
    }

    /**
     * Gets the value of the hotComplaintAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotComplaintAllowed() {
        return hotComplaintAllowed;
    }

    /**
     * Sets the value of the hotComplaintAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotComplaintAllowed(String value) {
        this.hotComplaintAllowed = value;
    }

    /**
     * Gets the value of the closedOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedOn() {
        return closedOn;
    }

    /**
     * Sets the value of the closedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedOn(String value) {
        this.closedOn = value;
    }

    /**
     * Gets the value of the complaintId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintId() {
        return complaintId;
    }

    /**
     * Sets the value of the complaintId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintId(String value) {
        this.complaintId = value;
    }

    /**
     * Gets the value of the declineAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclineAllowed() {
        return declineAllowed;
    }

    /**
     * Sets the value of the declineAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclineAllowed(String value) {
        this.declineAllowed = value;
    }

    /**
     * Gets the value of the declineBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclineBy() {
        return declineBy;
    }

    /**
     * Sets the value of the declineBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclineBy(String value) {
        this.declineBy = value;
    }

    /**
     * Gets the value of the cli property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLI() {
        return cli;
    }

    /**
     * Sets the value of the cli property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLI(String value) {
        this.cli = value;
    }

    /**
     * Gets the value of the attachmentAvailable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentAvailable() {
        return attachmentAvailable;
    }

    /**
     * Sets the value of the attachmentAvailable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentAvailable(String value) {
        this.attachmentAvailable = value;
    }

    /**
     * Gets the value of the attachments property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentListType }
     *     
     */
    public AttachmentListType getAttachments() {
        return attachments;
    }

    /**
     * Sets the value of the attachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentListType }
     *     
     */
    public void setAttachments(AttachmentListType value) {
        this.attachments = value;
    }

    /**
     * Gets the value of the ctrlConfig property.
     * 
     * @return
     *     possible object is
     *     {@link CtrlConfigItemType }
     *     
     */
    public CtrlConfigItemType getCtrlConfig() {
        return ctrlConfig;
    }

    /**
     * Sets the value of the ctrlConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtrlConfigItemType }
     *     
     */
    public void setCtrlConfig(CtrlConfigItemType value) {
        this.ctrlConfig = value;
    }

    /**
     * Gets the value of the isRoutingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRoutingEnabled() {
        return isRoutingEnabled;
    }

    /**
     * Sets the value of the isRoutingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRoutingEnabled(String value) {
        this.isRoutingEnabled = value;
    }

    /**
     * Gets the value of the smsAssociated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSAssociated() {
        return smsAssociated;
    }

    /**
     * Sets the value of the smsAssociated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSAssociated(String value) {
        this.smsAssociated = value;
    }

    /**
     * Gets the value of the sendInformationSMS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInformationSMS() {
        return sendInformationSMS;
    }

    /**
     * Sets the value of the sendInformationSMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInformationSMS(String value) {
        this.sendInformationSMS = value;
    }

    /**
     * Gets the value of the agentDomainId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDomainId() {
        return agentDomainId;
    }

    /**
     * Sets the value of the agentDomainId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDomainId(String value) {
        this.agentDomainId = value;
    }

}
