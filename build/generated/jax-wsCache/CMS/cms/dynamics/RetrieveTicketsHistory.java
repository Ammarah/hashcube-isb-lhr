
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SecurityParams" type="{http://www.ufone.com/CMS/}SecurityParamsType"/>
 *         &lt;element name="AgentDomainId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchCriteria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkCodeTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "securityParams",
    "agentDomainId",
    "searchCriteria",
    "searchText",
    "fromDate",
    "toDate",
    "workCodeTypeId"
})
@XmlRootElement(name = "RetrieveTicketsHistory")
public class RetrieveTicketsHistory {

    @XmlElement(name = "SecurityParams", required = true)
    protected SecurityParamsType securityParams;
    @XmlElement(name = "AgentDomainId")
    protected String agentDomainId;
    @XmlElement(name = "SearchCriteria")
    protected String searchCriteria;
    @XmlElement(name = "SearchText")
    protected String searchText;
    @XmlElement(name = "FromDate")
    protected String fromDate;
    @XmlElement(name = "ToDate")
    protected String toDate;
    @XmlElement(name = "WorkCodeTypeId")
    protected String workCodeTypeId;

    /**
     * Gets the value of the securityParams property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityParamsType }
     *     
     */
    public SecurityParamsType getSecurityParams() {
        return securityParams;
    }

    /**
     * Sets the value of the securityParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityParamsType }
     *     
     */
    public void setSecurityParams(SecurityParamsType value) {
        this.securityParams = value;
    }

    /**
     * Gets the value of the agentDomainId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDomainId() {
        return agentDomainId;
    }

    /**
     * Sets the value of the agentDomainId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDomainId(String value) {
        this.agentDomainId = value;
    }

    /**
     * Gets the value of the searchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCriteria() {
        return searchCriteria;
    }

    /**
     * Sets the value of the searchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCriteria(String value) {
        this.searchCriteria = value;
    }

    /**
     * Gets the value of the searchText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * Sets the value of the searchText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchText(String value) {
        this.searchText = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDate(String value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToDate(String value) {
        this.toDate = value;
    }

    /**
     * Gets the value of the workCodeTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkCodeTypeId() {
        return workCodeTypeId;
    }

    /**
     * Sets the value of the workCodeTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkCodeTypeId(String value) {
        this.workCodeTypeId = value;
    }

}
