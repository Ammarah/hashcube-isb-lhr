
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SecurityParams" type="{http://www.ufone.com/CMS/}SecurityParamsType"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RgMSISDNs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Identifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComplaintId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TATEndOn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RgName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "securityParams",
    "msisdn",
    "rgMSISDNs",
    "identifier",
    "complaintId",
    "tatEndOn",
    "rgName",
    "emailText"
})
@XmlRootElement(name = "SendSMSAndEmailText")
public class SendSMSAndEmailText {

    @XmlElement(name = "SecurityParams", required = true)
    protected SecurityParamsType securityParams;
    @XmlElement(name = "MSISDN")
    protected String msisdn;
    @XmlElement(name = "RgMSISDNs")
    protected String rgMSISDNs;
    @XmlElement(name = "Identifier")
    protected String identifier;
    @XmlElement(name = "ComplaintId")
    protected String complaintId;
    @XmlElement(name = "TATEndOn")
    protected String tatEndOn;
    @XmlElement(name = "RgName")
    protected String rgName;
    @XmlElement(name = "EmailText")
    protected String emailText;

    /**
     * Gets the value of the securityParams property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityParamsType }
     *     
     */
    public SecurityParamsType getSecurityParams() {
        return securityParams;
    }

    /**
     * Sets the value of the securityParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityParamsType }
     *     
     */
    public void setSecurityParams(SecurityParamsType value) {
        this.securityParams = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the rgMSISDNs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRgMSISDNs() {
        return rgMSISDNs;
    }

    /**
     * Sets the value of the rgMSISDNs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRgMSISDNs(String value) {
        this.rgMSISDNs = value;
    }

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifier(String value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the complaintId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintId() {
        return complaintId;
    }

    /**
     * Sets the value of the complaintId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintId(String value) {
        this.complaintId = value;
    }

    /**
     * Gets the value of the tatEndOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTATEndOn() {
        return tatEndOn;
    }

    /**
     * Sets the value of the tatEndOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTATEndOn(String value) {
        this.tatEndOn = value;
    }

    /**
     * Gets the value of the rgName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRgName() {
        return rgName;
    }

    /**
     * Sets the value of the rgName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRgName(String value) {
        this.rgName = value;
    }

    /**
     * Gets the value of the emailText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailText() {
        return emailText;
    }

    /**
     * Sets the value of the emailText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailText(String value) {
        this.emailText = value;
    }

}
