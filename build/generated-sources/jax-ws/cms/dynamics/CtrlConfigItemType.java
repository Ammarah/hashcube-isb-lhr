
package cms.dynamics;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CtrlConfigItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CtrlConfigItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CtrlConfig" type="{http://www.ufone.com/CMS/}CtrlConfigType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtrlConfigItemType", propOrder = {
    "ctrlConfig"
})
public class CtrlConfigItemType {

    @XmlElement(name = "CtrlConfig")
    protected List<CtrlConfigType> ctrlConfig;

    /**
     * Gets the value of the ctrlConfig property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ctrlConfig property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCtrlConfig().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CtrlConfigType }
     * 
     * 
     */
    public List<CtrlConfigType> getCtrlConfig() {
        if (ctrlConfig == null) {
            ctrlConfig = new ArrayList<CtrlConfigType>();
        }
        return this.ctrlConfig;
    }

}
