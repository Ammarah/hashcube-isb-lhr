
package cms.dynamics;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cms.dynamics package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cms.dynamics
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendEmailResponse }
     * 
     */
    public SendEmailResponse createSendEmailResponse() {
        return new SendEmailResponse();
    }

    /**
     * Create an instance of {@link ReturnParamsType }
     * 
     */
    public ReturnParamsType createReturnParamsType() {
        return new ReturnParamsType();
    }

    /**
     * Create an instance of {@link CreateTicketResponse }
     * 
     */
    public CreateTicketResponse createCreateTicketResponse() {
        return new CreateTicketResponse();
    }

    /**
     * Create an instance of {@link RetrieveTicketAttachmentsResponse }
     * 
     */
    public RetrieveTicketAttachmentsResponse createRetrieveTicketAttachmentsResponse() {
        return new RetrieveTicketAttachmentsResponse();
    }

    /**
     * Create an instance of {@link AttachmentListType }
     * 
     */
    public AttachmentListType createAttachmentListType() {
        return new AttachmentListType();
    }

    /**
     * Create an instance of {@link RetrieveHotTicketTAT }
     * 
     */
    public RetrieveHotTicketTAT createRetrieveHotTicketTAT() {
        return new RetrieveHotTicketTAT();
    }

    /**
     * Create an instance of {@link SecurityParamsType }
     * 
     */
    public SecurityParamsType createSecurityParamsType() {
        return new SecurityParamsType();
    }

    /**
     * Create an instance of {@link RetrieveDomainsResponse }
     * 
     */
    public RetrieveDomainsResponse createRetrieveDomainsResponse() {
        return new RetrieveDomainsResponse();
    }

    /**
     * Create an instance of {@link DomainListType }
     * 
     */
    public DomainListType createDomainListType() {
        return new DomainListType();
    }

    /**
     * Create an instance of {@link UpdateAgent }
     * 
     */
    public UpdateAgent createUpdateAgent() {
        return new UpdateAgent();
    }

    /**
     * Create an instance of {@link RetrieveAgentWorkCodes }
     * 
     */
    public RetrieveAgentWorkCodes createRetrieveAgentWorkCodes() {
        return new RetrieveAgentWorkCodes();
    }

    /**
     * Create an instance of {@link RetrieveLovTypes }
     * 
     */
    public RetrieveLovTypes createRetrieveLovTypes() {
        return new RetrieveLovTypes();
    }

    /**
     * Create an instance of {@link RetrieveAgentWorkCodeDetailsResponse }
     * 
     */
    public RetrieveAgentWorkCodeDetailsResponse createRetrieveAgentWorkCodeDetailsResponse() {
        return new RetrieveAgentWorkCodeDetailsResponse();
    }

    /**
     * Create an instance of {@link WorkCodeType }
     * 
     */
    public WorkCodeType createWorkCodeType() {
        return new WorkCodeType();
    }

    /**
     * Create an instance of {@link CreateAgent }
     * 
     */
    public CreateAgent createCreateAgent() {
        return new CreateAgent();
    }

    /**
     * Create an instance of {@link AgentType }
     * 
     */
    public AgentType createAgentType() {
        return new AgentType();
    }

    /**
     * Create an instance of {@link RetrieveDomains }
     * 
     */
    public RetrieveDomains createRetrieveDomains() {
        return new RetrieveDomains();
    }

    /**
     * Create an instance of {@link CreateHotTicketResponse }
     * 
     */
    public CreateHotTicketResponse createCreateHotTicketResponse() {
        return new CreateHotTicketResponse();
    }

    /**
     * Create an instance of {@link ChangeCustomerStatus }
     * 
     */
    public ChangeCustomerStatus createChangeCustomerStatus() {
        return new ChangeCustomerStatus();
    }

    /**
     * Create an instance of {@link CreateTicket }
     * 
     */
    public CreateTicket createCreateTicket() {
        return new CreateTicket();
    }

    /**
     * Create an instance of {@link TicketType }
     * 
     */
    public TicketType createTicketType() {
        return new TicketType();
    }

    /**
     * Create an instance of {@link UpdateCustomer }
     * 
     */
    public UpdateCustomer createUpdateCustomer() {
        return new UpdateCustomer();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link CreateAgentProfile }
     * 
     */
    public CreateAgentProfile createCreateAgentProfile() {
        return new CreateAgentProfile();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodeCategories }
     * 
     */
    public RetrieveWorkCodeCategories createRetrieveWorkCodeCategories() {
        return new RetrieveWorkCodeCategories();
    }

    /**
     * Create an instance of {@link GenericResponse }
     * 
     */
    public GenericResponse createGenericResponse() {
        return new GenericResponse();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link RetrieveHotTicketTATResponse }
     * 
     */
    public RetrieveHotTicketTATResponse createRetrieveHotTicketTATResponse() {
        return new RetrieveHotTicketTATResponse();
    }

    /**
     * Create an instance of {@link ChangeAgentProfileStatusResponse }
     * 
     */
    public ChangeAgentProfileStatusResponse createChangeAgentProfileStatusResponse() {
        return new ChangeAgentProfileStatusResponse();
    }

    /**
     * Create an instance of {@link UpdateAgentProfile }
     * 
     */
    public UpdateAgentProfile createUpdateAgentProfile() {
        return new UpdateAgentProfile();
    }

    /**
     * Create an instance of {@link CreateHotTicket }
     * 
     */
    public CreateHotTicket createCreateHotTicket() {
        return new CreateHotTicket();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodeTypesResponse }
     * 
     */
    public RetrieveWorkCodeTypesResponse createRetrieveWorkCodeTypesResponse() {
        return new RetrieveWorkCodeTypesResponse();
    }

    /**
     * Create an instance of {@link WorkCodeTypeListType }
     * 
     */
    public WorkCodeTypeListType createWorkCodeTypeListType() {
        return new WorkCodeTypeListType();
    }

    /**
     * Create an instance of {@link SendSMSAndEmailTextResponse }
     * 
     */
    public SendSMSAndEmailTextResponse createSendSMSAndEmailTextResponse() {
        return new SendSMSAndEmailTextResponse();
    }

    /**
     * Create an instance of {@link ChangeAgentStatus }
     * 
     */
    public ChangeAgentStatus createChangeAgentStatus() {
        return new ChangeAgentStatus();
    }

    /**
     * Create an instance of {@link DeclineTicket }
     * 
     */
    public DeclineTicket createDeclineTicket() {
        return new DeclineTicket();
    }

    /**
     * Create an instance of {@link UpdateTicketActionRemarks }
     * 
     */
    public UpdateTicketActionRemarks createUpdateTicketActionRemarks() {
        return new UpdateTicketActionRemarks();
    }

    /**
     * Create an instance of {@link BoostTicketPriority }
     * 
     */
    public BoostTicketPriority createBoostTicketPriority() {
        return new BoostTicketPriority();
    }

    /**
     * Create an instance of {@link BoostTicketPriorityResponse }
     * 
     */
    public BoostTicketPriorityResponse createBoostTicketPriorityResponse() {
        return new BoostTicketPriorityResponse();
    }

    /**
     * Create an instance of {@link DeclineTicketResponse }
     * 
     */
    public DeclineTicketResponse createDeclineTicketResponse() {
        return new DeclineTicketResponse();
    }

    /**
     * Create an instance of {@link SendEmail }
     * 
     */
    public SendEmail createSendEmail() {
        return new SendEmail();
    }

    /**
     * Create an instance of {@link CreateAgentProfileResponse }
     * 
     */
    public CreateAgentProfileResponse createCreateAgentProfileResponse() {
        return new CreateAgentProfileResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerResponse }
     * 
     */
    public UpdateCustomerResponse createUpdateCustomerResponse() {
        return new UpdateCustomerResponse();
    }

    /**
     * Create an instance of {@link RetrieveLovTypesResponse }
     * 
     */
    public RetrieveLovTypesResponse createRetrieveLovTypesResponse() {
        return new RetrieveLovTypesResponse();
    }

    /**
     * Create an instance of {@link LovListItemType }
     * 
     */
    public LovListItemType createLovListItemType() {
        return new LovListItemType();
    }

    /**
     * Create an instance of {@link RetrieveTicketsHistory }
     * 
     */
    public RetrieveTicketsHistory createRetrieveTicketsHistory() {
        return new RetrieveTicketsHistory();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodes }
     * 
     */
    public RetrieveWorkCodes createRetrieveWorkCodes() {
        return new RetrieveWorkCodes();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodeTypes }
     * 
     */
    public RetrieveWorkCodeTypes createRetrieveWorkCodeTypes() {
        return new RetrieveWorkCodeTypes();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodeCategoriesResponse }
     * 
     */
    public RetrieveWorkCodeCategoriesResponse createRetrieveWorkCodeCategoriesResponse() {
        return new RetrieveWorkCodeCategoriesResponse();
    }

    /**
     * Create an instance of {@link WorkCodeCategoryListType }
     * 
     */
    public WorkCodeCategoryListType createWorkCodeCategoryListType() {
        return new WorkCodeCategoryListType();
    }

    /**
     * Create an instance of {@link RetrieveLovs }
     * 
     */
    public RetrieveLovs createRetrieveLovs() {
        return new RetrieveLovs();
    }

    /**
     * Create an instance of {@link ChangeAgentStatusResponse }
     * 
     */
    public ChangeAgentStatusResponse createChangeAgentStatusResponse() {
        return new ChangeAgentStatusResponse();
    }

    /**
     * Create an instance of {@link RetrieveTicketsHistoryResponse }
     * 
     */
    public RetrieveTicketsHistoryResponse createRetrieveTicketsHistoryResponse() {
        return new RetrieveTicketsHistoryResponse();
    }

    /**
     * Create an instance of {@link TicketsListType }
     * 
     */
    public TicketsListType createTicketsListType() {
        return new TicketsListType();
    }

    /**
     * Create an instance of {@link RetrieveWorkCodesResponse }
     * 
     */
    public RetrieveWorkCodesResponse createRetrieveWorkCodesResponse() {
        return new RetrieveWorkCodesResponse();
    }

    /**
     * Create an instance of {@link WorkCodesListType }
     * 
     */
    public WorkCodesListType createWorkCodesListType() {
        return new WorkCodesListType();
    }

    /**
     * Create an instance of {@link CreateFeedbackSMS }
     * 
     */
    public CreateFeedbackSMS createCreateFeedbackSMS() {
        return new CreateFeedbackSMS();
    }

    /**
     * Create an instance of {@link RetrieveAgentWorkCodeDetails }
     * 
     */
    public RetrieveAgentWorkCodeDetails createRetrieveAgentWorkCodeDetails() {
        return new RetrieveAgentWorkCodeDetails();
    }

    /**
     * Create an instance of {@link CreateFeedbackSMSResponse }
     * 
     */
    public CreateFeedbackSMSResponse createCreateFeedbackSMSResponse() {
        return new CreateFeedbackSMSResponse();
    }

    /**
     * Create an instance of {@link UpdateTicketActionRemarksResponse }
     * 
     */
    public UpdateTicketActionRemarksResponse createUpdateTicketActionRemarksResponse() {
        return new UpdateTicketActionRemarksResponse();
    }

    /**
     * Create an instance of {@link ChangeCustomerStatusResponse }
     * 
     */
    public ChangeCustomerStatusResponse createChangeCustomerStatusResponse() {
        return new ChangeCustomerStatusResponse();
    }

    /**
     * Create an instance of {@link RetrieveLovsResponse }
     * 
     */
    public RetrieveLovsResponse createRetrieveLovsResponse() {
        return new RetrieveLovsResponse();
    }

    /**
     * Create an instance of {@link UpdateAgentResponse }
     * 
     */
    public UpdateAgentResponse createUpdateAgentResponse() {
        return new UpdateAgentResponse();
    }

    /**
     * Create an instance of {@link ChangeAgentProfileStatus }
     * 
     */
    public ChangeAgentProfileStatus createChangeAgentProfileStatus() {
        return new ChangeAgentProfileStatus();
    }

    /**
     * Create an instance of {@link CreateAgentResponse }
     * 
     */
    public CreateAgentResponse createCreateAgentResponse() {
        return new CreateAgentResponse();
    }

    /**
     * Create an instance of {@link RetrieveAgentWorkCodesResponse }
     * 
     */
    public RetrieveAgentWorkCodesResponse createRetrieveAgentWorkCodesResponse() {
        return new RetrieveAgentWorkCodesResponse();
    }

    /**
     * Create an instance of {@link UpdateAgentProfileResponse }
     * 
     */
    public UpdateAgentProfileResponse createUpdateAgentProfileResponse() {
        return new UpdateAgentProfileResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse }
     * 
     */
    public CreateCustomerResponse createCreateCustomerResponse() {
        return new CreateCustomerResponse();
    }

    /**
     * Create an instance of {@link RetrieveTicketAttachments }
     * 
     */
    public RetrieveTicketAttachments createRetrieveTicketAttachments() {
        return new RetrieveTicketAttachments();
    }

    /**
     * Create an instance of {@link SendSMSAndEmailText }
     * 
     */
    public SendSMSAndEmailText createSendSMSAndEmailText() {
        return new SendSMSAndEmailText();
    }

    /**
     * Create an instance of {@link CreateCustomer }
     * 
     */
    public CreateCustomer createCreateCustomer() {
        return new CreateCustomer();
    }

    /**
     * Create an instance of {@link AgentProfileType }
     * 
     */
    public AgentProfileType createAgentProfileType() {
        return new AgentProfileType();
    }

    /**
     * Create an instance of {@link WorkCodesListType1 }
     * 
     */
    public WorkCodesListType1 createWorkCodesListType1() {
        return new WorkCodesListType1();
    }

    /**
     * Create an instance of {@link FilesToUploadType }
     * 
     */
    public FilesToUploadType createFilesToUploadType() {
        return new FilesToUploadType();
    }

    /**
     * Create an instance of {@link AgentProfileListType }
     * 
     */
    public AgentProfileListType createAgentProfileListType() {
        return new AgentProfileListType();
    }

    /**
     * Create an instance of {@link DomainType }
     * 
     */
    public DomainType createDomainType() {
        return new DomainType();
    }

    /**
     * Create an instance of {@link CtrlConfigItemType }
     * 
     */
    public CtrlConfigItemType createCtrlConfigItemType() {
        return new CtrlConfigItemType();
    }

    /**
     * Create an instance of {@link CtrlConfigType1 }
     * 
     */
    public CtrlConfigType1 createCtrlConfigType1() {
        return new CtrlConfigType1();
    }

    /**
     * Create an instance of {@link AgentProfileItemType }
     * 
     */
    public AgentProfileItemType createAgentProfileItemType() {
        return new AgentProfileItemType();
    }

    /**
     * Create an instance of {@link NewComplexType }
     * 
     */
    public NewComplexType createNewComplexType() {
        return new NewComplexType();
    }

    /**
     * Create an instance of {@link WorkCodeType1 }
     * 
     */
    public WorkCodeType1 createWorkCodeType1() {
        return new WorkCodeType1();
    }

    /**
     * Create an instance of {@link AttachmentType }
     * 
     */
    public AttachmentType createAttachmentType() {
        return new AttachmentType();
    }

    /**
     * Create an instance of {@link CtrlConfigType }
     * 
     */
    public CtrlConfigType createCtrlConfigType() {
        return new CtrlConfigType();
    }

    /**
     * Create an instance of {@link LovType }
     * 
     */
    public LovType createLovType() {
        return new LovType();
    }

}
