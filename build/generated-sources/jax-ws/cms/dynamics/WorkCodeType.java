
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomainId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttachmentAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSMSAssociated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BatchLoggingAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentProfileList" type="{http://www.ufone.com/CMS/}AgentProfileItemType" minOccurs="0"/>
 *         &lt;element name="CtrlConfigList" type="{http://www.ufone.com/CMS/}CtrlConfigItemType" minOccurs="0"/>
 *         &lt;element name="EnableRouting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkCodeType", propOrder = {
    "id",
    "name",
    "typeId",
    "typeName",
    "categoryId",
    "categoryName",
    "domainId",
    "domainName",
    "attachmentAllowed",
    "isSMSAssociated",
    "batchLoggingAllowed",
    "tat",
    "agentProfileList",
    "ctrlConfigList",
    "enableRouting"
})
public class WorkCodeType {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "TypeId")
    protected String typeId;
    @XmlElement(name = "TypeName")
    protected String typeName;
    @XmlElement(name = "CategoryId")
    protected String categoryId;
    @XmlElement(name = "CategoryName")
    protected String categoryName;
    @XmlElement(name = "DomainId")
    protected String domainId;
    @XmlElement(name = "DomainName")
    protected String domainName;
    @XmlElement(name = "AttachmentAllowed")
    protected String attachmentAllowed;
    @XmlElement(name = "IsSMSAssociated")
    protected String isSMSAssociated;
    @XmlElement(name = "BatchLoggingAllowed")
    protected String batchLoggingAllowed;
    @XmlElement(name = "TAT")
    protected String tat;
    @XmlElement(name = "AgentProfileList")
    protected AgentProfileItemType agentProfileList;
    @XmlElement(name = "CtrlConfigList")
    protected CtrlConfigItemType ctrlConfigList;
    @XmlElement(name = "EnableRouting")
    protected String enableRouting;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the typeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * Sets the value of the typeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeId(String value) {
        this.typeId = value;
    }

    /**
     * Gets the value of the typeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the value of the typeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeName(String value) {
        this.typeName = value;
    }

    /**
     * Gets the value of the categoryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets the value of the categoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryId(String value) {
        this.categoryId = value;
    }

    /**
     * Gets the value of the categoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets the value of the categoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryName(String value) {
        this.categoryName = value;
    }

    /**
     * Gets the value of the domainId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainId() {
        return domainId;
    }

    /**
     * Sets the value of the domainId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainId(String value) {
        this.domainId = value;
    }

    /**
     * Gets the value of the domainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the value of the domainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Gets the value of the attachmentAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentAllowed() {
        return attachmentAllowed;
    }

    /**
     * Sets the value of the attachmentAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentAllowed(String value) {
        this.attachmentAllowed = value;
    }

    /**
     * Gets the value of the isSMSAssociated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSMSAssociated() {
        return isSMSAssociated;
    }

    /**
     * Sets the value of the isSMSAssociated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSMSAssociated(String value) {
        this.isSMSAssociated = value;
    }

    /**
     * Gets the value of the batchLoggingAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchLoggingAllowed() {
        return batchLoggingAllowed;
    }

    /**
     * Sets the value of the batchLoggingAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchLoggingAllowed(String value) {
        this.batchLoggingAllowed = value;
    }

    /**
     * Gets the value of the tat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAT() {
        return tat;
    }

    /**
     * Sets the value of the tat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAT(String value) {
        this.tat = value;
    }

    /**
     * Gets the value of the agentProfileList property.
     * 
     * @return
     *     possible object is
     *     {@link AgentProfileItemType }
     *     
     */
    public AgentProfileItemType getAgentProfileList() {
        return agentProfileList;
    }

    /**
     * Sets the value of the agentProfileList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentProfileItemType }
     *     
     */
    public void setAgentProfileList(AgentProfileItemType value) {
        this.agentProfileList = value;
    }

    /**
     * Gets the value of the ctrlConfigList property.
     * 
     * @return
     *     possible object is
     *     {@link CtrlConfigItemType }
     *     
     */
    public CtrlConfigItemType getCtrlConfigList() {
        return ctrlConfigList;
    }

    /**
     * Sets the value of the ctrlConfigList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtrlConfigItemType }
     *     
     */
    public void setCtrlConfigList(CtrlConfigItemType value) {
        this.ctrlConfigList = value;
    }

    /**
     * Gets the value of the enableRouting property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableRouting() {
        return enableRouting;
    }

    /**
     * Sets the value of the enableRouting property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableRouting(String value) {
        this.enableRouting = value;
    }

}
