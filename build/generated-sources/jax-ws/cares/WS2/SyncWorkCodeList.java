
package cares.WS2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operusername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operpassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wrkCode" type="{http://www.ufonegsm.net/ADT/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="rsl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result_Desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "operusername",
    "operpassword",
    "wrkCode",
    "rsl",
    "resultDesc"
})
@XmlRootElement(name = "Sync_Work_Code_List")
public class SyncWorkCodeList {

    protected String operusername;
    protected String operpassword;
    protected ArrayOfString wrkCode;
    protected String rsl;
    @XmlElement(name = "Result_Desc")
    protected String resultDesc;

    /**
     * Gets the value of the operusername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperusername() {
        return operusername;
    }

    /**
     * Sets the value of the operusername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperusername(String value) {
        this.operusername = value;
    }

    /**
     * Gets the value of the operpassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperpassword() {
        return operpassword;
    }

    /**
     * Sets the value of the operpassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperpassword(String value) {
        this.operpassword = value;
    }

    /**
     * Gets the value of the wrkCode property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getWrkCode() {
        return wrkCode;
    }

    /**
     * Sets the value of the wrkCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setWrkCode(ArrayOfString value) {
        this.wrkCode = value;
    }

    /**
     * Gets the value of the rsl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsl() {
        return rsl;
    }

    /**
     * Sets the value of the rsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsl(String value) {
        this.rsl = value;
    }

    /**
     * Gets the value of the resultDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

}
