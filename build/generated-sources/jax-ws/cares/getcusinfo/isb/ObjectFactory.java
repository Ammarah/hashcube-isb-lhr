
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cares.getcusinfo.isb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cares.getcusinfo.isb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSimTypeResponse }
     * 
     */
    public GetSimTypeResponse createGetSimTypeResponse() {
        return new GetSimTypeResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInfoResponse }
     * 
     */
    public GetCustomerInfoResponse createGetCustomerInfoResponse() {
        return new GetCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link GetCustomerRSPType }
     * 
     */
    public GetCustomerRSPType createGetCustomerRSPType() {
        return new GetCustomerRSPType();
    }

    /**
     * Create an instance of {@link GetCustomerInfo_Type }
     * 
     */
    public GetCustomerInfo_Type createGetCustomerInfo_Type() {
        return new GetCustomerInfo_Type();
    }

    /**
     * Create an instance of {@link SecurityParamType }
     * 
     */
    public SecurityParamType createSecurityParamType() {
        return new SecurityParamType();
    }

    /**
     * Create an instance of {@link GetSimType }
     * 
     */
    public GetSimType createGetSimType() {
        return new GetSimType();
    }

    /**
     * Create an instance of {@link SuperCardQuery }
     * 
     */
    public SuperCardQuery createSuperCardQuery() {
        return new SuperCardQuery();
    }

    /**
     * Create an instance of {@link SuperCardQueryResponse }
     * 
     */
    public SuperCardQueryResponse createSuperCardQueryResponse() {
        return new SuperCardQueryResponse();
    }

    /**
     * Create an instance of {@link ReturnParams }
     * 
     */
    public ReturnParams createReturnParams() {
        return new ReturnParams();
    }

    /**
     * Create an instance of {@link VPNType }
     * 
     */
    public VPNType createVPNType() {
        return new VPNType();
    }

    /**
     * Create an instance of {@link CustomerInfoType }
     * 
     */
    public CustomerInfoType createCustomerInfoType() {
        return new CustomerInfoType();
    }

    /**
     * Create an instance of {@link SIMINFOType }
     * 
     */
    public SIMINFOType createSIMINFOType() {
        return new SIMINFOType();
    }

    /**
     * Create an instance of {@link PaymentInfoType }
     * 
     */
    public PaymentInfoType createPaymentInfoType() {
        return new PaymentInfoType();
    }

    /**
     * Create an instance of {@link ConnectionType }
     * 
     */
    public ConnectionType createConnectionType() {
        return new ConnectionType();
    }

    /**
     * Create an instance of {@link MSISDNINFOType }
     * 
     */
    public MSISDNINFOType createMSISDNINFOType() {
        return new MSISDNINFOType();
    }

    /**
     * Create an instance of {@link VASType }
     * 
     */
    public VASType createVASType() {
        return new VASType();
    }

    /**
     * Create an instance of {@link CurrentVASType }
     * 
     */
    public CurrentVASType createCurrentVASType() {
        return new CurrentVASType();
    }

}
