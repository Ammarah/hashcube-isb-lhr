
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RetDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerInfoResponse" type="{http://www.ufone.com/GetCustomerInfo/}GetCustomerRSPType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retCode",
    "retDesc",
    "customerInfoResponse"
})
@XmlRootElement(name = "GetCustomerInfoResponse")
public class GetCustomerInfoResponse {

    @XmlElement(name = "RetCode", required = true)
    protected String retCode;
    @XmlElement(name = "RetDesc", required = true)
    protected String retDesc;
    @XmlElement(name = "CustomerInfoResponse")
    protected GetCustomerRSPType customerInfoResponse;

    /**
     * Gets the value of the retCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetCode() {
        return retCode;
    }

    /**
     * Sets the value of the retCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetCode(String value) {
        this.retCode = value;
    }

    /**
     * Gets the value of the retDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetDesc() {
        return retDesc;
    }

    /**
     * Sets the value of the retDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetDesc(String value) {
        this.retDesc = value;
    }

    /**
     * Gets the value of the customerInfoResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetCustomerRSPType }
     *     
     */
    public GetCustomerRSPType getCustomerInfoResponse() {
        return customerInfoResponse;
    }

    /**
     * Sets the value of the customerInfoResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCustomerRSPType }
     *     
     */
    public void setCustomerInfoResponse(GetCustomerRSPType value) {
        this.customerInfoResponse = value;
    }

}
