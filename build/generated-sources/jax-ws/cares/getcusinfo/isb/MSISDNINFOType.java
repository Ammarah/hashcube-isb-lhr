
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MSISDNINFOType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MSISDNINFOType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IMSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentIMSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HLRID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSISDNTYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSISDNINFOType", propOrder = {
    "imsi",
    "currentIMSI",
    "hlrid",
    "msisdntype"
})
public class MSISDNINFOType {

    @XmlElement(name = "IMSI")
    protected String imsi;
    @XmlElement(name = "CurrentIMSI")
    protected String currentIMSI;
    @XmlElement(name = "HLRID")
    protected String hlrid;
    @XmlElement(name = "MSISDNTYPE")
    protected String msisdntype;

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMSI() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMSI(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the currentIMSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentIMSI() {
        return currentIMSI;
    }

    /**
     * Sets the value of the currentIMSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentIMSI(String value) {
        this.currentIMSI = value;
    }

    /**
     * Gets the value of the hlrid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHLRID() {
        return hlrid;
    }

    /**
     * Sets the value of the hlrid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHLRID(String value) {
        this.hlrid = value;
    }

    /**
     * Gets the value of the msisdntype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDNTYPE() {
        return msisdntype;
    }

    /**
     * Sets the value of the msisdntype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDNTYPE(String value) {
        this.msisdntype = value;
    }

}
