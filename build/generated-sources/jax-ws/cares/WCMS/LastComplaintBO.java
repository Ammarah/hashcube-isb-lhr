
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LastComplaintBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LastComplaintBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="complaintCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complaintId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LastComplaintBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "complaintCategory",
    "complaintId"
})
public class LastComplaintBO {

    protected String complaintCategory;
    protected String complaintId;

    /**
     * Gets the value of the complaintCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintCategory() {
        return complaintCategory;
    }

    /**
     * Sets the value of the complaintCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintCategory(String value) {
        this.complaintCategory = value;
    }

    /**
     * Gets the value of the complaintId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintId() {
        return complaintId;
    }

    /**
     * Sets the value of the complaintId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintId(String value) {
        this.complaintId = value;
    }

}
