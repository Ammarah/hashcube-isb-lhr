
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="informationalWorkCodeList" type="{http://Ufone-WCMS-Integration}InformationalWorkCodeListBO"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="returnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "informationalWorkCodeList",
    "returnCode",
    "returnDesc"
})
@XmlRootElement(name = "getLastInformationalWorkCodesResponse")
public class GetLastInformationalWorkCodesResponse {

    @XmlElement(required = true, nillable = true)
    protected InformationalWorkCodeListBO informationalWorkCodeList;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer returnCode;
    @XmlElement(required = true, nillable = true)
    protected String returnDesc;

    /**
     * Gets the value of the informationalWorkCodeList property.
     * 
     * @return
     *     possible object is
     *     {@link InformationalWorkCodeListBO }
     *     
     */
    public InformationalWorkCodeListBO getInformationalWorkCodeList() {
        return informationalWorkCodeList;
    }

    /**
     * Sets the value of the informationalWorkCodeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link InformationalWorkCodeListBO }
     *     
     */
    public void setInformationalWorkCodeList(InformationalWorkCodeListBO value) {
        this.informationalWorkCodeList = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReturnCode(Integer value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the returnDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDesc() {
        return returnDesc;
    }

    /**
     * Sets the value of the returnDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDesc(String value) {
        this.returnDesc = value;
    }

}
