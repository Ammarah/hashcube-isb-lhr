
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkCodeListOutput" type="{http://Ufone-WCMS-Integration}WorkCodeListBO"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="returnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workCodeListOutput",
    "returnCode",
    "returnDesc"
})
@XmlRootElement(name = "GetComplaintWorkCodeListResponse")
public class GetComplaintWorkCodeListResponse {

    @XmlElement(name = "WorkCodeListOutput", required = true, nillable = true)
    protected WorkCodeListBO workCodeListOutput;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer returnCode;
    @XmlElement(required = true, nillable = true)
    protected String returnDesc;

    /**
     * Gets the value of the workCodeListOutput property.
     * 
     * @return
     *     possible object is
     *     {@link WorkCodeListBO }
     *     
     */
    public WorkCodeListBO getWorkCodeListOutput() {
        return workCodeListOutput;
    }

    /**
     * Sets the value of the workCodeListOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkCodeListBO }
     *     
     */
    public void setWorkCodeListOutput(WorkCodeListBO value) {
        this.workCodeListOutput = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReturnCode(Integer value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the returnDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDesc() {
        return returnDesc;
    }

    /**
     * Sets the value of the returnDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDesc(String value) {
        this.returnDesc = value;
    }

}
