
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for ComplaintBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplaintBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wc_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="wc_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cli" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="controlsData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imageFile" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
 *         &lt;element name="imageExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agentRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logAgentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplaintBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "wcName",
    "wcId",
    "wcType",
    "wcStatus",
    "msisdn",
    "cli",
    "customerEmail",
    "controlsData",
    "imageFile",
    "imageExt",
    "agentRemarks",
    "logAgentName"
})
public class ComplaintBO {

    @XmlElement(name = "wc_Name")
    protected String wcName;
    @XmlElement(name = "wc_Id")
    protected Integer wcId;
    @XmlElement(name = "wc_Type")
    protected String wcType;
    @XmlElement(name = "wc_Status")
    protected String wcStatus;
    @XmlElement(name = "MSISDN")
    protected String msisdn;
    protected String cli;
    protected String customerEmail;
    protected String controlsData;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(HexBinaryAdapter.class)
    @XmlSchemaType(name = "hexBinary")
    protected byte[] imageFile;
    protected String imageExt;
    protected String agentRemarks;
    protected String logAgentName;

    /**
     * Gets the value of the wcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcName() {
        return wcName;
    }

    /**
     * Sets the value of the wcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcName(String value) {
        this.wcName = value;
    }

    /**
     * Gets the value of the wcId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWcId() {
        return wcId;
    }

    /**
     * Sets the value of the wcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWcId(Integer value) {
        this.wcId = value;
    }

    /**
     * Gets the value of the wcType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcType() {
        return wcType;
    }

    /**
     * Sets the value of the wcType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcType(String value) {
        this.wcType = value;
    }

    /**
     * Gets the value of the wcStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcStatus() {
        return wcStatus;
    }

    /**
     * Sets the value of the wcStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcStatus(String value) {
        this.wcStatus = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the cli property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCli() {
        return cli;
    }

    /**
     * Sets the value of the cli property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCli(String value) {
        this.cli = value;
    }

    /**
     * Gets the value of the customerEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * Sets the value of the customerEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerEmail(String value) {
        this.customerEmail = value;
    }

    /**
     * Gets the value of the controlsData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlsData() {
        return controlsData;
    }

    /**
     * Sets the value of the controlsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlsData(String value) {
        this.controlsData = value;
    }

    /**
     * Gets the value of the imageFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public byte[] getImageFile() {
        return imageFile;
    }

    /**
     * Sets the value of the imageFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageFile(byte[] value) {
        this.imageFile = value;
    }

    /**
     * Gets the value of the imageExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageExt() {
        return imageExt;
    }

    /**
     * Sets the value of the imageExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageExt(String value) {
        this.imageExt = value;
    }

    /**
     * Gets the value of the agentRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentRemarks() {
        return agentRemarks;
    }

    /**
     * Sets the value of the agentRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentRemarks(String value) {
        this.agentRemarks = value;
    }

    /**
     * Gets the value of the logAgentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogAgentName() {
        return logAgentName;
    }

    /**
     * Sets the value of the logAgentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogAgentName(String value) {
        this.logAgentName = value;
    }

}
