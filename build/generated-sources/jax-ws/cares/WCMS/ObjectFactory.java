
package cares.WCMS;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cares.WCMS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LovDataBOLovId_QNAME = new QName("", "lov_id");
    private final static QName _LovDataBOLovName_QNAME = new QName("", "lov_name");
    private final static QName _LovDataBOLovValue_QNAME = new QName("", "lov_value");
    private final static QName _ComplaintInfoLogDate_QNAME = new QName("", "logDate");
    private final static QName _ComplaintInfoResolutionTime_QNAME = new QName("", "resolutionTime");
    private final static QName _ComplaintInfoClosingDate_QNAME = new QName("", "closingDate");
    private final static QName _ComplaintInfoRemarks_QNAME = new QName("", "remarks");
    private final static QName _ComplaintInfoComplaintCategory_QNAME = new QName("", "complaintCategory");
    private final static QName _ComplaintInfoStatus_QNAME = new QName("", "status");
    private final static QName _CityAreaClusterBOPCityAreaCluster_QNAME = new QName("", "p_city_area_cluster");
    private final static QName _CityAreaClusterBOPAreaName_QNAME = new QName("", "p_area_name");
    private final static QName _AutoFillControlBOPMsisdn_QNAME = new QName("", "p_msisdn");
    private final static QName _AutoFillControlBOPServiceCode_QNAME = new QName("", "p_service_code");
    private final static QName _AutoFillControlBOPAddress_QNAME = new QName("", "p_address");
    private final static QName _AutoFillControlBOPSimNo_QNAME = new QName("", "p_sim_no");
    private final static QName _AutoFillControlBOPPackageName_QNAME = new QName("", "p_package_name");
    private final static QName _AutoFillControlBOPConnectionType_QNAME = new QName("", "p_connection_type");
    private final static QName _AutoFillControlBOPNic_QNAME = new QName("", "p_nic");
    private final static QName _MetaDataBOWorkcodeu32Title_QNAME = new QName("", "workcodeu32title");
    private final static QName _MetaDataBOIsmandatory_QNAME = new QName("", "ismandatory");
    private final static QName _MetaDataBOFieldu32Name_QNAME = new QName("", "fieldu32name");
    private final static QName _MetaDataBOControlu32Name_QNAME = new QName("", "controlu32name");
    private final static QName _MetaDataBOSequencenumber_QNAME = new QName("", "sequencenumber");
    private final static QName _MetaDataBOScript_QNAME = new QName("", "script");
    private final static QName _MetaDataBOFieldu32Tooltip_QNAME = new QName("", "fieldu32tooltip");
    private final static QName _TaskBOWcname_QNAME = new QName("", "wcname");
    private final static QName _TaskBOComplaintlogenddate_QNAME = new QName("", "complaintlogenddate");
    private final static QName _TaskBOWcqualitystatus_QNAME = new QName("", "wcqualitystatus");
    private final static QName _TaskBOAgentremarks_QNAME = new QName("", "agentremarks");
    private final static QName _TaskBOLoggedby_QNAME = new QName("", "loggedby");
    private final static QName _TaskBOUserinformation_QNAME = new QName("", "userinformation");
    private final static QName _TaskBOActionremarks_QNAME = new QName("", "actionremarks");
    private final static QName _TaskBOIswithintat_QNAME = new QName("", "iswithintat");
    private final static QName _TaskBOPrevactionremarks_QNAME = new QName("", "prevactionremarks");
    private final static QName _TaskBOLoggedstatus_QNAME = new QName("", "loggedstatus");
    private final static QName _TaskBOWorkcodeid_QNAME = new QName("", "workcodeid");
    private final static QName _TaskBOLoglocation_QNAME = new QName("", "loglocation");
    private final static QName _TaskBOComplaintid_QNAME = new QName("", "complaintid");
    private final static QName _TaskBOClinumber_QNAME = new QName("", "clinumber");
    private final static QName _TaskBOComplaintlogstartdate_QNAME = new QName("", "complaintlogstartdate");
    private final static QName _TaskBOWcstatus_QNAME = new QName("", "wcstatus");
    private final static QName _TaskBOLogagentid_QNAME = new QName("", "logagentid");
    private final static QName _InformationalWorkCodeBOWorkcodename_QNAME = new QName("", "workcodename");
    private final static QName _InformationalWorkCodeBOLoggedlocation_QNAME = new QName("", "loggedlocation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cares.WCMS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeclineTicket }
     * 
     */
    public DeclineTicket createDeclineTicket() {
        return new DeclineTicket();
    }

    /**
     * Create an instance of {@link GetLastInformationalWorkCodes }
     * 
     */
    public GetLastInformationalWorkCodes createGetLastInformationalWorkCodes() {
        return new GetLastInformationalWorkCodes();
    }

    /**
     * Create an instance of {@link GetCityAreaClusterResponse }
     * 
     */
    public GetCityAreaClusterResponse createGetCityAreaClusterResponse() {
        return new GetCityAreaClusterResponse();
    }

    /**
     * Create an instance of {@link CityAreaClusterBO }
     * 
     */
    public CityAreaClusterBO createCityAreaClusterBO() {
        return new CityAreaClusterBO();
    }

    /**
     * Create an instance of {@link GetLastComplaintsResponse }
     * 
     */
    public GetLastComplaintsResponse createGetLastComplaintsResponse() {
        return new GetLastComplaintsResponse();
    }

    /**
     * Create an instance of {@link LastComplaintListBO }
     * 
     */
    public LastComplaintListBO createLastComplaintListBO() {
        return new LastComplaintListBO();
    }

    /**
     * Create an instance of {@link GetLastInformationalWorkCodesResponse }
     * 
     */
    public GetLastInformationalWorkCodesResponse createGetLastInformationalWorkCodesResponse() {
        return new GetLastInformationalWorkCodesResponse();
    }

    /**
     * Create an instance of {@link InformationalWorkCodeListBO }
     * 
     */
    public InformationalWorkCodeListBO createInformationalWorkCodeListBO() {
        return new InformationalWorkCodeListBO();
    }

    /**
     * Create an instance of {@link LogComplaintServiceResponse }
     * 
     */
    public LogComplaintServiceResponse createLogComplaintServiceResponse() {
        return new LogComplaintServiceResponse();
    }

    /**
     * Create an instance of {@link WCMSLogResponse }
     * 
     */
    public WCMSLogResponse createWCMSLogResponse() {
        return new WCMSLogResponse();
    }

    /**
     * Create an instance of {@link GetComplaintResponse }
     * 
     */
    public GetComplaintResponse createGetComplaintResponse() {
        return new GetComplaintResponse();
    }

    /**
     * Create an instance of {@link ComplaintInfo }
     * 
     */
    public ComplaintInfo createComplaintInfo() {
        return new ComplaintInfo();
    }

    /**
     * Create an instance of {@link GetCwMetaDataResponse }
     * 
     */
    public GetCwMetaDataResponse createGetCwMetaDataResponse() {
        return new GetCwMetaDataResponse();
    }

    /**
     * Create an instance of {@link MetaDataListBO }
     * 
     */
    public MetaDataListBO createMetaDataListBO() {
        return new MetaDataListBO();
    }

    /**
     * Create an instance of {@link DeclineTicketResponse }
     * 
     */
    public DeclineTicketResponse createDeclineTicketResponse() {
        return new DeclineTicketResponse();
    }

    /**
     * Create an instance of {@link GetLovData }
     * 
     */
    public GetLovData createGetLovData() {
        return new GetLovData();
    }

    /**
     * Create an instance of {@link GetLovDataByNameResponse }
     * 
     */
    public GetLovDataByNameResponse createGetLovDataByNameResponse() {
        return new GetLovDataByNameResponse();
    }

    /**
     * Create an instance of {@link LovDataListBO }
     * 
     */
    public LovDataListBO createLovDataListBO() {
        return new LovDataListBO();
    }

    /**
     * Create an instance of {@link GetCityAreaCluster }
     * 
     */
    public GetCityAreaCluster createGetCityAreaCluster() {
        return new GetCityAreaCluster();
    }

    /**
     * Create an instance of {@link GetLastComplaints }
     * 
     */
    public GetLastComplaints createGetLastComplaints() {
        return new GetLastComplaints();
    }

    /**
     * Create an instance of {@link LogComplaintService }
     * 
     */
    public LogComplaintService createLogComplaintService() {
        return new LogComplaintService();
    }

    /**
     * Create an instance of {@link ComplaintBO }
     * 
     */
    public ComplaintBO createComplaintBO() {
        return new ComplaintBO();
    }

    /**
     * Create an instance of {@link GetUserIdResponse }
     * 
     */
    public GetUserIdResponse createGetUserIdResponse() {
        return new GetUserIdResponse();
    }

    /**
     * Create an instance of {@link GetAutoFilledControlsResponse }
     * 
     */
    public GetAutoFilledControlsResponse createGetAutoFilledControlsResponse() {
        return new GetAutoFilledControlsResponse();
    }

    /**
     * Create an instance of {@link AutoFillControlBO }
     * 
     */
    public AutoFillControlBO createAutoFillControlBO() {
        return new AutoFillControlBO();
    }

    /**
     * Create an instance of {@link GetComplaintWorkCodeList }
     * 
     */
    public GetComplaintWorkCodeList createGetComplaintWorkCodeList() {
        return new GetComplaintWorkCodeList();
    }

    /**
     * Create an instance of {@link GetLovDataResponse }
     * 
     */
    public GetLovDataResponse createGetLovDataResponse() {
        return new GetLovDataResponse();
    }

    /**
     * Create an instance of {@link GetAutoFilledControls }
     * 
     */
    public GetAutoFilledControls createGetAutoFilledControls() {
        return new GetAutoFilledControls();
    }

    /**
     * Create an instance of {@link GetCwMetaData }
     * 
     */
    public GetCwMetaData createGetCwMetaData() {
        return new GetCwMetaData();
    }

    /**
     * Create an instance of {@link GetLovDataByName }
     * 
     */
    public GetLovDataByName createGetLovDataByName() {
        return new GetLovDataByName();
    }

    /**
     * Create an instance of {@link GetBalance }
     * 
     */
    public GetBalance createGetBalance() {
        return new GetBalance();
    }

    /**
     * Create an instance of {@link GetComplaintWorkCodeListResponse }
     * 
     */
    public GetComplaintWorkCodeListResponse createGetComplaintWorkCodeListResponse() {
        return new GetComplaintWorkCodeListResponse();
    }

    /**
     * Create an instance of {@link WorkCodeListBO }
     * 
     */
    public WorkCodeListBO createWorkCodeListBO() {
        return new WorkCodeListBO();
    }

    /**
     * Create an instance of {@link GetTasksResponse }
     * 
     */
    public GetTasksResponse createGetTasksResponse() {
        return new GetTasksResponse();
    }

    /**
     * Create an instance of {@link TasksListBO }
     * 
     */
    public TasksListBO createTasksListBO() {
        return new TasksListBO();
    }

    /**
     * Create an instance of {@link GetTasks }
     * 
     */
    public GetTasks createGetTasks() {
        return new GetTasks();
    }

    /**
     * Create an instance of {@link GetComplaint }
     * 
     */
    public GetComplaint createGetComplaint() {
        return new GetComplaint();
    }

    /**
     * Create an instance of {@link GetBalanceResponse }
     * 
     */
    public GetBalanceResponse createGetBalanceResponse() {
        return new GetBalanceResponse();
    }

    /**
     * Create an instance of {@link GetUserId }
     * 
     */
    public GetUserId createGetUserId() {
        return new GetUserId();
    }

    /**
     * Create an instance of {@link LastComplaintBO }
     * 
     */
    public LastComplaintBO createLastComplaintBO() {
        return new LastComplaintBO();
    }

    /**
     * Create an instance of {@link LovDataBO }
     * 
     */
    public LovDataBO createLovDataBO() {
        return new LovDataBO();
    }

    /**
     * Create an instance of {@link WorkCodeBO }
     * 
     */
    public WorkCodeBO createWorkCodeBO() {
        return new WorkCodeBO();
    }

    /**
     * Create an instance of {@link InformationalWorkCodeBO }
     * 
     */
    public InformationalWorkCodeBO createInformationalWorkCodeBO() {
        return new InformationalWorkCodeBO();
    }

    /**
     * Create an instance of {@link MetaDataBO }
     * 
     */
    public MetaDataBO createMetaDataBO() {
        return new MetaDataBO();
    }

    /**
     * Create an instance of {@link TaskBO }
     * 
     */
    public TaskBO createTaskBO() {
        return new TaskBO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lov_id", scope = LovDataBO.class)
    public JAXBElement<BigDecimal> createLovDataBOLovId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LovDataBOLovId_QNAME, BigDecimal.class, LovDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lov_name", scope = LovDataBO.class)
    public JAXBElement<String> createLovDataBOLovName(String value) {
        return new JAXBElement<String>(_LovDataBOLovName_QNAME, String.class, LovDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lov_value", scope = LovDataBO.class)
    public JAXBElement<String> createLovDataBOLovValue(String value) {
        return new JAXBElement<String>(_LovDataBOLovValue_QNAME, String.class, LovDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "logDate", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoLogDate(String value) {
        return new JAXBElement<String>(_ComplaintInfoLogDate_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resolutionTime", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoResolutionTime(String value) {
        return new JAXBElement<String>(_ComplaintInfoResolutionTime_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "closingDate", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoClosingDate(String value) {
        return new JAXBElement<String>(_ComplaintInfoClosingDate_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "remarks", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoRemarks(String value) {
        return new JAXBElement<String>(_ComplaintInfoRemarks_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complaintCategory", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoComplaintCategory(String value) {
        return new JAXBElement<String>(_ComplaintInfoComplaintCategory_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "status", scope = ComplaintInfo.class)
    public JAXBElement<String> createComplaintInfoStatus(String value) {
        return new JAXBElement<String>(_ComplaintInfoStatus_QNAME, String.class, ComplaintInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_city_area_cluster", scope = CityAreaClusterBO.class)
    public JAXBElement<String> createCityAreaClusterBOPCityAreaCluster(String value) {
        return new JAXBElement<String>(_CityAreaClusterBOPCityAreaCluster_QNAME, String.class, CityAreaClusterBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_area_name", scope = CityAreaClusterBO.class)
    public JAXBElement<String> createCityAreaClusterBOPAreaName(String value) {
        return new JAXBElement<String>(_CityAreaClusterBOPAreaName_QNAME, String.class, CityAreaClusterBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_msisdn", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPMsisdn(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPMsisdn_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_service_code", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPServiceCode(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPServiceCode_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_address", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPAddress(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPAddress_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_sim_no", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPSimNo(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPSimNo_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_package_name", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPPackageName(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPPackageName_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_connection_type", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPConnectionType(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPConnectionType_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "p_nic", scope = AutoFillControlBO.class)
    public JAXBElement<String> createAutoFillControlBOPNic(String value) {
        return new JAXBElement<String>(_AutoFillControlBOPNic_QNAME, String.class, AutoFillControlBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "workcodeu32title", scope = MetaDataBO.class)
    public JAXBElement<String> createMetaDataBOWorkcodeu32Title(String value) {
        return new JAXBElement<String>(_MetaDataBOWorkcodeu32Title_QNAME, String.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ismandatory", scope = MetaDataBO.class)
    public JAXBElement<BigDecimal> createMetaDataBOIsmandatory(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MetaDataBOIsmandatory_QNAME, BigDecimal.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fieldu32name", scope = MetaDataBO.class)
    public JAXBElement<String> createMetaDataBOFieldu32Name(String value) {
        return new JAXBElement<String>(_MetaDataBOFieldu32Name_QNAME, String.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "controlu32name", scope = MetaDataBO.class)
    public JAXBElement<String> createMetaDataBOControlu32Name(String value) {
        return new JAXBElement<String>(_MetaDataBOControlu32Name_QNAME, String.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sequencenumber", scope = MetaDataBO.class)
    public JAXBElement<BigDecimal> createMetaDataBOSequencenumber(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MetaDataBOSequencenumber_QNAME, BigDecimal.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "script", scope = MetaDataBO.class)
    public JAXBElement<String> createMetaDataBOScript(String value) {
        return new JAXBElement<String>(_MetaDataBOScript_QNAME, String.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fieldu32tooltip", scope = MetaDataBO.class)
    public JAXBElement<String> createMetaDataBOFieldu32Tooltip(String value) {
        return new JAXBElement<String>(_MetaDataBOFieldu32Tooltip_QNAME, String.class, MetaDataBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wcname", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOWcname(String value) {
        return new JAXBElement<String>(_TaskBOWcname_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complaintlogenddate", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOComplaintlogenddate(String value) {
        return new JAXBElement<String>(_TaskBOComplaintlogenddate_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wcqualitystatus", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOWcqualitystatus(String value) {
        return new JAXBElement<String>(_TaskBOWcqualitystatus_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agentremarks", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOAgentremarks(String value) {
        return new JAXBElement<String>(_TaskBOAgentremarks_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loggedby", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOLoggedby(String value) {
        return new JAXBElement<String>(_TaskBOLoggedby_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "userinformation", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOUserinformation(String value) {
        return new JAXBElement<String>(_TaskBOUserinformation_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "actionremarks", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOActionremarks(String value) {
        return new JAXBElement<String>(_TaskBOActionremarks_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "iswithintat", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOIswithintat(String value) {
        return new JAXBElement<String>(_TaskBOIswithintat_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "prevactionremarks", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOPrevactionremarks(String value) {
        return new JAXBElement<String>(_TaskBOPrevactionremarks_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loggedstatus", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOLoggedstatus(String value) {
        return new JAXBElement<String>(_TaskBOLoggedstatus_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "workcodeid", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOWorkcodeid(String value) {
        return new JAXBElement<String>(_TaskBOWorkcodeid_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loglocation", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOLoglocation(String value) {
        return new JAXBElement<String>(_TaskBOLoglocation_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complaintid", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOComplaintid(String value) {
        return new JAXBElement<String>(_TaskBOComplaintid_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "clinumber", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOClinumber(String value) {
        return new JAXBElement<String>(_TaskBOClinumber_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complaintlogstartdate", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOComplaintlogstartdate(String value) {
        return new JAXBElement<String>(_TaskBOComplaintlogstartdate_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wcstatus", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOWcstatus(String value) {
        return new JAXBElement<String>(_TaskBOWcstatus_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "logagentid", scope = TaskBO.class)
    public JAXBElement<String> createTaskBOLogagentid(String value) {
        return new JAXBElement<String>(_TaskBOLogagentid_QNAME, String.class, TaskBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "workcodename", scope = InformationalWorkCodeBO.class)
    public JAXBElement<String> createInformationalWorkCodeBOWorkcodename(String value) {
        return new JAXBElement<String>(_InformationalWorkCodeBOWorkcodename_QNAME, String.class, InformationalWorkCodeBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loggedlocation", scope = InformationalWorkCodeBO.class)
    public JAXBElement<String> createInformationalWorkCodeBOLoggedlocation(String value) {
        return new JAXBElement<String>(_InformationalWorkCodeBOLoggedlocation_QNAME, String.class, InformationalWorkCodeBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complaintlogstartdate", scope = InformationalWorkCodeBO.class)
    public JAXBElement<String> createInformationalWorkCodeBOComplaintlogstartdate(String value) {
        return new JAXBElement<String>(_TaskBOComplaintlogstartdate_QNAME, String.class, InformationalWorkCodeBO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loggedby", scope = InformationalWorkCodeBO.class)
    public JAXBElement<String> createInformationalWorkCodeBOLoggedby(String value) {
        return new JAXBElement<String>(_TaskBOLoggedby_QNAME, String.class, InformationalWorkCodeBO.class, value);
    }

}
