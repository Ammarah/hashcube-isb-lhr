
package cares.WS4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Update_Bill_AddressResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Result_Desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateBillAddressResult",
    "resultDesc"
})
@XmlRootElement(name = "Update_Bill_AddressResponse")
public class UpdateBillAddressResponse {

    @XmlElement(name = "Update_Bill_AddressResult")
    protected int updateBillAddressResult;
    @XmlElement(name = "Result_Desc")
    protected String resultDesc;

    /**
     * Gets the value of the updateBillAddressResult property.
     * 
     */
    public int getUpdateBillAddressResult() {
        return updateBillAddressResult;
    }

    /**
     * Sets the value of the updateBillAddressResult property.
     * 
     */
    public void setUpdateBillAddressResult(int value) {
        this.updateBillAddressResult = value;
    }

    /**
     * Gets the value of the resultDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

}
