/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function ActivationClass() {

	this.classname = this.constructor.name;

	this.msisdn = null;

	this.cnic = null;

	this.preSaleObj = new PreSaleClass();

	this.pmdVerified = false;

	this.onvObj = new ONVClass();

	this.isTermsConditionsVerified = false;

	this.isActivated = false;

	this.destroy = function() {

		this.classname = this.constructor.name;

		this.msisdn = null;

		this.cnic = null;

		this.preSaleObj = new PreSaleClass();

		this.pmdVerified = false;

		this.onvObj = new ONVClass();

		this.isTermsConditionsVerified = false;

		this.isActivated = false;
	};

	this.serializeToJSON = function() {
		return JSON.stringify(this);
	};
}

function PreSaleClass() {

	this.classname = this.constructor.name;

	this.msisdn = null;

	this.retailerMsisdn = null;

	this.purchaserCnic = null;

	this.uniqueID = null;

	this.saleDate = null;

	this.isVerified = false;

	this.destroy = function() {

		this.classname = this.constructor.name;

		this.msisdn = null;

		this.retailerMsisdn = null;

		this.purchaserCnic = null;

		this.uniqueID = null;

		this.saleDate = null;

		this.isVerified = false;

	};

	this.serializeToJSON = function() {
		return JSON.stringify(this);
	};
}

function ONVClass() {

	this.classname = this.constructor.name;

	this.msisdn = null;

	this.name = null;

	this.fatherName = null;

	this.motherName = null;

	this.birthPlace = null;

	this.currentAddress = null;

	this.permanentAddress = null;

	this.destroy = function() {

		this.classname = this.constructor.name;

		this.msisdn = null;

		this.name = null;

		this.fatherName = null;

		this.motherName = null;

		this.birthPlace = null;

		this.currentAddress = null;

		this.permanentAddress = null;
	};

	this.serializeToJSON = function() {
		return JSON.stringify(this);
	};
}

function LocationClass() {

	this.classname = this.constructor.name;

	this.latitude = 0.0;

	this.longitude = 0.0;

	this.latitudeCenter = 0.0;

	this.longitudeCenter = 0.0;

	this.zoom = 14;

	this.destroy = function() {

		this.classname = this.constructor.name;

		this.latitude = 0.0;

		this.longitude = 0.0;

		this.latitudeCenter = 0.0;

		this.longitudeCenter = 0.0;

		this.zoom = 14;

	};

	this.serializeToJSON = function() {
		return JSON.stringify(this);
	};
}

function StatsScreenClass() {

	this.classname = this.constructor.name;

	this.statsLongScaleWidth = 891;

	this.statsShortScaleWidth = 191;

	this.statsLongSLABuffer = 88.77665544332211;

	this.statsShortSLABuffer = 79.05759162303665;

	this.loggedInTimeCurrent_seconds = 5.25 * 3600;

	this.loggedInTimeMax_seconds = 7.8 * 3600;

	//this.loggedInTime_sla_seconds = 8 * 3600;
	this.loggedInTime_sla_seconds = 480 * 60;

	this.idleTimeCurrent_seconds = 0.26 * 3600;

	this.idleTimeMax_seconds = 0.5 * 3600;

	//this.idleTime_sla_seconds = 2 * 3600;
	this.idleTime_sla_seconds = 80 * 60;

	this.breakTimeCurrent_seconds = 0.26 * 3600;

	this.breakTimeMax_seconds = 0.56588 * 3600;

	this.breakTime_sla_seconds = 2 * 3600;

	this.notReadyTimeCurrent_seconds = 0.26 * 3600;

	this.notReadyTimeMax_seconds = 0.5 * 3600;

	//this.notReadyTime_sla_seconds = 2 * 3600;
	this.notReadyTime_sla_seconds = 60 * 60;

	this.productiveDurationCurrent_seconds = 8 * 3600;

	this.productiveDurationMax_seconds = 5 * 3600;

	//this.productiveDuration_sla_seconds = 8 * 3600;
	this.productiveDuration_sla_seconds = 420 * 60;

	this.acwTimeCurrent_seconds = 0.26 * 3600;

	this.acwTimeMax_seconds = 0.5 * 3600;

	//this.acwTime_sla_seconds = 1 * 3600;
	this.acwTime_sla_seconds = 230 * 5;

	this.holdTimeCurrent_seconds = 0.26 * 3600;

	this.holdTimeMax_seconds = 0.5 * 3600;

	//this.holdTime_sla_seconds = 1 * 3600;
	this.holdTime_sla_seconds = 230 * 7;

	this.avgTalkTimeCurrent_seconds = 0.26 * 3600;

	this.avgTalkTimeMax_seconds = 0.5 * 3600;

	//this.avgTalkTime_sla_seconds = 1 * 3600;
	this.avgTalkTime_sla_seconds = 230 * 120;

	this.callsOfferedCurrent = 99;

	this.callsOfferedMax = 180;

	this.callsOffered_sla = 400;

	this.callsAnsweredCurrent = 94;

	this.callsAnsweredMax = 167;

	//this.callsAnswered_sla = 300;
	this.callsAnswered_sla = 230;

	this.callsREQCurrent = 5;

	this.callsREQMax = 12;

	//this.callsREQ_sla = 20;
	this.callsREQ_sla = 10;

	this.shortCallsCurrent = 2;

	this.shortCallsMax = 7;

	//this.shortCalls_sla = 8;
	this.shortCalls_sla = 10;

	this.getValuesInPixel = function(time, sla, scaleSize) {
		var slaBuffer = null;
		if (scaleSize == this.statsLongScaleWidth) {
			slaBuffer = this.statsLongSLABuffer;
		}
		else {
			slaBuffer = this.statsShortSLABuffer;
		}
		var percent = parseFloat((time / ((sla * 100) / slaBuffer)) * 100);
		if (percent >= 100) {
			percent = 100;
		}
		else if (percent <= 0) {
			percent = 0;
		}
		var pixel = Math.round((percent / 100) * scaleSize);
		return pixel;
	};

	this.convertSecondsToHHMM = function(sec) {
		var hours = parseInt(sec / 3600);
		var remainder = parseInt(sec % 3600);
		var minutes = parseInt(remainder / 60);
		return ((hours < 10 ? "" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes);
	};

	this.getSLABufferLimit = function(sla, slaBuffer) {
		return Math.round((sla * 100) / slaBuffer);
	};

	this.destroy = function() {
		this.classname = this.constructor.name;

		this.loggedInTimeCurrent_seconds = 0;

		this.loggedInTimeMax_seconds = 0;

		this.idleTimeCurrent_seconds = 0;

		this.idleTimeMax_seconds = 0;

		this.breakTimeCurrent_seconds = 0;

		this.breakTimeMax_seconds = 0;

		this.notReadyTimeCurrent_seconds = 0;

		this.notReadyTimeMax_seconds = 0;

		this.productiveDurationCurrent_seconds = 0;

		this.productiveDurationMax_seconds = 0;

		this.acwTimeCurrent_seconds = 0;

		this.acwTimeMax_seconds = 0;

		this.holdTimeCurrent_seconds = 0;

		this.holdTimeMax_seconds = 0;

		this.avgTalkTimeCurrent_seconds = 0;

		this.avgTalkTimeMax_seconds = 0;

		this.callsOfferedCurrent = 0;

		this.callsOfferedMax = 0;

		this.callsAnsweredCurrent = 0;

		this.callsAnsweredMax = 0;

		this.callsREQCurrent = 0;

		this.callsREQMax = 0;

		this.shortCallsCurrent = 0;

		this.shortCallsMax = 0;

	};

	this.serializeToJSON = function() {
		return JSON.stringify(this);
	};
}

var activationObj = new ActivationClass();
var locationObj = new LocationClass();
var statsScreenObj = new StatsScreenClass();