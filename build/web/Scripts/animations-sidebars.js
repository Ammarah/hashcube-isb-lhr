/*
 * Side Bar Script - HASHCUBE JavaScript Library v1.0
 * 
 * O3Interfaces (Pvt) Ltd. 2012
 * Client: Apollo Telecom
 * Date: July 2012
 */

//Global Variables
var open_panel;
var timerId;
var telephony_bar_show_status = false;
var stats_bar_show_status = false;
var LocalTime_timerId;
var hide_break_timerId;
var HideBreakTimerPassword = 10; //Timer in Seconds
var last_card_clicked = 1;
var last_card_clicked_position = 1;
var isCardExpanded = false;
var unicaConfirmationEventTarget = null;
var unicaConfirmationEventName = null;
var unicaConfirmationUnicaCode = null;

var acw_check = true;
var brk_check = true;
var lightbox_black_hover = true;

//require(["jquery-1.7.2.min","jquery.watermark.min","jquery.easing.1.3","jquery.countdown","jquery.fancybox.pack","settings","custom_form_elements"], function() {
//require(["settings"], function() {
$(function() {
	$("#lightbox-bg-blackout").mousedown(function() {
		/*
		 $(".theme-selector-wrapper").animate({
		 opacity: 0
		 }, 10, 'linear', function(){
		 $(".theme-selector-wrapper").css('display', 'none');
		 });
		 */
		$(".theme-selector-wrapper").css('opacity', '0');
		$(".theme-selector-wrapper").css('display', 'none');

	});
	$(".checkbox-generic").click(function() {
		$(this).toggleClass("checked-checkbox");
	});
	$(".radio-btn-generic").click(function() {
		$(".radio-btn-generic").attr('class', 'radio-btn-generic');
		$(this).toggleClass("checked-radio-btn");
	});
	$(".telephony-bar-wrapper").mouseenter(function() {
		//console.log("Telephony Enter");
		//if(acw_check && brk_check)
		if (brk_check)
		{
			telephony_bar_show_status = true;
		}
		$("#lightbox-bg-blackout").css('display', 'block');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$(".telephony-bar-wrapper").animate({
			right: 0
		}, {
			duration: 10,
			easing: 'easeInOutCirc',
			step: function() {
				if (telephony_bar_show_status == false) {
					telephony_bar_hide();
				}
			},
			complete: function() {

			}
		});
	});

	$(".stats-bar-wrapper").mouseenter(function() {
		stats_bar_show_status = true;
		$("#lightbox-bg-blackout").css('display', 'block');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$(".stats-bar-wrapper").animate({
			left: 0
		}, {
			duration: 10,
			easing: 'easeInOutCirc',
			step: function() {
				if (stats_bar_show_status == false) {
					stats_bar_hide();
				}
			},
			complete: function() {

			}
		});
	});

	$("#lightbox-bg-blackout").mouseenter(function() {

		if (telephony_bar_show_status == true && lightbox_black_hover) {
			telephony_bar_show_status = false;
			if ($(".transfer-pop-out-wrapper").css('display') == 'none') {
				telephony_bar_hide();
			}
			else {
				close_selected_panel(open_panel, 'all');
			}
			if ($(".break-pop-out-wrapper").css('display') == 'none') {
				telephony_bar_hide();
			}
			else {
				close_selected_panel(open_panel, 'all');
			}
			if ($(".dialer-pop-out-wrapper").css('display') == 'none') {
				telephony_bar_hide();
			}
			else {
				close_selected_panel(open_panel, 'all');
			}
			open_panel = null;
		}

		if (stats_bar_show_status == true) {
			stats_bar_show_status = false;
			stats_bar_hide();
		}
	});

	$("#btnParentTransfer").mousedown(function() {
		if ($(".transfer-pop-out-wrapper").css('display') != 'block') {
			if (open_panel != null) {
				close_selected_panel(open_panel, 'selected');
			}
			open_panel = 'transfer';
			$(this).parent().attr('class', 'selected-telebar-btn-wrapper');
			$(".transfer-pop-out-wrapper").css('display', 'block');
			/*
			 $(".transfer-pop-out").animate({
			 opacity: 1
			 }, 10, 'easeOutCirc');
			 */
			$(".transfer-pop-out").css('opacity', '1');
		}
	});

	$("#break-btn").mousedown(function() {
		if ($(".break-pop-out-wrapper").css('display') != 'block') {
			if (open_panel != null) {
				close_selected_panel(open_panel, 'selected');
			}
			open_panel = 'break';
			$(this).parent().attr('class', 'selected-telebar-btn-wrapper');
			$(".break-pop-out-wrapper").css('display', 'block');
			/*
			 $(".break-pop-out").animate({
			 opacity: 1
			 }, 10, 'easeOutCirc');
			 */
			$(".break-pop-out").css('opacity', '1');
		}
	});

	$("#dial-btn").mousedown(function() {
		if ($(".dialer-pop-out-wrapper").css('display') != 'block') {
			if (open_panel != null) {
				close_selected_panel(open_panel, 'selected');
			}
			open_panel = 'dial';
			$(this).parent().attr('class', 'selected-telebar-btn-wrapper');
			$(".dialer-pop-out-wrapper").css('display', 'block');
			/*
			 $(".dialer-pop-out").animate({
			 opacity: 1
			 }, 10, 'easeOutCirc');
			 */
			$(".dialer-pop-out").css('opacity', '1');
		}
	});

	/*$("#mute-btn").mousedown(function() {
	 mute_btn_during_call();
	 });*/

	$("#cms-expand-btn").mousedown(function() {

		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		$(".complaint-management-lightbox").css('display', 'block');
		$(".complaint-management-lightbox").css('opacity', '1');

//		$("button").css('z-index', '0');
//		$("#lightbox-bg-blackout").css('z-index', '1000');
//		$("#lightbox-bg-blackout").animate({
//			opacity: 0.8
//		}, 500, 'easeOutCirc');
//		$(".complaint-management-lightbox").css('display', 'block');
//		$(".complaint-management-lightbox").animate({
//			opacity: 1
//		}, 500, 'easeOutCirc');
	});

	$("#close-cms-lightbox").mousedown(function() {

		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		$(".complaint-management-lightbox").css('opacity', '0');
		$(".complaint-management-lightbox").css('display', 'none');

//		$("button").css('z-index', '1500');
//		$("#lightbox-bg-blackout").css('z-index', '0');
//		$("#lightbox-bg-blackout").animate({
//			opacity: 0
//		}, 500, 'easeOutCirc');
//		$(".complaint-management-lightbox").animate({
//			opacity: 0
//		}, 500, 'easeOutCirc', function() {
//			$(".complaint-management-lightbox").css('display', 'none');
//		});

	});

	$(".complaint-management-lightbox").mousemove(function() {
		if (telephony_bar_show_status === true) {
			telephony_bar_show_status = false;
			if ($(".transfer-pop-out-wrapper").css('display') === 'none') {
				telephony_bar_hide();
			} else {
				close_selected_panel(open_panel, 'all');
			}
			if ($(".break-pop-out-wrapper").css('display') === 'none') {
				telephony_bar_hide();
			} else {
				close_selected_panel(open_panel, 'all');
			}
			if ($(".dialer-pop-out-wrapper").css('display') === 'none') {
				telephony_bar_hide();
			} else {
				close_selected_panel(open_panel, 'all');
			}
			open_panel = null;

		}
		if (stats_bar_show_status === true) {
			stats_bar_show_status = false;
			stats_bar_hide();
		}
	});

	$("#close-acw-lightbox").mousedown(function() {

		//telephony_bar_show_status = true;
		acw_check = true;

		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".acw-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".acw-lightbox").css('display', 'none');
		 });
		 */
		$(".acw-lightbox").css('opacity', '0');
		$(".acw-lightbox").css('display', 'none');

	});

	$("#btnIVR_Transfer").mousedown(function() {
		$("#lightbox-bg-blackout").mouseenter();
		setTimeout(function() {
			$("button").css('z-index', '0');
			$("#lightbox-bg-blackout").css('display', 'block');
			$("#lightbox-bg-blackout").css('z-index', '1500');
			$("#lightbox-bg-blackout").css('opacity', '0.8');
			/*
			 $("#lightbox-bg-blackout").animate({
			 opacity: 0.8
			 }, 10, 'easeOutCirc');
			 */
			$(".transfer-ivr-lightbox").css('display', 'block');
			/*
			 $(".transfer-ivr-lightbox").animate({
			 opacity: 1
			 }, 10, 'easeOutCirc');
			 */
			$(".transfer-ivr-lightbox").css('opacity', '1');
		}, 100);
	});

	$("#btnConsent_transfer").mousedown(function() {
		$("#lightbox-bg-blackout").mouseenter();
		setTimeout(function() {
			$("button").css('z-index', '0');
			$("#lightbox-bg-blackout").css('display', 'block');
			$("#lightbox-bg-blackout").css('z-index', '1500');
			$("#lightbox-bg-blackout").css('opacity', '0.8');
			/*
			 $("#lightbox-bg-blackout").animate({
			 opacity: 0.8
			 }, 10, 'easeOutCirc');
			 */
			$(".consent-lightbox").css('display', 'block');
			/*
			 $(".consent-lightbox").animate({
			 opacity: 1
			 }, 10, 'easeOutCirc');
			 */
			$(".consent-lightbox").css('opacity', '1');
		}, 100);
	});

	$("#close-ivr-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".transfer-ivr-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".transfer-ivr-lightbox").css('display', 'none');
		 });
		 */
		$(".transfer-ivr-lightbox").css('opacity', '0');
		$(".transfer-ivr-lightbox").css('display', 'none');
	});

	$("#close-consent-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".consent-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".consent-lightbox").css('display', 'none');
		 });
		 */
		$(".consent-lightbox").css('opacity', '0');
		$(".consent-lightbox").css('display', 'none');

	});

	$("#customer-demo-expand-btn").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 10, 'easeOutCirc');
		 */
		$(".customer-demo-lightbox").css('display', 'block');
		/*
		 $(".customer-demo-lightbox").animate({
		 opacity: 1
		 }, 10, 'easeOutCirc');
		 */
		$(".customer-demo-lightbox").css('opacity', '1');
	});

	$("#close-customer-demo-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".customer-demo-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".customer-demo-lightbox").css('display', 'none');
		 });
		 */
		$(".customer-demo-lightbox").css('opacity', '0');
		$(".customer-demo-lightbox").css('display', 'none');

	});

	$("#profit-sanc-expand-btn").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 10, 'easeOutCirc');
		 */
		$(".profit-sanc-lightbox").css('display', 'block');
		/*
		 $(".profit-sanc-lightbox").animate({
		 opacity: 1
		 }, 10, 'easeOutCirc');
		 */
		$(".profit-sanc-lightbox").css('opacity', '1');
	});

	$("#close-profit-sanc-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".profit-sanc-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".profit-sanc-lightbox").css('display', 'none');
		 });
		 */
		$(".profit-sanc-lightbox").css('opacity', '0');
		$(".profit-sanc-lightbox").css('display', 'none');

	});


	$("#offers-expand-btn").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*$("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 500, 'easeOutCirc');
		 */
		$(".offers-lightbox").css('display', 'block');
		/*
		 $(".offers-lightbox").animate({
		 opacity: 1
		 }, 500, 'easeOutCirc');
		 */
		$(".offers-lightbox").css('opacity', '1');
	});

	$("#close-offers-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*$("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 500, 'easeOutCirc');
		 
		 $(".offers-lightbox").animate({
		 opacity: 0
		 }, 500, 'easeOutCirc', function() {
		 $(".offers-lightbox").css('display', 'none');
		 });
		 */
		$(".offers-lightbox").css('opacity', '0');
		$(".offers-lightbox").css('display', 'none');
	});

	$("#package-details-expand-btn").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 10, 'easeOutCirc');
		 */
		$(".package-details-lightbox").css('display', 'block');
		/*
		 $(".package-details-lightbox").animate({
		 opacity: 1
		 }, 10, 'easeOutCirc');
		 */
		$(".package-details-lightbox").css('opacity', '1');
	});

	$("#close-package-details-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".package-details-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".package-details-lightbox").css('display', 'none');
		 });
		 */
		$(".package-details-lightbox").css('opacity', '0');
		$(".package-details-lightbox").css('display', 'none');

	});

	$("#latest-activities-expand-btn").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 10, 'easeOutCirc');
		 */
		$(".latest-activities-lightbox").css('display', 'block');
		/*
		 $(".latest-activities-lightbox").animate({
		 opacity: 1
		 }, 10, 'easeOutCirc');
		 */
		$(".latest-activities-lightbox").css('opacity', '1');
	});

	$("#close-latest-activities-lightbox").mousedown(function() {
		$("button").css('z-index', '1500');
		$('button.expand-button,.pagination-buttons button').css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'none');
		$("#lightbox-bg-blackout").css('z-index', '0');
		$("#lightbox-bg-blackout").css('opacity', '0');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc');
		 */
		/*
		 $(".latest-activities-lightbox").animate({
		 opacity: 0
		 }, 10, 'easeOutCirc', function() {
		 $(".latest-activities-lightbox").css('display', 'none');
		 });
		 */
		$(".latest-activities-lightbox").css('opacity', '0');
		$(".latest-activities-lightbox").css('display', 'none');

	});

	$("#anchrBtn_brkDiv").mousedown(function() {
		$("button").css('z-index', '0');
		$("#lightbox-bg-blackout").css('display', 'block');
		$("#lightbox-bg-blackout").css('z-index', '1500');
		$("#lightbox-bg-blackout").css('opacity', '0.8');
		/*
		 $("#lightbox-bg-blackout").animate({
		 opacity: 0.8
		 }, 10, 'easeOutCirc');
		 */
		$(".break-timer-lightbox").css('display', 'block');
		/*
		 $(".break-timer-lightbox").animate({
		 opacity: 1
		 }, 10, 'easeOutCirc');
		 */
		$(".break-timer-lightbox").css('opacity', '1');
	});

	$(".break-timer-lightbox").mousedown(function() {
		if ($(".break-timer-password-field-wrapper").css('display') != 'block') {
			$(".break-timer-counter-wrapper").animate({
				marginTop: "53px"
			}, 10, 'easeInOutCirc');
			$("#spanBreakTimer").animate({
				fontSize: "200%"
			}, 10, 'easeInOutCirc');
			$(".break-timer-password-field-wrapper").css('display', 'block');
			$(".break-timer-password-field-wrapper").animate({
				opacity: 1
			}, 10, 'easeInOutCirc');
			hide_break_timerId = setInterval(hide_break_timer_password, HideBreakTimerPassword * 1000);
		}
	});

	$("#agent-info-btn").hover(
			function() {
				$(".agent-popup-wrapper").css('display', 'block');
				/*
				 $(".agent-popup-wrapper").animate({
				 opacity: 1
				 }, 10, 'linear');
				 */
				$(".agent-popup-wrapper").css('opacity', '1');
			},
			function() {
				/*
				 $(".agent-popup-wrapper").animate({
				 opacity: 0
				 }, 10, 'linear', function() {
				 $(".agent-popup-wrapper").css('display', 'none');
				 });
				 */
				$(".agent-popup-wrapper").css('opacity', '0');
				$(".agent-popup-wrapper").css('display', 'none');
			});

	$("#agent-sop-btn").mousedown(function() {
		if ($(".agent-popup-sop-wrapper").css('display') == 'none')
		{
			$(".agent-popup-sop-wrapper").css('display', 'block');
			/*
			 $(".agent-popup-wrapper").animate({
			 opacity: 1
			 }, 10, 'linear');
			 */
			$(".agent-popup-sop-wrapper").css('opacity', '1');
		}
		else {
			//function() {
			/*
			 $(".agent-popup-wrapper").animate({
			 opacity: 0
			 }, 10, 'linear', function() {
			 $(".agent-popup-wrapper").css('display', 'none');
			 });
			 */
			$(".agent-popup-sop-wrapper").css('opacity', '0');
			$(".agent-popup-sop-wrapper").css('display', 'none');
		}
	});

	$("#theme-selector-btn").mousedown(function() {
		if ($(".theme-selector-wrapper").css('display') == 'none') {
			$(".theme-selector-wrapper").css('display', 'block');
			/*
			 $(".theme-selector-wrapper").animate({
			 opacity: 1
			 }, 10, 'linear');
			 */
			$(".theme-selector-wrapper").css('opacity', '1');
		}
		else {
			/*
			 $(".theme-selector-wrapper").animate({
			 opacity: 0
			 }, 10, 'linear', function(){
			 $(".theme-selector-wrapper").css('display', 'none');
			 });
			 */
			$(".theme-selector-wrapper").css('opacity', '0');
			$(".theme-selector-wrapper").css('display', 'none');

		}

	});

	$("#theme-selector-dropdown").change(function() {
		switch ($(this).val()) {
			case "Carbon Grey":
				$("body").css('background', 'url(images/bg.png) repeat');
				$(".main-screen-panel-active").css('background', 'url(images/main-panel-active.png) no-repeat');
				$(".customStyleSelectBox").css('background', 'url(images/package-detail-dropdown-sprite.png) 0 0');
				$(".voice_icn").css('background', 'url(images/voice-drop-down-icon.png) no-repeat');
				$(".data_icn").css('background', 'url(images/data-drop-down-icon.png) no-repeat');
				$(".message_icn").css('background', 'url(images/message-drop-down-icon.png) no-repeat');
				$(".others_icn").css('background', 'url(images/others-drop-down-icon.png) no-repeat');
				$(".profit-base-track").css('background', 'url(images/base-track-bg.png) no-repeat -583px 0');
				$(".customer-name-sep").css('background', 'url(images/customer-name-sep.png) no-repeat');
				$(".drop-shadow").css('text-shadow', '0px 1px 2px black');
				$(".regular-body-lucida-white").attr('class', 'regular-body-lucida-light-grey');
				$(".regular-lucida-bold-yellow-11").attr('class', 'regular-lucida-bold-11');
				$(".inactive-star").css('background', 'url(images/star-sprite.png) no-repeat -20px 0');
				$(".last-acitivty-body-wrapper .sep").css('background', 'url(images/engraved-line-bg.png) repeat-x');
				$(".footer-links").css('color', '#626262');
				$(".terms-link").css('border-right', '1px solid #626262');
				$("#designed-by").attr('class', 'regular-body');
				$("#apollo-telecom").attr('class', 'regular-body-light-grey');

				$(".acw-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".radio-checkbox-panel").css('background', 'url(images/lightbox-radio-checkbox-panel.png) no-repeat');
				$(".search-wrapper").css('background', 'url(images/lightbox-search-bg.png) no-repeat');
				$(".submit-btn").css('background', 'transparent url(images/generic-grey-btn-bg.png) repeat-x');
				$(".acw-lightbox").css('background-color', '#292929');
				$(".select-box-generic").css('background-color', '#333');
				$(".select-box-generic").css('border', '1px solid #3C3C3C');
				$(".selected-options-panel-wrapper").css('background-color', '#333');
				$(".selected-options-panel-wrapper").css('border', '1px solid #3C3C3C');
				$(".selected-option-wrapper").css('background-color', '#333');
				$(".selected-option-wrapper").css('border', '1px solid #3C3C3C');
				$(".package-details-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".package-details-lightbox").css('background-color', '#292929');
				$(".profit-sanc-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".profit-sanc-lightbox").css('background-color', '#292929');
				$(".panel-bg .left-col").css('border-right', '1px solid #3C3C3C');
				$(".latest-activities-lightbox").css('background-color', '#292929');
				$(".latest-activities-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".customer-demo-lightbox").css('background-color', '#292929');
				$(".customer-demo-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".transfer-ivr-lightbox").css('background-color', '#292929');
				$(".transfer-ivr-lightbox .header").css('background', 'url(images/bg.png) repeat');
				$(".panel-bg").css('background-color', '#333');
				$(".panel-bg").css('border', '1px solid #3C3C3C');
				$(".profit-sanc-lightbox");
				$(".sep").css('background-color', '#3C3C3C');
				/*
				 $(".theme-selector-wrapper").animate({
				 opacity: 0
				 }, 10, 'linear', function(){
				 $(".theme-selector-wrapper").css('display', 'none');
				 });
				 */
				$(".theme-selector-wrapper").css('opacity', '0');
				$(".theme-selector-wrapper").css('display', 'none');

				break;

			case "Eminent Blue":
				$("body").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".main-screen-panel-active").css('background', 'url(images/main-panel-active.png) no-repeat');
				$(".customStyleSelectBox").css('background', 'url(images/package-detail-dropdown-sprite-navyblue.png) 0 0');
				$(".voice_icn").css('background', 'url(images/voice-drop-down-icon-navyblue.png) no-repeat');
				$(".data_icn").css('background', 'url(images/data-drop-down-icon-navyblue.png) no-repeat');
				$(".message_icn").css('background', 'url(images/message-drop-down-icon-navyblue.png) no-repeat');
				$(".others_icn").css('background', 'url(images/others-drop-down-icon-navyblue.png) no-repeat');
				$(".profit-base-track").css('background', 'url(images/base-track-bg.png) no-repeat -583px 0');
				$(".customer-name-sep").css('background', 'url(images/customer-name-sep.png) no-repeat');
				$(".drop-shadow").css('text-shadow', '0px 1px 2px black');
				$(".regular-body-lucida-white").attr('class', 'regular-body-lucida-light-grey');
				$(".regular-lucida-bold-yellow-11").attr('class', 'regular-lucida-bold-11');
				$(".inactive-star").css('background', 'url(images/star-sprite.png) no-repeat -20px 0');
				$(".last-acitivty-body-wrapper .sep").css('background', 'url(images/engraved-line-bg.png) repeat-x');
				$(".footer-links").css('color', '#487dc6');
				$(".terms-link").css('border-right', '1px solid #487dc6');
				$("#designed-by").attr('class', 'regular-body-blue');
				$("#apollo-telecom").attr('class', 'regular-body-light-blue');

				$(".acw-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".radio-checkbox-panel").css('background', 'url(images/lightbox-radio-checkbox-panel-navyblue.png) no-repeat');
				$(".search-wrapper").css('background', 'url(images/lightbox-search-bg-navyblue.png) no-repeat');
				$(".submit-btn").css('background', 'transparent url(images/generic-navyblue-btn-bg.png) repeat-x');
				$(".acw-lightbox").css('background-color', '#123e6d');
				$(".select-box-generic").css('background-color', '#29517c');
				$(".select-box-generic").css('border', '1px solid #356291');
				$(".selected-options-panel-wrapper").css('background-color', '#29517c');
				$(".selected-options-panel-wrapper").css('border', '1px solid #356291');
				$(".selected-option-wrapper").css('background-color', '#29517c');
				$(".selected-option-wrapper").css('border', '1px solid #356291');
				$(".package-details-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".package-details-lightbox").css('background-color', '#123e6d');
				$(".profit-sanc-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".profit-sanc-lightbox").css('background-color', '#123e6d');
				$(".panel-bg .left-col").css('border-right', '1px solid #356291');
				$(".latest-activities-lightbox").css('background-color', '#123e6d');
				$(".latest-activities-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".customer-demo-lightbox").css('background-color', '#123e6d');
				$(".customer-demo-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".transfer-ivr-lightbox").css('background-color', '#123e6d');
				$(".transfer-ivr-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".panel-bg").css('background-color', '#29517c');
				$(".panel-bg").css('border', '1px solid #356291');
				$(".profit-sanc-lightbox");
				$(".sep").css('background-color', '#356291');
				/*
				 $(".theme-selector-wrapper").animate({
				 opacity: 0
				 }, 10, 'linear', function(){
				 $(".theme-selector-wrapper").css('display', 'none');
				 });
				 */
				$(".theme-selector-wrapper").css('opacity', '0');
				$(".theme-selector-wrapper").css('display', 'none');

				break;

			case "Windows Aqua":
				$("body").css('background', 'url(images/bg-aqua.png) repeat');
				$(".main-screen-panel-active").css('background', 'url(images/main-panel-active-aqua.png) no-repeat');
				$(".customStyleSelectBox").css('background', 'url(images/package-detail-dropdown-sprite-aqua.png) 0 0');
				$(".voice_icn").css('background', 'url(images/voice-drop-down-icon-navyblue.png) no-repeat');
				$(".data_icn").css('background', 'url(images/data-drop-down-icon-navyblue.png) no-repeat');
				$(".message_icn").css('background', 'url(images/message-drop-down-icon-navyblue.png) no-repeat');
				$(".others_icn").css('background', 'url(images/others-drop-down-icon-navyblue.png) no-repeat');
				$(".profit-base-track").css('background', 'url(images/base-track-bg-aqua.png) no-repeat -583px 0');
				$(".customer-name-sep").css('background', 'url(images/customer-name-sep-aqua.png) no-repeat');
				$(".drop-shadow").css('text-shadow', '0px 1px 2px rgba(0, 0, 0, 0.4)');
				$(".regular-body-lucida-light-grey").attr('class', 'regular-body-lucida-white');
				$(".regular-lucida-bold-11").attr('class', 'regular-lucida-bold-yellow-11');
				$(".inactive-star").css('background', 'url(images/star-sprite-aqua.png) no-repeat -20px 0');
				$(".last-acitivty-body-wrapper .sep").css('background', 'url(images/engraved-line-bg-aqua.png) repeat-x');
				$(".footer-links").css('color', '#FFFFFF');
				$(".terms-link").css('border-right', '1px solid #FFFFFF');
				$("#designed-by").attr('class', 'regular-body-white');
				$("#apollo-telecom").attr('class', 'regular-body-white');

				$(".acw-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".radio-checkbox-panel").css('background', 'url(images/lightbox-radio-checkbox-panel-navyblue.png) no-repeat');
				$(".search-wrapper").css('background', 'url(images/lightbox-search-bg-navyblue.png) no-repeat');
				$(".submit-btn").css('background', 'transparent url(images/generic-navyblue-btn-bg.png) repeat-x');
				$(".acw-lightbox").css('background-color', '#5985b4');
				$(".select-box-generic").css('background-color', '#6d94bd');
				$(".select-box-generic").css('border', '1px solid #557ba3');
				$(".selected-options-panel-wrapper").css('background-color', '#6d94bd');
				$(".selected-options-panel-wrapper").css('border', '1px solid #557ba3');
				$(".selected-option-wrapper").css('background-color', '#6d94bd');
				$(".selected-option-wrapper").css('border', '1px solid #557ba3');
				$(".package-details-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".package-details-lightbox").css('background-color', '#5985b4');
				$(".profit-sanc-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".profit-sanc-lightbox").css('background-color', '#5985b4');
				$(".panel-bg .left-col").css('border-right', '1px solid #557ba3');
				$(".latest-activities-lightbox").css('background-color', '#5985b4');
				$(".latest-activities-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".customer-demo-lightbox").css('background-color', '#5985b4');
				$(".customer-demo-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".transfer-ivr-lightbox").css('background-color', '#5985b4');
				$(".transfer-ivr-lightbox .header").css('background', 'url(images/bg-navyblue.png) repeat');
				$(".panel-bg").css('background-color', '#6d94bd');
				$(".panel-bg").css('border', '1px solid #557ba3');
				$(".profit-sanc-lightbox");
				$(".sep").css('background-color', '#557ba3');
				/*
				 $(".theme-selector-wrapper").animate({
				 opacity: 0
				 }, 10, 'linear', function(){
				 $(".theme-selector-wrapper").css('display', 'none');
				 });
				 */
				$(".theme-selector-wrapper").css('opacity', '0');
				$(".theme-selector-wrapper").css('display', 'none');

				break;
		}
	});

	$(".show_all_cards").live('click', function() {
		$(".show_all_cards").css('display', 'none');
		close_all_cards();
	});
	$(".offers-details-body-wrapper").live('click', function(event) {
		var classname = event.target.className;
		var card_number = null;
		if (classname == "regular-body-lucida-uppercase-white") {
			card_number = $(event.target).parent().parent().parent().attr('contextmenu');
			if (!isNaN(parseInt($(event.target).parent().parent().parent().attr('cardposition')))) {
				last_card_clicked_position = parseInt($(event.target).parent().parent().parent().attr('cardposition'));
			}
		}
		else {
			card_number = $(event.target).parent().attr('contextmenu');
			if (!isNaN(parseInt($(event.target).parent().attr('cardposition')))) {
				last_card_clicked_position = parseInt($(event.target).parent().attr('cardposition'));
			}
		}
		if (!isNaN(card_number)) {
			last_card_clicked = parseInt(card_number);
			if (!isCardExpanded) {
				card_number = parseInt(card_number) + 1;
				var card_location = null;
				var temp1 = null;
				if (classname == "regular-body-lucida-uppercase-white") {
					card_location = parseInt($(event.target).parent().parent().parent().css('top').replace("px", "")) + 104;
					console.log(card_location);
					if ($(event.target).parent().parent().parent().attr('id') !== 'card-disabled')
					{
						temp1 = parseInt($(event.target).parent().parent().parent().css('top').replace("px", "")) - (38 * (last_card_clicked_position - 1));
						$('div[contextmenu=\"' + last_card_clicked + '\"]').animate({
							top: temp1
						}, 100, 'easeInOutCirc');
						$('div[contextmenu=\"' + last_card_clicked + '\"]').find('span.spanCardDesc').css("display", "");
						isCardExpanded = true;
						for (i = card_number; i <= unicaOfferArray.length; i++) {
							$('div[contextmenu=\"' + i + '\"]').animate({
								top: card_location
							}, 100, 'easeInOutCirc');
							$(".show_all_cards").css('display', 'block');
							$(".show_all_cards").animate({
								opacity: 1
							}, 100, 'easeInOutCirc');
							card_location -= 38;
						}
						$(".show_all_cards").css('display', 'block');
						$(".show_all_cards").animate({
							opacity: 1
						}, 100, 'easeInOutCirc');
					}
				}
				else {
					card_location = parseInt($(event.target).parent().css('top').replace("px", "")) + 104;
					console.log(card_location);
					if ($(event.target).parent().attr('id') !== 'card-disabled')
					{
						temp1 = parseInt($(event.target).parent().css('top').replace("px", "")) - (38 * (last_card_clicked_position - 1));
						$('div[contextmenu=\"' + last_card_clicked + '\"]').animate({
							top: temp1
						}, 100, 'easeInOutCirc');
						$('div[contextmenu=\"' + last_card_clicked + '\"]').find('span.spanCardDesc').css("display", "");
						isCardExpanded = true;
						for (i = card_number; i <= unicaOfferArray.length; i++) {
							$('div[contextmenu=\"' + i + '\"]').animate({
								top: card_location
							}, 100, 'easeInOutCirc');
							$(".show_all_cards").css('display', 'block');
							$(".show_all_cards").animate({
								opacity: 1
							}, 100, 'easeInOutCirc');
							card_location -= 38;
						}
						$(".show_all_cards").css('display', 'block');
						$(".show_all_cards").animate({
							opacity: 1
						}, 100, 'easeInOutCirc');
					}
				}
			}
		}
	});
	$(".cards-accept-btn, .cards-pending-btn, .cards-reject-btn").live('click', function(event) {
		var classname = event.target.className;
		var eventName = "";

		if (classname == "cards-accept-btn") {
			eventName = "accept";
		}
		else if (classname == "cards-reject-btn") {
			eventName = "reject";
		}
		else {
			eventName = "interest";
		}

		var unicaCode = $(event.target).parent().parent().parent().parent().find("input.inputUnicaCode").first().val();
		unicaConfirmationEventTarget = $(event.target);
		unicaConfirmationEventName = eventName;
		unicaConfirmationUnicaCode = unicaCode;

		if (eventName == "accept") {
			if ($(event.target).parent().parent().parent().parent().attr('id') !== 'card-disabled') {
				showUnicaOfferConfirmationBox(eventName, unicaCode);
			}
		}
		else if (eventName == "reject") {
			if ($(event.target).parent().parent().parent().parent().attr('id') !== 'card-disabled') {
				showUnicaOfferConfirmationBox(eventName, unicaCode);
			}
		}
		else {
			var card_number = parseInt($(event.target).parent().parent().parent().parent().attr('contextmenu'));
			var card_location = parseInt($(event.target).parent().parent().parent().parent().css('top').replace("px", "")) - 38;
			console.log("last_card_clicked event = " + last_card_clicked);
			if ($(event.target).parent().parent().parent().parent().attr('id') !== 'card-disabled') {

				unicaPostEvent(eventName, unicaCode);

				if (isCardExpanded) {
					setTimeout(function() {
						$(".show_all_cards").click();
					}, 300);
				}

				$(event.target).parent().parent().parent().parent().animate({
					opacity: 0
				}, 100, 'easeInOutCirc', function() {
					$(event.target).parent().parent().parent().parent().attr('display', 'none');
					$(event.target).parent().parent().parent().parent().css("z-index", "0");
				});

				$('div[contextmenu=\"' + card_number + '\"]').attr("cardposition", 0);
				for (i = card_number + 1; i <= unicaOfferArray.length; i++) {
					$('div[contextmenu=\"' + i + '\"]').attr("cardposition", parseInt($('div[contextmenu=\"' + i + '\"]').attr("cardposition") - 1));
				}

				for (i = card_number; i <= unicaOfferArray.length; i++) {
					var temp1 = $('div[contextmenu=\"' + i + '\"]').attr("display");
					if (temp1 == "none") {
						console.log("temp1 = " + temp1);
						card_location -= 38;
					}
					$('div[contextmenu=\"' + i + '\"]').animate({
						top: card_location
					}, 100, 'easeInOutCirc');
				}
				if (card_number < unicaOfferArray.length) {
					var cardsToShow = clientObj.getCardsToShow();
					for (i = 1; i <= cardsToShow && card_number + i <= unicaOfferArray.length; i++) {
						var card_class = 'card-color-' + getUnicaCardColor(unicaOfferArray[card_number + parseInt(i - 1)].split("|")[9]);
						var next_card = card_number + i;
						$('div[contextmenu=\"' + next_card + '\"]').attr('id', card_class);
						$('div[contextmenu=\"' + next_card + '\"]').find('.red-card-icon').first().show();
						$('div[contextmenu=\"' + next_card + '\"]').find('.blue-card-icon').first().show();
						$('div[contextmenu=\"' + next_card + '\"]').find('.orange-card-icon').first().show();
						$('div[contextmenu=\"' + next_card + '\"]').find('.card-heading-text').first().show();
						$('div[contextmenu=\"' + next_card + '\"]').find('.card-action-btns-wrapper').first().show();
						if (i == 1) {
							$('div[contextmenu=\"' + next_card + '\"]').find('span.spanCardDesc').css("display", "");
						}
						else {
							$('div[contextmenu=\"' + next_card + '\"]').find('span.spanCardDesc').css("display", "none");
						}
					}
				}
			}
		}
	});

	$('.search-offer-input').watermark('Search', {
		useNative: false
	}, {
		className: 'search-offer-input'
	});


});


function close_all_cards() {
	isCardExpanded = false;
	var next_card = parseInt(last_card_clicked) + 1;
	try {
		var temp1 = parseInt($('div[contextmenu=\"' + last_card_clicked + '\"]').css('top').replace("px", "")) + (38 * (last_card_clicked_position - 1));
	}
	catch (exception) {

	}
	$('div[contextmenu=\"' + last_card_clicked + '\"]').animate({
		top: temp1
	}, 100, 'easeInOutCirc');
	$('div.cards-wrapper').find('span.spanCardDesc').css("display", "none");
	var card_location = parseInt($('div[contextmenu=\"' + last_card_clicked + '\"]').css('top').replace("px", "")) + (38 * (last_card_clicked_position - 1));
	console.log("before loop");
	for (i = next_card; i <= unicaOfferArray.length; i++) {
		//var card_location = parseInt($('div[contextmenu=\"' + last_card_clicked + '\"]').css('top').replace("px", ""));
		var temp2 = $('div[contextmenu=\"' + i + '\"]').attr("display");
		if (temp2 == "none") {
			console.log("temp2 = " + temp2);
			card_location -= 38;
		}
		else {
			//card_location = 0;
		}
		console.log(card_location);
		console.log("inside loop");
		$('div[contextmenu=\"' + i + '\"]').animate({
			top: card_location
		}, 100, 'easeInOutCirc');
	}
}



function close_selected_panel(panel, depth) {
	switch (panel) {
		case 'transfer':
			//$(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');
			$('#btnParentTransfer').parent().parent().find(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');
			$(".transfer-pop-out").animate({
				opacity: 0
			}, 10, 'easeOutCirc', function() {
				$(".transfer-pop-out-wrapper").css('display', 'none');
				if (depth == 'all') {
					$(".telephony-bar-wrapper").animate({
						right: -294
					}, 10, 'easeInOutCirc');
				}
			});
			break;

		case 'break':
			//$(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');
			$('#break-btn').parent().parent().find(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');

			$(".break-pop-out").animate({
				opacity: 0
			}, 10, 'easeOutCirc', function() {
				$(".break-pop-out-wrapper").css('display', 'none');
				if (depth == 'all') {
					$(".telephony-bar-wrapper").animate({
						right: -294
					}, 10, 'easeInOutCirc');
				}
			});
			break;

		case 'dial':

			//$(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');
			$('#dial-btn').parent().parent().find(".selected-telebar-btn-wrapper").attr('class', 'telebar-btn-wrapper');

			$(".dialer-pop-out").animate({
				opacity: 0
			}, 10, 'easeOutCirc', function() {
				$(".dialer-pop-out-wrapper").css('display', 'none');
				if (depth == 'all') {
					$(".telephony-bar-wrapper").animate({
						right: -294
					}, 10, 'easeInOutCirc');
				}
			});
			break;
	}
}

//Hide Stats Bar
function stats_bar_hide() {
	$("#lightbox-bg-blackout").css('display', 'none');
	$("#lightbox-bg-blackout").css('z-index', '0');
	$("button.expand-button,.pagination-buttons button").css('z-index', '500');
	$(".stats-bar-wrapper").stop();
	$(".stats-bar-wrapper").animate({
		left: -294
	}, 10, 'easeInOutCirc');
}

function telephony_bar_hide() {
	$("#lightbox-bg-blackout").css('display', 'none');
	$("#lightbox-bg-blackout").css('z-index', '0');
	$("button.expand-button,.pagination-buttons button").css('z-index', '500');
	$(".telephony-bar-wrapper").stop();
	$(".telephony-bar-wrapper").animate({
		right: -294
	}, 10, 'easeInOutCirc');
}

//Show Exception screen on main page
function exception_screen_show() {
	$("button").css('z-index', '0');
	$(".exception-generic-btn").css('z-index', '2000');

	$("#lightbox-bg-blackout").css('display', 'block');
	$("#lightbox-bg-blackout").css('z-index', '1500');
	$("#lightbox-bg-blackout").css('opacity', '0.8');
	//		$("#lightbox-bg-blackout").animate({
	//			opacity: 0.8
	//		}, 500, 'easeOutCirc');
	$(".exception-lightbox").css('display', 'block');
	$(".exception-lightbox").css('opacity', '1');
//		$(".exception-lightbox").animate({
//			opacity: 1
//		}, 500, 'easeOutCirc');
}

//Hide Exception screen on main page
function exception_screen_hide() {

	$("button").css('z-index', '1500');
	$('button.expand-button,.pagination-buttons button').css('z-index', '0');
	$("#lightbox-bg-blackout").css('display', 'none');
	$("#lightbox-bg-blackout").css('z-index', '0');
	$("#lightbox-bg-blackout").css('opacity', '0');

	$(".exception-generic-btn").css('z-index', '0');

	//		$("#lightbox-bg-blackout").animate({
	//			opacity: 0
	//		}, 500, 'easeOutCirc');

	$(".exception-lightbox").css('opacity', '0');
	$(".exception-lightbox").css('display', 'none');

//		$(".exception-lightbox").animate({
//			opacity: 0
//		}, 500, 'easeOutCirc', function() {
//			$(".exception-lightbox").css('display', 'none');
//		});
}

function showUnicaOfferConfirmationBox(eventName, unicaCode) {
	var exceptionMessage = "Are you sure?";
	$(".exception-label-wrapper span").html(exceptionMessage);

	exception_screen_show();
}

$("#exception-ok").live('click', function() {

	unicaPostEvent(unicaConfirmationEventName, unicaConfirmationUnicaCode);
	var card_number = parseInt($(unicaConfirmationEventTarget).parent().parent().parent().parent().attr('contextmenu'));
	var card_location = parseInt($(unicaConfirmationEventTarget).parent().parent().parent().parent().css('top').replace("px", "")) - 38;
	console.log("last_card_clicked event = " + last_card_clicked);
	if ($(unicaConfirmationEventTarget).parent().parent().parent().parent().attr('id') !== 'card-disabled') {
		if (isCardExpanded) {
			setTimeout(function() {
				$(".show_all_cards").click();
			}, 300);
		}
		$(unicaConfirmationEventTarget).parent().parent().parent().parent().animate({
			opacity: 0
		}, 100, 'easeInOutCirc', function() {
			$(unicaConfirmationEventTarget).parent().parent().parent().parent().attr('display', 'none');
			$(unicaConfirmationEventTarget).parent().parent().parent().parent().css("z-index", "0");
		});

		$('div[contextmenu=\"' + card_number + '\"]').attr("cardposition", 0);
		for (i = card_number + 1; i <= unicaOfferArray.length; i++) {
			$('div[contextmenu=\"' + i + '\"]').attr("cardposition", parseInt($('div[contextmenu=\"' + i + '\"]').attr("cardposition") - 1));
		}

		for (i = card_number; i <= unicaOfferArray.length; i++) {
			var temp1 = $('div[contextmenu=\"' + i + '\"]').attr("display");
			if (temp1 == "none") {
				console.log("temp1 = " + temp1);
				card_location -= 38;
			}
			$('div[contextmenu=\"' + i + '\"]').animate({
				top: card_location
			}, 100, 'easeInOutCirc');
		}
		if (card_number < unicaOfferArray.length) {
			var cardsToShow = clientObj.getCardsToShow();
			for (i = 1; i <= cardsToShow && card_number + i <= unicaOfferArray.length; i++) {
				var card_class = 'card-color-' + getUnicaCardColor(unicaOfferArray[card_number + parseInt(i - 1)].split("|")[9]);
				var next_card = card_number + i;
				$('div[contextmenu=\"' + next_card + '\"]').attr('id', card_class);
				$('div[contextmenu=\"' + next_card + '\"]').find('.red-card-icon').first().show();
				$('div[contextmenu=\"' + next_card + '\"]').find('.blue-card-icon').first().show();
				$('div[contextmenu=\"' + next_card + '\"]').find('.orange-card-icon').first().show();
				$('div[contextmenu=\"' + next_card + '\"]').find('.card-heading-text').first().show();
				$('div[contextmenu=\"' + next_card + '\"]').find('.card-action-btns-wrapper').first().show();
				if (i == 1) {
					$('div[contextmenu=\"' + next_card + '\"]').find('span.spanCardDesc').css("display", "");
				}
				else {
					$('div[contextmenu=\"' + next_card + '\"]').find('span.spanCardDesc').css("display", "none");
				}
			}
		}
	}
	exception_screen_hide();
	$(".exception-label-wrapper span").html("");
});

$("#exception-cancel").live('click', function() {
	exception_screen_hide();
	$(".exception-label-wrapper span").html("");
});

function show_notification_bar() {
//	$(".notification-bar-wrapper").css('top', 0);
	$(".notification-bar-wrapper").animate({
		top: 0
	}, 500, 'easeInOutCirc');
}

function hide_notification_bar() {
//	$(".notification-bar-wrapper").css('top', -24);
	$(".notification-bar-wrapper").animate({
		top: -24
	}, 500, 'easeInOutCirc');
}

function mute_btn_during_call() {
	$(".mute-btn").animate({
		opacity: 0.1
	}, 1000, 'linear', function() {
		$(".mute-btn").animate({
			opacity: 1
		}, 10, 'linear');
		mute_btn_during_call();
	});
}

function mute_btn_after_call() {
	$(".mute-btn").animate({
		opacity: 1
	}, 10, 'linear');
}
//});