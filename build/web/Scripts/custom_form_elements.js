(function($) {
	$.fn.extend({
		customStyle: function(options) {
			if (!$.browser.msie || ($.browser.msie && $.browser.version > 6)) {
				return this.each(function() {
					var currentSelected = $(this).find(':selected');
					if (options == 'large') {
						$(this).after('<span class="customStyleSelectBox-large"><span class="customStyleSelectBoxInner-large">' + currentSelected.text() + '</span></span>').css({
							position: 'absolute',
							opacity: 0,
							fontSize: $(this).next().css('font-size')
						});
					}
					else if (options == 'withouticon') {
						$(this).after('<span class="customStyleSelectBox-withouticon"><span class="customStyleSelectBoxInner-withouticon">' + currentSelected.text() + '</span></span>').css({
							position: 'absolute',
							opacity: 0,
							fontSize: $(this).next().css('font-size')
						});
					}
					else if (options === 'offers') {
						$(this).after('<span class="customStyleSelectBox-offers"><span class="customStyleSelectBoxInner-offers">' + currentSelected.text() + '</span></span>').css({
							position: 'absolute',
							opacity: 0,
							fontSize: $(this).next().css('font-size')
						});
					}
					else if (options === 'formelement') {
						$(this).after('<span class="customStyleSelectBox-form-element"><span class="customStyleSelectBoxInner-form-element">' + currentSelected.text() + '</span></span>').css({
							position: 'absolute',
							opacity: 0,
							fontSize: $(this).next().css('font-size')
						});
					}
					else {
						$(this).after('<span class="customStyleSelectBox"><span class="customStyleSelectBoxInner">' + currentSelected.text() + '</span></span>').css({
							position: 'absolute',
							opacity: 0,
							fontSize: $(this).next().css('font-size')
						});
					}
					var selectBoxSpan = $(this).next();
					var selectBoxWidth = parseInt($(this).width()) - parseInt(selectBoxSpan.css('padding-left')) - parseInt(selectBoxSpan.css('padding-right'));
					var selectBoxSpanInner = selectBoxSpan.find(':first-child');
					selectBoxSpan.css({
						display: 'block'
					});
					selectBoxSpanInner.css({
						//                        width:selectBoxWidth, 
						display: 'block'
					});
					var selectBoxHeight = parseInt(selectBoxSpan.height()) + parseInt(selectBoxSpan.css('padding-top')) + parseInt(selectBoxSpan.css('padding-bottom'));
					$(this).height(selectBoxHeight).change(function() {
						selectBoxSpanInner.text($(this).find(':selected').text()).parent().addClass('changed');
					});
				});
			}
		}
	});
})(jQuery);