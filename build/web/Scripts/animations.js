/*
 * Main HASHCUBE JavaScript Library v1.0
 * 
 * O3Interfaces (Pvt) Ltd. 2012
 * Client: Apollo Telecom
 * Date: July 2012
 */



//Global Settings
var LoginCloseTime = 5*60; //Time in seconds

var timerId;
var connectedToWSServer = false;
$(function() {
	$(".keep-signed-in").click(function () {
		$(this).toggleClass("checked-checkbox");
	});
    
	$("#open-login-area").click(function (){
		if(connectedToWSServer){
			$('.hashcube-logo-yellow').css('display', 'block');
			$('.hashcube-logo-yellow').animate({
				opacity: 1
			}, 1000, 'linear');
			$('.hashcube-logo-black').animate({
				opacity: 0
			}, 1000, 'easeInOutCirc', function () {
				$('.logo-wrapper').animate({
					top: 0
				}, 500, 'easeInOutCirc', function () {
					$('.login-area-wrapper').animate({
						opacity: 1
					}, 500, 'linear');
				});
			});
			$('#txtUsername').focus();
			timerId = setInterval(close_login_area, LoginCloseTime*1000);
		}
	});
    
	function close_login_area() {
		if($('.hashcube-logo-yellow').css('display')!='none') {
			$('.login-area-wrapper').animate({
				opacity: 0
			}, 500, 'linear', function () {
				$('.logo-wrapper').animate({
					top: 84
				}, 500, 'easeInOutCirc', function () {
					$('.hashcube-logo-yellow').animate({
						opacity: 0
					}, 1000, 'linear');
					$('.hashcube-logo-black').animate({
						opacity: 1
					}, 1000, 'linear', function () {
						$('.hashcube-logo-yellow').css('display', 'none');
					});
				});
        
			});
			$('#txtUsername').blur();
			$('#txtUserpass').blur();
			clearInterval(timerId);
		}
	}
    
});

function close_login_area(){
	if($('.hashcube-logo-yellow').css('display')!='none') {
		$('.login-area-wrapper').animate({
			opacity: 0
		}, 500, 'linear', function () {
			$('.logo-wrapper').animate({
				top: 84
			}, 500, 'easeInOutCirc', function () {
				$('.hashcube-logo-yellow').animate({
					opacity: 0
				}, 1000, 'linear');
				$('.hashcube-logo-black').animate({
					opacity: 1
				}, 1000, 'linear', function () {
					$('.hashcube-logo-yellow').css('display', 'none');
				});
			});
	
		});
		$('#txtUsername').blur();
		$('#txtUserpass').blur();
		clearInterval(timerId);
	}
}