// NOTE: 
// The following data is in degrees. If you want to convert percentage of a sector into degrees, just multiply your percentage with 3.6.
// e.g. If your first sector is 24 percent then the data to insert into the following array would be 24x3.6.
// Make sure that after calculating all the degrees the sum of all four values should be exactly 360.

var data_top = [0, 0, 0, 0];
var data_middle = [0, 0, 0, 0];
var data_bottom = [0, 0, 0, 0];

function getData_Top(){
	return data_top;
}

function getData_Middle(){
	return data_middle;
}

function getData_Bottom(){
	return data_bottom;
}

function setData_Top(logged_in_time, idle_time, break_time, not_ready_time){
	//var total_time = Math.round(parseFloat(logged_in_time) + parseFloat(idle_time) + parseFloat(break_time) + parseFloat(not_ready_time));
	var total_time = logged_in_time;
	logged_in_time = Math.round((logged_in_time/total_time)*360);
	idle_time = Math.round((idle_time/total_time)*360);
	break_time = Math.round((break_time/total_time)*360);
	not_ready_time = Math.round((not_ready_time/total_time)*360);
	
	data_top = [logged_in_time, idle_time, break_time, not_ready_time];
	
}

function setData_Middle(productive_duration, acw_time, hold_time, avg_talk_time){
	//var total_time = Math.round(parseFloat(productive_duration) + parseFloat(acw_time) + parseFloat(hold_time) + parseFloat(avg_talk_time));
	var total_time = productive_duration;
	productive_duration = Math.round((productive_duration/total_time)*360);
	acw_time = Math.round((acw_time/total_time)*360);
	hold_time = Math.round((hold_time/total_time)*360);
	avg_talk_time = Math.round((avg_talk_time/total_time)*360);
	
	data_middle = [productive_duration, acw_time, hold_time, avg_talk_time];
}

function setData_Bottom(call_offered, call_answered, call_req, short_calls){
	//var total_time = Math.round(parseFloat(call_offered) + parseFloat(call_answered) + parseFloat(call_req) + parseFloat(short_calls));
	var total_time = call_offered;
	
	call_offered = Math.round((call_offered/total_time)*360);
	call_answered = Math.round((call_answered/total_time)*360);
	call_req = Math.round((call_req/total_time)*360);
	short_calls = Math.round((short_calls/total_time)*360);
	
	data_bottom = [call_offered, call_answered, call_req, short_calls];
}

//Chart Creation Functions Below
var colors = ["#ffd200", "#f7941d", "#8a2e26", "fff3be"];
var canvas;
var context;

function initialize_stats_charts()
{
	var stage = new Kinetic.Stage({
		container: "chart01",
		width: 155,
		height: 155
	});
	var layer = new Kinetic.Layer();
	layer.move(77.5,77.5);
	chart_num = 1
	drawSegments(data_top, chart_num, layer, stage);
    
    
	var stage = new Kinetic.Stage({
		container: "chart02",
		width: 155,
		height: 155
	});
	var layer = new Kinetic.Layer();
	layer.move(77.5,77.5);
	drawSegments(data_middle, chart_num, layer, stage);

	var stage = new Kinetic.Stage({
		container: "chart03",
		width: 155,
		height: 155
	});
	var layer = new Kinetic.Layer();
	layer.move(77.5,77.5);
	drawSegments(data_bottom, chart_num, layer, stage);
    
}
            
function drawSegments(data, chart_num, layer, stage) {
    
	var startingAngle = 4.715
    
	var endingAngle1 = startingAngle + degreesToRadians(data[0]);
	var endingAngle2 = startingAngle + degreesToRadians(data[1]);
	var endingAngle3 = startingAngle + degreesToRadians(data[2]);
	var endingAngle4 = startingAngle + degreesToRadians(data[3]);
        
	var segment1 = new Kinetic.Shape({
		drawFunc: function(canvas) {
			var context = canvas.getContext('2d');
			context.beginPath();
			context.moveTo(0, 0);
			context.arc(0, 0, 77.5, startingAngle, endingAngle1, false);
			context.closePath();
			canvas.fill(this);
		},
		fill: colors[0],
		offset: [0, 0],
		scale: [0,0],
		opacity: 0
	});
                
	var segment2 = new Kinetic.Shape({
		drawFunc: function(canvas) {
			var context = canvas.getContext('2d');
			context.beginPath();
			context.moveTo(0, 0);
			context.arc(0, 0, 67.5, startingAngle, endingAngle2, false);
			context.closePath();
			canvas.fill(this);
		},
		fill: colors[1],
		offset: [0, 0],
		scale: [0,0],
		opacity: 0
	});
                
	var segment3 = new Kinetic.Shape({
		drawFunc: function(canvas) {
			var context = canvas.getContext('2d');
			context.beginPath();
			context.moveTo(0, 0);
			context.arc(0, 0, 57.5, startingAngle, endingAngle3, false);
			context.closePath();
			canvas.fill(this);
		},
		fill: colors[2],
		offset: [0, 0],
		scale: [0,0],
		opacity: 0
	});
                
	var segment4 = new Kinetic.Shape({
		drawFunc: function(canvas) {
			var context = canvas.getContext('2d');
			context.beginPath();
			context.moveTo(0, 0);
			context.arc(0, 0, 47.5, startingAngle, endingAngle4, false);
			context.closePath();
			canvas.fill(this);
		},
		fill: colors[3],
		offset: [0, 0],
		scale: [0,0],
		opacity: 0
	});

	// add the segment to the layer
	layer.add(segment1);
	layer.add(segment2);
	layer.add(segment3);
	layer.add(segment4);

	// add the layer to the stage
	stage.add(layer);
    
	var tween = new Kinetic.Tween({
		node: segment1,
		rotation: Math.PI * 4,
		opacity: 1,
		scaleX: 1,
		scaleY: 1,
		duration: 1.4,
		easing: Kinetic.Easings.EaseInOut
	});
	tween.play();
    
	var tween = new Kinetic.Tween({
		node: segment2,
		rotation: Math.PI * 4,
		opacity: 1,
		scaleX: 1,
		scaleY: 1,
		duration: 1.3,
		easing: Kinetic.Easings.EaseInOut
	});
	tween.play();
    
	var tween = new Kinetic.Tween({
		node: segment3,
		rotation: Math.PI * 4,
		opacity: 1,
		scaleX: 1,
		scaleY: 1,
		duration: 1.2,
		easing: Kinetic.Easings.EaseInOut
	});
	tween.play();
    
	var tween = new Kinetic.Tween({
		node: segment4,
		rotation: Math.PI * 4,
		opacity: 1,
		scaleX: 1,
		scaleY: 1,
		duration: 1.1,
		easing: Kinetic.Easings.EaseInOut
	});
	tween.play();
    
}

function sumTo(a, i) {
	var sum = 0;
	for (var j = 0; j < i; j++) {
		sum += a[j];
	}
	return sum;
}

function degreesToRadians(degrees) {
	return (degrees * Math.PI) / 180;
}
