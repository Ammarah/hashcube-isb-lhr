/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var smsWC = '';
var smsWCArr = '';

$('#freqACWDiv .checkbox-wrapper a.checkbox-generic').live('click', function () {
    $('.dragable').draggable({cursor: "crosshair", revert: "invalid",
        drag: function () {
            $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow', 'overlay');
        },
        stop: function () {
            $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow-y', 'scroll');
        }});

    $('.dropable').droppable({accept: ".dragable",
        drop: function (event, ui) {
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
        }});
    $('.dropable').sortable();
    $('.drag').droppable({accept: ".dragable",
        drop: function (event, ui) {
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
        }});
    $('.drag').sortable();
});

$('#acwListCodeName option').live('click', function () {
    $('.dragable').draggable({cursor: "crosshair", revert: "invalid",
        drag: function () {
            $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow', 'overlay');
        },
        stop: function () {
            $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow-y', 'scroll');
        }});

    $('.dropable').droppable({accept: ".dragable",
        drop: function (event, ui) {
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
        }});
    $('.dropable').sortable();
    $('.drag').droppable({accept: ".dragable",
        drop: function (event, ui) {
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
        }});
    $('.drag').sortable();
});

$('#acwSelectedName .selected-option-wrapper, #acwSelectedName1 .selected-option-wrapper').live('click', function () {
    smsWC = $(this).children('div.label').children('input').val();
    smsWCArr = smsWC.split(',');
    if (!($(this).hasClass('bgdblClick'))) {
        if (smsWCArr[1] == 1) {
            var smsString = smsWCArr[5];
            var smsSOP = smsWCArr[6];
            var smsHtml = "";
            if ($(this).hasClass('bgClick')) {
                $(this).removeClass('bgClick');
                smsHtml = 'SMS is disabled on workcode' + '<br/><br/>';
                $('#acwDesc').html(smsHtml);
            }
            else {
                //$('#acwSelectedName .dragable').removeClass('bgClick');
                $(this).addClass('bgClick');
                smsHtml += 'SMS is enabled on workcode' + '<br/><br/>';
                if (smsSOP != 'null') {
                    smsHtml += '' + "SMS Description:" + '<br/>' + '\n';
                    smsHtml += '' + smsSOP + '<br/><br/>' + '\n';
                }
                if (smsString != 'null') {
                    smsHtml += '' + 'SMS SOP:' + '<br/>' + '\n';
                    smsHtml += '' + smsString + '<br/>' + '\n';
                }
                $('#acwDesc').html(smsHtml);
            }
        }
    }
}).live('dblclick', function () {
    if (clientObj.isAgentOnCall()) {
        if (!($(this).hasClass('bgClick'))) {
            if (smsWCArr[4] != 'null') {
                var smsHtml = "";
                if ($(this).hasClass('bgdblClick')) {
                    $(this).removeClass('bgdblClick');
                    $('#submitAWC_btn').css('display', 'none');
                    smsHtml = 'IVR transfer is disabled on workcode' + '<br/><br/>';
                    $('#acwDesc').html(smsHtml);
                }
                else {
                    $('#acwSelectedName .dragable').removeClass('bgdblClick');
                    $(this).addClass('bgdblClick');
                    $('#submitAWC_btn').css('display', 'inline-block');
                    smsHtml = 'IVR transfer is enabled on workcode' + '<br/><br/>';
                    $('#acwDesc').html(smsHtml);
                }
            }
        }
    }
});



